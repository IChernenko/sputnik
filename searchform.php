<?php
/*** The html form for search input. ***/
?>

<form action="<?php echo esc_url( site_url() ); ?>" role="search" method="get" id="search">
	<input type="text" class="s" name="s" id="s" value="<?php esc_attr(the_search_query()); ?>" placeholder="<?php esc_attr_e('Try and type enter...', 'sputnik');?>" />
</form>