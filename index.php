<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 */

get_header();

$sputnik_postpage_id = get_option( 'page_for_posts' );
$sputnik_frontpage_id = get_option( 'page_on_front' );
$sputnik_page_id = isset($wp_query) ? $wp_query->get_queried_object_id() : '';

if ( $sputnik_page_id == $sputnik_postpage_id && $sputnik_postpage_id != $sputnik_frontpage_id ) :
	$sputnik_custom = isset( $wp_query ) ? get_post_custom( $wp_query->get_queried_object_id() ) : '';
	$sputnik_layout = isset( $sputnik_custom['pix_page_layout'] ) ? $sputnik_custom['pix_page_layout'][0] : '2';
	$sputnik_sidebar = isset( $sputnik_custom['pix_selected_sidebar'][0] ) ? $sputnik_custom['pix_selected_sidebar'][0] : 'sidebar-1';
else :
	$sputnik_layout = sputnik_get_option('blog_settings_sidebar_type', '2');
	$sputnik_sidebar = sputnik_get_option('blog_settings_sidebar_content', 'sidebar-1');
endif;

if ( ! is_active_sidebar($sputnik_sidebar) ) $sputnik_layout = '1';

?>

	<!-- =========================
		BLOG ITEMS
	============================== -->
	<div class="def-section blog-section">
		<div class="container">
			<div class="row">

				<?php sputnik_show_sidebar( 'left', $sputnik_layout, $sputnik_sidebar ); ?>

				<!-- === BLOG ITEMS === -->

				<div class="<?php if ( $sputnik_layout == 1 ) : ?>col-lg-12 col-md-12 col-sm-12<?php else : ?>col-lg-9 col-md-9 col-sm-9<?php endif; ?> col-xs-12 blog-items-<?php echo esc_attr($sputnik_layout); ?>">

					<?php
						if ( have_posts() ) :
							// Start the Loop.
							while ( have_posts() ) : the_post();

								get_template_part( 'templates/post-parts/content' );

							endwhile;

						else:
							// If no content, include the "No posts found" template.
							get_template_part( 'templates/post-parts/content', 'none' );

						endif;

					?>

				</div>

				<?php sputnik_show_sidebar( 'right', $sputnik_layout, $sputnik_sidebar ); ?>

			</div>
		</div>
	</div>

	<?php sputnik_num_pagination(); ?>

<?php get_footer(); ?>