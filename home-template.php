<?php
/**
 * The template for displaying Home page (full width).
 *
 * @package sputnik
 * @since 1.0
 *
 * Template Name: Home Full Width
 */

get_header(); ?>

<div class="home-section">
	<div class="container">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<?php the_content(); ?>

	<?php endwhile; ?>

	</div>
</div>

<?php get_footer(); ?>