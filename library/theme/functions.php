<?php

function sputnik_show_sidebar($type, $layout, $sidebar)
{

    $layouts = array(
            1 => 'full',
            2 => 'right',
            3 => 'left',
    );

    if (isset($layouts[$layout]) && $type === $layouts[$layout]) {
        echo '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><div class="sidebar blog-right-bar">';
        if (is_active_sidebar($sidebar)) : dynamic_sidebar($sidebar); endif;
        echo '</div></div>';
    } else {
        echo '';
    }

}

function sputnik_limit_words($string, $word_limit)
{

    $string = preg_replace("#\[.*?\]#is", '', $string);
    $string = wp_trim_words($string, $word_limit, ' [...]');

    return $string;
}

/* BOOTSTRAP MENU WALKER */

class sputnik_Walker_Bootstrap_Menu extends Walker_Nav_Menu
{
    /**
     * Display Element
     */
    function display_element($element, &$children_elements, $max_depth, $depth, $args, &$output)
    {
        $id_field = $this->db_fields['id'];

        if (isset($args[0]) && is_object($args[0])) {
            $args[0]->has_children = !empty($children_elements[$element->$id_field]);
        }

        return parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }

    /**
     * Start Element
     */
    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {
        if (is_object($args) && !empty($args->has_children)) {

            $link_after = $args->link_after;
            $args->link_after = ' <span class="caret"></span>';
            array_push($item->classes, "dropdown");
        }


        parent::start_el($output, $item, $depth, $args, $id);

        if (is_object($args) && !empty($args->has_children))
            $args->link_after = $link_after;
    }

    /**
     * Start Level
     */
    function start_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"dropdown-menu\">\n";
    }
}

/* END BOOTSTRAP MENU WALKER */

/* RESPONSIVE MENU WALKER */

class sputnik_Walker_Responsive_Menu extends Walker_Nav_Menu
{
    /**
     * Display Element
     */
    function display_element($element, &$children_elements, $max_depth, $depth, $args, &$output)
    {
        $id_field = $this->db_fields['id'];

        if (isset($args[0]) && is_object($args[0])) {
            $args[0]->has_children = !empty($children_elements[$element->$id_field]);
        }

        return parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }

    /**
     * Start Element
     */
    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {
        if (is_object($args) && !empty($args->has_children)) {

            $link_after = $args->link_after;
            $args->link_after = ' <i class="fa fa-plus arrow"></i>';
        }


        parent::start_el($output, $item, $depth, $args, $id);

        if (is_object($args) && !empty($args->has_children))
            $args->link_after = $link_after;
    }

    /**
     * Start Level
     */
    function start_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"slide-menu-child\">\n";
    }
}

/* RESPONSIVE MENU WALKER */

/********************************************/


/************* COMMENTS HOOK *************/

function sputnik_comments_callback($comment, $args, $depth)
{
    $GLOBALS['comment'] = $comment; ?>
<li <?php comment_class('media'); ?> id="li-comment-<?php comment_ID() ?>">
    <div id="comment-<?php comment_ID(); ?>" class="comment-item">
        <div class="media-left pull-left">
            <?php
            get_avatar($comment, $size = '95');
            $get_avatar = get_avatar($comment);
            preg_match("/src=['\"](.*?)['\"]/i", $get_avatar, $matches);
            $src = !empty($matches[1]) ? $matches[1] : '';
            ?>
            <img alt="<?php echo get_comment_author(); ?>" src="<?php echo !empty($src) ? esc_url($src) : esc_url(get_template_directory_uri() . '/img/nouser.jpg'); ?>" class="media-object">
        </div>

        <div class="media-body">

            <div class="media-body-top">
                <div class="media-info">
                    <h4 class="media-heading"><?php echo get_comment_author_link(); ?></h4>
                    <span class="media-date"><?php printf(esc_html__('%1$s at %2$s', 'sputnik'), get_comment_date(), get_comment_time()); ?></span>
                </div>
                <?php comment_reply_link(array_merge($args, array('reply_text' => esc_html__('Reply', 'sputnik'), 'depth' => $depth, 'max_depth' => $args['max_depth'], 'before' => '<div class="media-reply"><div class="my-btn my-btn-default"><div class="my-btn-bg-top"></div><div class="my-btn-bg-bottom"></div>', 'after' => '</div></div>'))); ?>
                <?php if ($comment->comment_approved == '0') : ?>
                    <span class="not-approve"><?php esc_html_e('Your comment is awaiting moderation.', 'sputnik'); ?></span>
                <?php endif; ?>
            </div>
            <div class="rtd"><?php comment_text(); ?></div>
            <?php edit_comment_link(esc_html__('Edit', 'sputnik'), '  ', '') ?>
        </div>
    </div>
<?php }

function sputnik_comments_end_callback()
{
    echo '</li>';
}

add_filter('comment_reply_link', 'sputnik_replace_reply_link_class');
function sputnik_replace_reply_link_class($class)
{
    $class = str_replace("class='comment-reply-link", "class='comment-reply-link my-btn-text", $class);
    return $class;
}

/*****************************************/

// meta box
if (!function_exists('rwmb_meta')) {
    function rwmb_meta($key, $args = '', $post_id = null)
    {
        return false;
    }
}

// numbered pagination
function sputnik_num_pagination($pages = '', $range = 2)
{
    $showitems = ($range * 2) + 1;

    global $paged;
    if (empty($paged)) {
        $paged = 1;
    }

    if ($pages == '') {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if (!$pages) {
            $pages = 1;
        }
    }

    if (1 != $pages) {
        echo '<div class="def-section pagination-section"><div class="container"><ul class="pagination">';

        if ($paged > 1 && $showitems < $pages) echo '<li><a href="' . esc_url(get_pagenum_link(esc_html($paged) - 1)) . '"><i class="fa-chevron-left"></i></a></li>';

        for ($i = 1; $i <= $pages; $i++) {
            if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
                echo ($paged == $i) ? '<li class="active"><a href="#">' . $i . '</a></li>' : '<li><a href="' . esc_url(get_pagenum_link($i)) . '">' . esc_html($i) . '</a></li>';
            }
        }

        if ($paged < $pages && $showitems < $pages) echo '<li><a href="' . esc_url(get_pagenum_link(esc_html($paged) + 1)) . '"><i class="fa-chevron-right"></i></a></li>';

        echo '</ul></div></div>';
    }
}

function sputnik_pix_wp_get_attachment($attachment_id)
{
    $attachment = get_post($attachment_id);
    return array(
            'alt' => get_post_meta($attachment->ID, '_wp_attachment_image_alt', true),
            'caption' => $attachment->post_excerpt,
            'description' => $attachment->post_content,
            'href' => get_permalink($attachment->ID),
            'src' => $attachment->guid,
            'title' => $attachment->post_title
    );
}

//page comment open by default
function sputnik_open_comments_for_page($status, $post_type, $comment_type)
{
    if ('page' === $post_type) {
        return 'open';
    }

    // You could be more specific here for different comment types if desired
    return $status;
}

add_filter('get_default_comment_status', 'sputnik_open_comments_for_page', 10, 3);


//Change the order of the output fields comment form
add_filter('comment_form_fields', 'sputnik_reorder_comment_fields');
function sputnik_reorder_comment_fields($fields)
{

    $new_fields = array();

    $myorder = array('author', 'email', 'url', 'comment');

    foreach ($myorder as $key) {
        $new_fields[$key] = $fields[$key];
        unset($fields[$key]);
    }

    if ($fields)
        foreach ($fields as $key => $val)
            $new_fields[$key] = $val;

    return $new_fields;
}

// add user contact info
add_filter('user_contactmethods', 'sputnik_user_contactmethods');
function sputnik_user_contactmethods($user_contactmethods)
{

    $user_contactmethods['twitter'] = esc_html__('Twitter', 'sputnik');
    $user_contactmethods['facebook'] = esc_html__('Facebook', 'sputnik');
    $user_contactmethods['google-plus'] = esc_html__('Google+', 'sputnik');
    $user_contactmethods['pinterest'] = esc_html__('Pinterest', 'sputnik');
    $user_contactmethods['instagram'] = esc_html__('Instagram', 'sputnik');

    return $user_contactmethods;
}

// Enqueue the Google fonts
function sputnik_fonts_url()
{
    $fonts_url = '';

    /* Translators: If there are characters in your language that are not
    * supported by Montserrat, translate this to 'off'. Do not translate
    * into your own language.
    */
    $montserrat = esc_html_x('on', 'Montserrat font: on or off', 'sputnik');

    /* Translators: If there are characters in your language that are not
    * supported by Lato, translate this to 'off'. Do not translate
    * into your own language.
    */
    $lato = esc_html_x('on', 'Lato font: on or off', 'sputnik');


    if ('off' !== $montserrat || 'off' !== $lato) {
        $font_families = array();

        if ('off' !== $montserrat) {
            $font_families[] = 'Montserrat:400,700';
        }

        if ('off' !== $lato) {
            $font_families[] = 'Lato:400,300,700';
        }

        $query_args = array(
                'family' => urlencode(implode('|', $font_families)),
                'subset' => urlencode('latin,latin-ext')
        );

        $fonts_url = add_query_arg($query_args, 'https://fonts.googleapis.com/css');
    }

    return esc_url_raw($fonts_url);
}

// dynamic styles activate
function sputnik_pix_dynamic_styles()
{
    include(get_template_directory() . '/css/dynamic-styles.php');
    exit;
}

add_action('wp_ajax_dynamic_styles', 'sputnik_pix_dynamic_styles');
add_action('wp_ajax_nopriv_dynamic_styles', 'sputnik_pix_dynamic_styles');
