<?php
/**
 * The template for registering metabox.
 *
 * @package sputnik
 * @since 1.0
 */
add_filter( 'rwmb_meta_boxes', 'sputnik_pix_register_meta_boxes' );

function sputnik_pix_register_meta_boxes( $meta_boxes ) {

	$meta_boxes[] = array(

		'id' => 'post_format',
		'title' => esc_html__( 'Post Format Options', 'sputnik' ),
		'pages' => array( 'post' ),
		'context' => 'normal',
		'priority' => 'high',
		'autosave' => true,
		'fields' => array(
			array(
				'name' => esc_html__('Post Gallery:', 'sputnik'),
				'id'   => 'post_gallery',
				'type' => 'image_advanced',
				'max_file_uploads' => 25
			),
			array(
				'name'  => esc_html__('Quote Source:', 'sputnik'),
				'id'    => 'post_quote_source',
				'desc'  => '',
				'type'  => 'text',
				'std'   => '',
			),
			array(
				'name'  => esc_html__('Quote Content:', 'sputnik'),
				'id'    => 'post_quote_content',
				'desc'  => '',
				'type'  => 'textarea',
				'std'   => '',
			),
			array(
				'name'  => esc_html__('Video URL', 'sputnik'),
				'id'    => "post_video",
				'type'  => 'oembed',
				'desc' => esc_html__( 'Enter video link eg (https://youtu.be/R8OOWcsFj0U)', 'sputnik' )
			),
		)

	);


	return $meta_boxes;
}

function sputnik_register_page_metabox()
{
	$cmb = new_cmb2_box(array(
		'id' => 'page_metabox',
		'title' => __('Page meta', 'sputnik'),
		'object_types' => array('page'), // post type
		'context' => 'normal', //  'normal', 'advanced', or 'side'
		'priority' => 'high',  //  'high', 'core', 'default' or 'low'
		'show_names' => true, // Show field names on the left
	));

	$cmb->add_field(array(
		'name' => __('Header image', 'sputnik'),
		'id' => 'header_image',
		'type' => 'file',
	));

	$cmb->add_field(array(
		'name' => __('Header title', 'sputnik'),
		'id' => 'header_title',
		'type' => 'text',
	));

	$cmb->add_field(array(
		'name' => __('Header description', 'sputnik'),
		'id' => 'header_description',
		'type' => 'textarea_small',
	));
}

add_action('cmb2_admin_init', 'sputnik_register_page_metabox');
