/*------------------------------------------------------------------
GENERATED CSS FILE
-------------------------------------------------------------------*/
<?php
$sputnik_main_color = sputnik_get_option( 'style_settings_main_color', '#00c0e2' );
$sputnik_color_1 = sputnik_get_option( 'style_settings_additional_color_1', '#5fd3e8' );
$sputnik_color_2 = sputnik_get_option( 'style_settings_additional_color_2', '#e3ebf0' );
?>

a {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.color-primary {
	color: <?php echo esc_attr($sputnik_main_color); ?> !important;
}
.with-square:before {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.my-btn-primary {
	border: 1px solid <?php echo esc_attr($sputnik_main_color); ?>;
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.my-btn-primary:hover {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.btn-primary {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
	border-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.btn-primary.disabled {
	background-color: <?php echo esc_attr($sputnik_color_1); ?>;
}
.text-white .my-btn-primary:hover .my-btn-text {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.text-black .my-btn-primary:hover .my-btn-text {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.pagination .active a {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
	border-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.pagination .active a:hover,
.pagination .active a:focus {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
	border-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.breadcrumb > li > a:hover,
.breadcrumb > li > a:focus {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.breadcrumb > .active {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
blockquote {
	background-color: <?php echo esc_attr($sputnik_color_2); ?>;
}
blockquote::before {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.my-blockquote {
	background-color: <?php echo esc_attr($sputnik_color_2); ?>;
}
.my-blockquote-quote div {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.media-body-top .media-info .media-date {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.media-body-top .media-reply .my-btn-default {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
	border-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.media-body-top .media-reply .my-btn-default .my-btn-bg-top,
.media-body-top .media-reply .my-btn-default .my-btn-bg-bottom {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.media-body-top .media-reply .my-btn-default:hover {
	border-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.cssload-whirlpool,
.cssload-whirlpool::before,
.cssload-whirlpool::after {
	border-left-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.top-bar {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.main-menu .navbar-nav > li > a:hover,
.main-menu .navbar-nav > li > a:focus {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.main-menu .navbar-nav > li > a.active {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.main-menu .navbar-nav > li.current-menu-ancestor > a {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.main-menu .navbar-nav > li.active > a {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.main-menu .navbar-nav li.open > a {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.main-menu .dropdown-menu {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.main-menu .dropdown-menu > li > a:hover,
.main-menu .dropdown-menu > li > a:focus {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.main-menu .dropdown-menu > li > a.active {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.main-menu .dropdown-menu > li.active > a {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.main-menu .dropdown-menu:after {
	border-bottom-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.main-slider .my-btn-default {
	border-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.main-slider .my-btn-default .my-btn-bg-top,
.main-slider .my-btn-default .my-btn-bg-bottom {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.service-mark {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.service-mark .my-btn-default .my-btn-bg-top,
.service-mark .my-btn-default .my-btn-bg-bottom {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.service-mark-border-top {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.service-mark-border-right {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.service-mark-border-bottom {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.service-mark-border-left {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.text-white .service .my-btn-default .my-btn-bg-top,
.text-white .service .my-btn-default .my-btn-bg-bottom {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.section-bg-right {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.get-quote {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.home-review-carousel-quote div {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.stat-item-icon {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.stat-item-mark {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.home-blog-item-desc-info .comments-icon {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.home-blog-item-mark {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.home-blog-item-mark .home-blog-item-date {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.home-blog-item-mark .home-blog-item-desc {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.home-subscribe-form-button .my-btn-primary {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.service-menu > li > a:hover,
.sidebar-item .service-menu > li > a:hover,
.service-menu > li > a:focus,
.sidebar-item .service-menu > li > a:focus {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.service-menu > li.active > a,
.sidebar-item .service-menu > li.active > a,
.service-menu > li.current-cat > a,
.sidebar-item .service-menu > li.current-cat > a {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.service-detail .service-related h3 a:hover,
.service-detail .service-related h3 a:focus {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.about-text h2:before {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.team-item-image-overlay {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.team-item:hover .team-item-name {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.blog-item-info .author-icon {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.blog-item-info .date-icon {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.blog-item-info .comments-icon {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.blog-item-title h3:before {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.blog-item-mark .blog-item-date {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.blog-item.sticky .blog-item-date {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
	text-transform: uppercase;
}
.blog-item.sticky .my-btn {
	border: 1px solid <?php echo esc_attr($sputnik_main_color); ?>;
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.blog-item.sticky .my-btn:hover {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.sidebar-item:before {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.sidebar-item ul li a:hover,
.sidebar-item ul li a:focus {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.sidebar-item ul li.current-cat > a,
.sidebar-item ul li.current_page_item > a {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.tagcloud a:hover,
.tagcloud a:focus {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.widget_rss .widget-title .rsswidget:hover {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.widget_rss .rss-date::before {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.widget_rss .rssSummary {
	background-color: <?php echo esc_attr($sputnik_color_2); ?>;
	border-bottom: 3px solid <?php echo esc_attr($sputnik_main_color); ?>;
}
.widget_recent_entries .post-date::before {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.widget_pix_totalposts .nav-tabs li a {
	border-top: 1px solid <?php echo esc_attr($sputnik_main_color); ?>;
	background-color: <?php echo esc_attr($sputnik_color_2); ?>;
}
.widget_pix_totalposts .nav-tabs li.active a,
.widget_pix_totalposts .nav-tabs li:hover a {
	border-top: 1px solid <?php echo esc_attr($sputnik_main_color); ?>;
}
.widget_pix_totalposts .media-tab .media-body time .icon-calendar::before {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.widget_pix_totalposts .media-tab .media-body .media-heading a:hover {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.widget_pix_totalposts .media-tab-comment .media-body {
	background-color: <?php echo esc_attr($sputnik_color_2); ?>;
	border-left: 3px solid <?php echo esc_attr($sputnik_main_color); ?>;
}
.single-post-date {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.single-post-info .author-icon {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.single-post-info .date-icon {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.single-post-info .comments-icon {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.single-post-title h3:before {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.single-post-author-info-socials .my-btn-default .my-btn-bg-top,
.single-post-author-info-socials .my-btn-default .my-btn-bg-bottom {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.single-post-author-info-socials .my-btn-default:hover {
	border-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.single-post-comments h3:before {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.single-post-leave-comment h3:before {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.contacts-info-title {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.contact-detail {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.contact-detail-mark .contact-detail-icon {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.contact-detail-mark .contact-detail-title h3:before {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.slide-menu .close-menu:hover {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.slide-menu .left-menu > li > a:hover,
.slide-menu .left-menu > li > a:focus {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.mc4wp-form input[type="submit"] {
	background-color: <?php echo esc_attr($sputnik_color_2); ?>;
}
.mc4wp-form input[type="submit"]:hover {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.text-white .home-blog-more a:hover .my-btn-text {
	color: <?php echo esc_attr($sputnik_main_color); ?>;
}
.text-white .fobox .with-square:before {
	background-color: <?php echo esc_attr($sputnik_main_color); ?>;
}




<?php if ( sputnik_get_option('style_settings_custom_css') != '' ) : ?>
<?php echo esc_html( sputnik_get_option('style_settings_custom_css') ); ?>
<?php endif; ?>



















