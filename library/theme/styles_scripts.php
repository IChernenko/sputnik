<?php

add_action('wp_enqueue_scripts', 'sputnik_load_styles_scripts');

function sputnik_load_styles_scripts(){

	if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

		wp_enqueue_style('style', get_stylesheet_uri());
//		wp_enqueue_style('sputnik-fonts', sputnik_fonts_url(), array(), NULL, 'screen, all');
		wp_enqueue_style('prettyPhoto', get_template_directory_uri() . '/assets/prettyphoto/css/prettyPhoto.css' );
		wp_enqueue_style('sputnik-master', get_template_directory_uri() . '/css/master.css');
		wp_enqueue_style('switcher', get_template_directory_uri() . '/js/switcher/css/switcher.css');
		wp_enqueue_style('color1', get_template_directory_uri() . '/js/switcher/css/color1.css');
//		wp_enqueue_style('sputnik-dynamic-styles', admin_url('admin-ajax.php').'?action=dynamic_styles&pageID='.get_the_ID());

		wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery') , '3.3.5', true );
		wp_enqueue_script( 'smoothscroll', get_template_directory_uri() . '/js/smoothscroll.min.js', array('jquery') , NULL, true );
		wp_enqueue_script( 'stellar', get_template_directory_uri() . '/js/jquery.stellar.min.js', array('jquery') , '0.6.2', true );
		wp_enqueue_script( 'wow', get_template_directory_uri() . '/js/wow.min.js', array('jquery') , '1.1.2', true );
		wp_enqueue_script( 'jflickrfeed', get_template_directory_uri() . '/js/jflickrfeed.min.js', array('jquery') , NULL, true );
		wp_enqueue_script( 'imagesloaded', get_template_directory_uri() . '/js/imagesloaded.min.js', array('jquery') , '4.1.0', true );
		wp_enqueue_script( 'scrollspy', get_template_directory_uri() . '/js/scrollspy.min.js', array('jquery') , NULL, true );
		wp_enqueue_script( 'metisMenu', get_template_directory_uri() . '/js/metisMenu.min.js', array('jquery') , '2.0.3', true );
		wp_enqueue_script( 'isotope', get_template_directory_uri() . '/js/jquery.isotope.min.js', array('jquery') , '2.2.2', true );
		wp_enqueue_script( 'prettyPhoto', get_template_directory_uri() . '/assets/prettyphoto/js/jquery.prettyPhoto.js', array('jquery') , '3.1.6', true );
//		wp_enqueue_script( 'sputnik-custom', get_template_directory_uri() . '/js/theme.js', array('jquery') , NULL, true );

		wp_enqueue_script( 'jquery-1.11.3.min', get_template_directory_uri() . '/js/jquery-1.11.3.min.js', array('jquery') , NULL, true );
    wp_enqueue_script( 'slidebar', get_template_directory_uri() . '/js/header/slidebar.js', array('jquery') , NULL, true );
    wp_enqueue_script( 'header', get_template_directory_uri() . '/js/header/header.js', array('jquery') , NULL, true );
    wp_enqueue_script( 'sliderPro', get_template_directory_uri() . '/js/slider-pro/jquery.sliderPro.min.js', array('jquery') , NULL, true );
    wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/js/magnific-popup/jquery.magnific-popup.min.js', array('jquery') , NULL, true );
    wp_enqueue_script( 'bootstrap-select', get_template_directory_uri() . '/js/bootstrap-select/dist/js/bootstrap-select.min.js', array('jquery') , NULL, true );
    wp_enqueue_script( 'doubletaptogo', get_template_directory_uri() . '/js/doubletaptogo.js', array('jquery') , NULL, true );
    wp_enqueue_script( 'waypoints', get_template_directory_uri() . '/js/waypoints.min.js', array('jquery') , NULL, true );
    wp_enqueue_script( 'flowplayer', get_template_directory_uri() . '/js/flowplayer/flowplayer.min.js', array('jquery') , NULL, true );
    wp_enqueue_script( 'classie', get_template_directory_uri() . '/js/classie.js', array('jquery') , NULL, true );
    wp_enqueue_script( 'scrollreveal', get_template_directory_uri() . '/js/scrollreveal/scrollreveal.min.js', array('jquery') , NULL, true );
    wp_enqueue_script( 'dmss', get_template_directory_uri() . '/js/switcher/js/dmss.js', array('jquery') , NULL, true );
		wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/assets/owl-carousel/owl.carousel.js', array('jquery') , '1.3.3', true );

		wp_enqueue_script( 'easypiechart', get_template_directory_uri() . '/js/jquery.easypiechart.min.js', array('jquery') , '2.1.7', true );
		wp_enqueue_script( 'sputnik-custom', get_template_directory_uri() . '/js/separate-js/custom.js', array('jquery') , NULL, true );



		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }


	}
}

//Load Custom Admin Scripts
function sputnik_custom_admin_enqueue_scripts() {

	wp_enqueue_style('sputnik-admin', get_template_directory_uri() . '/css/admin.css', array(), NULL, 'screen, all');

	wp_register_script('sputnik_custom_admin_script', get_template_directory_uri() . '/js/custom-admin.js', false, '1.0.0');
	wp_enqueue_script('sputnik_custom_admin_script');
}
add_action('admin_enqueue_scripts', 'sputnik_custom_admin_enqueue_scripts');


add_filter('body_class','sputnik_browser_body_class');
function sputnik_browser_body_class($classes = '') {
	$classes[] = 'animated-css';
	$classes[] = 'layout-switch';

	if (sputnik_get_option('header_settings_type')){
		$headerType = sputnik_get_option('header_settings_type');
		$classes[] =  'home-construction-v' . $headerType;

	}

	return $classes;
}