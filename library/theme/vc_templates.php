<?php
add_action('init', 'sputnik_integrateWithVC', 200);

function sputnik_integrateWithVC()
{

    if (!function_exists('vc_map'))
        return FALSE;

    $args = array('taxonomy' => 'category', 'hide_empty' => '0');
    $categories_blog = get_categories($args);
    $cats_post = array();
    $i = 0;

    foreach ($categories_blog as $category) {
        if ($category && is_object($category)) {
            if ($i == 0) {
                $default = $category->slug;
                $i++;
            }
            $cats_post[$category->name] = $category->term_id;
        }

    }

    $args = array('taxonomy' => 'services_category', 'hide_empty' => '0');
    $categories_serv = get_categories($args);
    $cats_serv = array();
    $i = 0;

    foreach ($categories_serv as $category) {
        if ($category && is_object($category)) {
            if ($i == 0) {
                $default = $category->slug;
                $i++;
            }
            $cats_serv[$category->name] = $category->term_id;
        }

    }

    $args = array('post_type' => 'wpcf7_contact_form');
    $forms = get_posts($args);
    $cform7 = array();
    if (empty($forms['errors'])) {
        foreach ($forms as $form) {
            $cform7[$form->post_title] = $form->ID;
        }
    }

    /** Fonts Icon Loader */

    $vc_icons_data = sputnik_init_vc_icons();

    $add_css_animation = array(
        'type' => 'dropdown',
        'heading' => esc_html__('CSS Animation', 'sputnik'),
        'param_name' => 'css_animation',
        'admin_label' => true,
        'value' => array(
            esc_html__('No', 'sputnik') => '',
            esc_html__('bounce', 'sputnik') => 'bounce',
            esc_html__('flash', 'sputnik') => 'flash',
            esc_html__('pulse', 'sputnik') => 'pulse',
            esc_html__('rubberBand', 'sputnik') => 'rubberBand',
            esc_html__('shake', 'sputnik') => 'shake',
            esc_html__('swing', 'sputnik') => 'swing',
            esc_html__('tada', 'sputnik') => 'tada',
            esc_html__('wobble', 'sputnik') => 'wobble',
            esc_html__('jello', 'sputnik') => 'jello',

            esc_html__('bounceIn', 'sputnik') => 'bounceIn',
            esc_html__('bounceInDown', 'sputnik') => 'bounceInDown',
            esc_html__('bounceInLeft', 'sputnik') => 'bounceInLeft',
            esc_html__('bounceInRight', 'sputnik') => 'bounceInRight',
            esc_html__('bounceInUp', 'sputnik') => 'bounceInUp',
            esc_html__('bounceOut', 'sputnik') => 'bounceOut',
            esc_html__('bounceOutDown', 'sputnik') => 'bounceOutDown',
            esc_html__('bounceOutLeft', 'sputnik') => 'bounceOutLeft',
            esc_html__('bounceOutRight', 'sputnik') => 'bounceOutRight',
            esc_html__('bounceOutUp', 'sputnik') => 'bounceOutUp',

            esc_html__('fadeIn', 'sputnik') => 'fadeIn',
            esc_html__('fadeInDown', 'sputnik') => 'fadeInDown',
            esc_html__('fadeInDownBig', 'sputnik') => 'fadeInDownBig',
            esc_html__('fadeInLeft', 'sputnik') => 'fadeInLeft',
            esc_html__('fadeInLeftBig', 'sputnik') => 'fadeInLeftBig',
            esc_html__('fadeInRight', 'sputnik') => 'fadeInRight',
            esc_html__('fadeInRightBig', 'sputnik') => 'fadeInRightBig',
            esc_html__('fadeInUp', 'sputnik') => 'fadeInUp',
            esc_html__('fadeInUpBig', 'sputnik') => 'fadeInUpBig',
            esc_html__('fadeOut', 'sputnik') => 'fadeOut',
            esc_html__('fadeOutDown', 'sputnik') => 'fadeOutDown',
            esc_html__('fadeOutDownBig', 'sputnik') => 'fadeOutDownBig',
            esc_html__('fadeOutLeft', 'sputnik') => 'fadeOutLeft',
            esc_html__('fadeOutLeftBig', 'sputnik') => 'fadeOutLeftBig',
            esc_html__('fadeOutRight', 'sputnik') => 'fadeOutRight',
            esc_html__('fadeOutRightBig', 'sputnik') => 'fadeOutRightBig',
            esc_html__('fadeOutUp', 'sputnik') => 'fadeOutUp',
            esc_html__('fadeOutUpBig', 'sputnik') => 'fadeOutUpBig',

            esc_html__('flip', 'sputnik') => 'flip',
            esc_html__('flipInX', 'sputnik') => 'flipInX',
            esc_html__('flipInY', 'sputnik') => 'flipInY',
            esc_html__('flipOutX', 'sputnik') => 'flipOutX',
            esc_html__('flipOutY', 'sputnik') => 'flipOutY',

            esc_html__('lightSpeedIn', 'sputnik') => 'lightSpeedIn',
            esc_html__('lightSpeedOut', 'sputnik') => 'lightSpeedOut',

            esc_html__('rotateIn', 'sputnik') => 'rotateIn',
            esc_html__('rotateInDownLeft', 'sputnik') => 'rotateInDownLeft',
            esc_html__('rotateInDownRight', 'sputnik') => 'rotateInDownRight',
            esc_html__('rotateInUpLeft', 'sputnik') => 'rotateInUpLeft',
            esc_html__('rotateInUpRight', 'sputnik') => 'rotateInUpRight',
            esc_html__('rotateOut', 'sputnik') => 'rotateOut',
            esc_html__('rotateOutDownLeft', 'sputnik') => 'rotateOutDownLeft',
            esc_html__('rotateOutDownRight', 'sputnik') => 'rotateOutDownRight',
            esc_html__('rotateOutUpLeft', 'sputnik') => 'rotateOutUpLeft',
            esc_html__('rotateOutUpRight', 'sputnik') => 'rotateOutUpRight',

            esc_html__('slideInUp', 'sputnik') => 'slideInUp',
            esc_html__('slideInDown', 'sputnik') => 'slideInDown',
            esc_html__('slideInLeft', 'sputnik') => 'slideInLeft',
            esc_html__('slideInRight', 'sputnik') => 'slideInRight',
            esc_html__('slideOutUp', 'sputnik') => 'slideOutUp',
            esc_html__('slideOutDown', 'sputnik') => 'slideOutDown',
            esc_html__('slideOutLeft', 'sputnik') => 'slideOutLeft',
            esc_html__('slideOutRight', 'sputnik') => 'slideOutRight',

            esc_html__('zoomIn', 'sputnik') => 'zoomIn',
            esc_html__('zoomInDown', 'sputnik') => 'zoomInDown',
            esc_html__('zoomInLeft', 'sputnik') => 'zoomInLeft',
            esc_html__('zoomInRight', 'sputnik') => 'zoomInRight',
            esc_html__('zoomInUp', 'sputnik') => 'zoomInUp',
            esc_html__('zoomOut', 'sputnik') => 'zoomOut',
            esc_html__('zoomOutDown', 'sputnik') => 'zoomOutDown',
            esc_html__('zoomOutLeft', 'sputnik') => 'zoomOutLeft',
            esc_html__('zoomOutRight', 'sputnik') => 'zoomOutRight',
            esc_html__('zoomOutUp', 'sputnik') => 'zoomOutUp',

            esc_html__('hinge', 'sputnik') => 'hinge',
            esc_html__('rollIn', 'sputnik') => 'rollIn',
            esc_html__('rollOut', 'sputnik') => 'rollOut',

        ),
        'description' => esc_html__('Select type of animation if you want this element to be animated when it enters into the browsers viewport. Note: Works only in modern browsers.', 'sputnik')
    );


    /** Additional Row Settings */

    $attributes = array(
        array(
            'type' => 'dropdown',
            'heading' => esc_html__('Padding', 'sputnik'),
            'param_name' => 'ppadding',
            'value' => array(
                esc_html__("Default", "sputnik") => 'vc_pixrow-no-padding',
                esc_html__("Both", "sputnik") => 'vc_pixrow-padding-both',
                esc_html__("Top", "sputnik") => 'vc_pixrow-padding-top',
                esc_html__("Bottom", "sputnik") => 'vc_pixrow-padding-bottom',
            ),
            'description' => esc_html__('Top, bottom, both', 'sputnik'),
            'group' => esc_html__('Row Options', 'sputnik'),
        ),
        array(
            'type' => 'dropdown',
            'heading' => esc_html__('Overlay', 'sputnik'),
            'param_name' => 'pixoverlay',
            'value' => array(
                esc_html__(esc_html__("No", "sputnik"), "sputnik") => '',
                esc_html__(esc_html__("Yes", "sputnik"), "sputnik") => 'vc_row-overlay dark',

            ),
            'description' => esc_html__('Yes / No', 'sputnik'),
            'group' => esc_html__('Row Options', 'sputnik'),
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_html__('Overlay Opacity', 'sputnik'),
            'param_name' => 'pixoverlayopacity',
            'value' => "0.1",
            'description' => esc_html__('Values 0.1 - 0.9', 'sputnik'),
            'group' => esc_html__('Row Options', 'sputnik'),
        ),
        array(
            'type' => 'dropdown',
            'heading' => esc_html__('Text Color', 'sputnik'),
            'param_name' => 'ptextcolor',
            'value' => array(
                esc_html__('Default', 'sputnik') => '',
                esc_html__('White', 'sputnik') => 'text-white',
                esc_html__('Black', 'sputnik') => 'text-black',
            ),
            'description' => esc_html__("Text Color", 'sputnik'),
            'group' => esc_html__('Row Options', 'sputnik'),
        ),
        array(
            'type' => 'dropdown',
            'heading' => esc_html__('To use 2 background color option (left and right bg color)', 'sputnik'),
            'param_name' => 'twobgcolor',
            'value' => array(
                esc_html__('No', 'sputnik') => 'no',
                esc_html__('Yes', 'sputnik') => 'yes',
            ),
            'description' => esc_html__('On/off 2 bg color option', 'sputnik'),
            'group' => esc_html__('Row Options', 'sputnik'),
        ),
        array(
            'type' => 'colorpicker',
            'class' => '',
            'heading' => esc_html__('Left Background', 'sputnik'),
            'param_name' => 'pixrow_left_bg',
            'value' => '#f5f5f5',
            'description' => esc_html__('Select left bg color. Left column should have the same background for correct display on small screens.', 'sputnik'),
            'group' => esc_html__('Row Options', 'sputnik'),
        ),
        array(
            'type' => 'colorpicker',
            'class' => '',
            'heading' => esc_html__('Right Background', 'sputnik'),
            'param_name' => 'pixrow_right_bg',
            'value' => '#00c0e2',
            'description' => esc_html__('Select right bg color. Right column should have the same background for correct display on small screens', 'sputnik'),
            'group' => esc_html__('Row Options', 'sputnik'),
        ),
    );

    vc_add_params('vc_row', $attributes);

    /** Class for repeated fields */
    class WPBakeryShortCode_Repeater_Params extends WPBakeryShortCode
    {
        public static function convertAttributesToArray($atts)
        {
            if (isset($atts['values']) && strlen($atts['values']) > 0) {
                $values = vc_param_group_parse_atts($atts['values']);
                if (!is_array($values)) {
                    $temp = explode(',', $atts['values']);
                    $paramValues = array();
                    foreach ($temp as $value) {
                        $data = explode('|', $value);
                        $colorIndex = 2;
                        $newLine = array();
                        $newLine['value'] = isset($data[0]) ? $data[0] : 0;
                        $newLine['label'] = isset($data[1]) ? $data[1] : '';
                        if (isset($data[1]) && preg_match('/^\d{1,3}\%$/', $data[1])) {
                            $colorIndex += 1;
                            $newLine['value'] = (float)str_replace('%', '', $data[1]);
                            $newLine['label'] = isset($data[2]) ? $data[2] : '';
                        }
                        if (isset($data[$colorIndex])) {
                            $newLine['customcolor'] = $data[$colorIndex];
                        }
                        $paramValues[] = $newLine;
                    }
                    $atts['values'] = urlencode(json_encode($paramValues));
                }
            }

            return $atts;
        }
    }

    $progress_list_items_params = array(
        array(
            "type" => "textfield",
            "heading" => __("Quantity", "sputnik"),
            "param_name" => "quantity",
        ),
        array(
            "type" => "textfield",
            "heading" => __("Title", "sputnik"),
            "param_name" => "title",
        ),
    );
    $params = array_merge($progress_list_items_params, sputnik_get_vc_icons($vc_icons_data));
    vc_map(
        array(
            'name' => __('Section progress list', 'sputnik'),
            'base' => 'section_progress_list',
            'category' => __('Sputnik', 'sputnik'),
            'params' => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Style', 'sputnik'),
                    'param_name' => 'style',
                    'value' => array('A' => 'a', 'B' => 'b'),
                ),
                array(
                    'type' => 'param_group',
                    'heading' => __('Progress list items', 'sputnik'),
                    'param_name' => 'progress_list_items',
                    'params' => $params,
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Section_Progress_List extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    $advantages_items_params = array(
        array(
            "type" => "textfield",
            "heading" => __("Title", "sputnik"),
            "param_name" => "title",
        ),
        array(
            "type" => "textarea",
            "heading" => __("Text", "sputnik"),
            "param_name" => "text",
        ),
        array(
            "type" => "vc_link",
            "heading" => __("Button link", "sputnik"),
            "param_name" => "button_link",
        ),
    );
    $params = array_merge($advantages_items_params, sputnik_get_vc_icons($vc_icons_data));
    vc_map(
        array(
            'name' => __('Block advantages', 'sputnik'),
            'base' => 'block_advantages',
            'category' => __('Sputnik', 'sputnik'),
            'params' => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Style', 'sputnik'),
                    'param_name' => 'style',
                    'value' => array('A', 'B', 'C', 'D', 'E'),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __('Align', 'sputnik'),
                    'param_name' => 'align',
                    'value' => array('left', 'right'),
                    'dependency' => array(
                        'element' => 'style',
                        'value' => 'A',
                    ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __('Lifted', 'sputnik'),
                    'param_name' => 'lifted',
                    'value' => array('yes' => 'section-advantages-1_block-top', 'no' => 'section-advantages-1_mod-a'),
                    'dependency' => array(
                        'element' => 'style',
                        'value' => 'D',
                    ),
                ),
                array(
                    'type' => 'param_group',
                    'heading' => __('Advantages items', 'sputnik'),
                    'param_name' => 'advantages_items',
                    'params' => $params,
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Advantages extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    vc_map(
        array(
            'name' => __('Block accordion', 'sputnik'),
            'base' => 'block_accordion',
            'category' => __('Sputnik', 'sputnik'),
            'params' => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Color', 'sputnik'),
                    'param_name' => 'color',
                    'value' => array('Black' => '', 'White' => 'accordion_light'),
                ),
                array(
                    'type' => 'param_group',
                    'heading' => __('Panels', 'sputnik'),
                    'param_name' => 'panels',
                    'description' => __('Enter title and text for accordion panel', 'sputnik'),
                    'params' => array(
                        array(
                            "type" => "textfield",
                            "heading" => __("Title", "sputnik"),
                            "param_name" => "title",
                        ),
                        array(
                            "type" => "textarea",
                            "heading" => __("Text", "sputnik"),
                            "param_name" => "text",
                        ),
                        array(
                            'type' => 'checkbox',
                            'heading' => __('Default', 'sputnik'),
                            'param_name' => 'default',
                            'description' => __('Whether panel opened on load or not', 'sputnik'),
                            'value' => '',
                        ),
                    ),
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Accordion extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    vc_map(
        array(
            'name' => __('Section latest news', 'sputnik'),
            'base' => 'section_latest_news',
            'category' => __('Sputnik', 'sputnik'),    
            'description' => __('Display last 4 posts', 'sputnik'),
            'params' => array(
                array(
                    "type" => "vc_link",
                    "heading" => __("Button link", "sputnik"),
                    "param_name" => "button_link",
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Section_Latest_News extends WPBakeryShortCode
        {

        }
    }

    vc_map(
        array(
            'name' => __('Block testimonials slider', 'sputnik'),
            'base' => 'block_testimonials_slider',
            'category' => __('Sputnik', 'sputnik'),
            'params' => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Color', 'sputnik'),
                    'param_name' => 'color',
                    'value' => array('Black' => '', 'White' => 'b-reviews-1_color_white'),
                ),
                array(
                    'type' => 'param_group',
                    'heading' => __('Slides', 'sputnik'),
                    'param_name' => 'slides',
                    'params' => array(
                        array(
                            "type" => "textfield",
                            "heading" => __("Name", 'sputnik'),
                            "param_name" => "name",
                        ),
                        array(
                            "type" => "textfield",
                            "heading" => __("Position", 'sputnik'),
                            "param_name" => "position",
                        ),
                        array(
                            "type" => "textfield",
                            "heading" => __("Title", "sputnik"),
                            "param_name" => "title",
                        ),
                        array(
                            "type" => "textarea",
                            "heading" => __("Text", "sputnik"),
                            "param_name" => "text",
                        ),
                        array(
                            'type' => 'attach_image',
                            'heading' => __('Image', 'sputnik'),
                            'param_name' => 'image',
                            'value' => '',
                            'description' => __('Select image from media library.', 'sputnik')
                        ),
                    ),
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Testimonials_Slider extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    $contacts_params = array(
        array(
            "type" => "textfield",
            "heading" => __("Title", "sputnik"),
            "param_name" => "title",
        ),
        array(
            "type" => "textarea",
            "heading" => __("Text", "sputnik"),
            "param_name" => "text",
        ),
    );
    $params = array_merge($contacts_params, sputnik_get_vc_icons($vc_icons_data));
    vc_map(
        array(
            'name' => __('Section contacts', 'sputnik'),
            'base' => 'section_contacts',
            'category' => __('Sputnik', 'sputnik'),
            'params' => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Style', 'sputnik'),
                    'param_name' => 'style',
                    'value' => array('A', 'B'),
                ),
                array(
                    'type' => 'param_group',
                    'heading' => __('Items', 'sputnik'),
                    'param_name' => 'items',
                    'params' => $params,
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Section_Contacts extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    vc_map(
        array(
            'name' => __('Section team slider', 'sputnik'),
            'base' => 'section_team_slider',
            'category' => __('Sputnik', 'sputnik'),
            'params' => array(
                array(
                    'type' => 'param_group',
                    'heading' => __('Slides', 'sputnik'),
                    'param_name' => 'slides',
                    'params' => array(
                        array(
                            "type" => "textfield",
                            "heading" => __("Name", 'sputnik'),
                            "param_name" => "name",
                        ),
                        array(
                            "type" => "textfield",
                            "heading" => __("Position", 'sputnik'),
                            "param_name" => "position",
                        ),
                        array(
                            "type" => "textarea",
                            "heading" => __("Text", "sputnik"),
                            "param_name" => "text",
                        ),
                        array(
                            'type' => 'attach_image',
                            'heading' => __('Image', 'sputnik'),
                            'param_name' => 'image',
                            'value' => '',
                            'description' => __('Select image from media library.', 'sputnik')
                        ),
                        array(
                            "type" => "vc_link",
                            "heading" => __("Twitter link", "sputnik"),
                            "param_name" => "twitter_link",
                        ),
                        array(
                            "type" => "vc_link",
                            "heading" => __("Facebook link", "sputnik"),
                            "param_name" => "facebook_link",
                        ),
                        array(
                            "type" => "vc_link",
                            "heading" => __("Google plus link", "sputnik"),
                            "param_name" => "google_plus_link",
                        ),
                        array(
                            "type" => "vc_link",
                            "heading" => __("Instaram link", "sputnik"),
                            "param_name" => "instagram_link",
                        ),
                    ),
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Section_Team_Slider extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    vc_map(
        array(
            'name' => __('Section brand photos', 'sputnik'),
            'base' => 'section_brand_photos',
            'category' => __('Sputnik', 'sputnik'),
            'params' => array(
                array(
                    'type' => 'attach_images',
                    'heading' => __('Images', 'sputnik'),
                    'param_name' => 'images',
                    'value' => '',
                    'description' => __('Select images from media library.', 'sputnik')
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Section_Brand_Photos extends WPBakeryShortCode
        {

        }
    }


    vc_map(
        array(
            'name' => esc_html__('Section history', 'sputnik'),
            'base' => 'section_history',
            'category' => esc_html__('Sputnik', 'sputnik'),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'heading' => __('Button text', 'sputnik'),
                    'param_name' => 'button_text',
                ),
                array(
                    'type' => 'param_group',
                    'heading' => __('History items', 'sputnik'),
                    'param_name' => 'history_items',
                    'params' => array(
                        array(
                            "type" => "textfield",
                            "heading" => __("Date", "sputnik"),
                            "param_name" => "date",
                        ),
                        array(
                            "type" => "textfield",
                            "heading" => __("Title", "sputnik"),
                            "param_name" => "title",
                        ),
                        array(
                            "type" => "textarea",
                            "heading" => __("Text", "sputnik"),
                            "param_name" => "text",
                        ),
                    ),
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Section_History extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    $contacts_params = array(
        array(
            "type" => "textarea",
            "heading" => __("Text", "sputnik"),
            "param_name" => "text",
        ),
    );
    $params = array_merge($contacts_params, sputnik_get_vc_icons($vc_icons_data));
    vc_map(
        array(
            'name' => __('Block contacts', 'sputnik'),
            'base' => 'block_contacts',
            'category' => __('Sputnik', 'sputnik'),
            'params' => array(
                array(
                    'type' => 'param_group',
                    'heading' => __('Main items', 'sputnik'),
                    'param_name' => 'main_items',
                    'params' => $params,
                ),
                array(
                    'type' => 'param_group',
                    'heading' => __('Items', 'sputnik'),
                    'param_name' => 'items',
                    'params' => array(
                        array(
                            "type" => "textfield",
                            "heading" => __("Title", "sputnik"),
                            "param_name" => "title",
                        ),
                        array(
                            "type" => "textfield",
                            "heading" => __("Subtitle", "sputnik"),
                            "param_name" => "subtitle",
                        ),
                        array(
                            "type" => "textarea",
                            "heading" => __("Text", "sputnik"),
                            "param_name" => "text",
                        ),
                    ),
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Contacts extends WPBakeryShortCode_Repeater_Params
        {

        }
    }



    vc_map(
        array(
            'name' => esc_html__('Block pricing', 'sputnik'),
            'base' => 'block_pricing',
            'category' => esc_html__('Sputnik', 'sputnik'),
            'params' => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Style', 'sputnik'),
                    'param_name' => 'style',
                    'value' => array('A', 'B', 'C'),
                ),
                array(
                    "type" => "textfield",
                    "heading" => __("Price title", "sputnik"),
                    "param_name" => "price_title",
                ),
                array(
                    "type" => "textfield",
                    "heading" => __("Price number", "sputnik"),
                    "param_name" => "price_number",
                ),
                array(
                    "type" => "textfield",
                    "heading" => __("Price label", "sputnik"),
                    "param_name" => "price_label",
                ),
                array(
                    "type" => "textfield",
                    "heading" => __("Box icons number", "sputnik"),
                    "param_name" => "box_icons_number",
                ),
                array(
                    'type' => 'vc_link',
                    'heading' => __('Button link', 'sputnik'),
                    'param_name' => 'button_link',
                ),
                array(
                    'type' => 'param_group',
                    'heading' => __('List items', 'sputnik'),
                    'param_name' => 'list_items',
                    'params' => array(
                        array(
                            "type" => "textfield",
                            "heading" => __("Text", "sputnik"),
                            "param_name" => "text",
                        ),
                    ),
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Pricing extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    vc_map(
        array(
            'name' => __('Block title', 'sputnik'),
            'base' => 'block_title',
            'category' => __('Sputnik', 'sputnik'),
            'params' => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Style', 'sputnik'),
                    'param_name' => 'style',
                    'value' => array('A', 'B'),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __('Color', 'sputnik'),
                    'param_name' => 'color',
                    'value' => array('Black' => '', 'White' => 'color-white'),
//                    'dependency' => array(
//                        'element' => 'style',
//                        'value' => 'B',
//                    ),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Title', 'sputnik'),
                    'param_name' => 'title',
                ),
                array(
                    'type' => 'textarea',
                    'heading' => __('Subtitle', 'sputnik'),
                    'param_name' => 'subtitle',
                    'dependency' => array(
                        'element' => 'style',
                        'value' => 'A',
                    ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __('Align', 'sputnik'),
                    'param_name' => 'align',
                    'value' => array('left' => '', 'center' => 'text-center'),
                    'dependency' => array(
                        'element' => 'style',
                        'value' => 'A',
                    ),
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Title extends WPBakeryShortCode
        {

        }
    }

    $contacts_params = array(
        array(
            "type" => "textfield",
            "heading" => __("Text", "sputnik"),
            "param_name" => "text",
        ),
    );
    $params = array_merge($contacts_params, sputnik_get_vc_icons($vc_icons_data));
    vc_map(
        array(
            'name' => __('Block footer contacts', 'sputnik'),
            'base' => 'Block_footer_contacts',
            'category' => __('Sputnik', 'sputnik'),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'heading' => __('Text', 'sputnik'),
                    'param_name' => 'text',
                ),
                array(
                    'type' => 'param_group',
                    'heading' => __('Items', 'sputnik'),
                    'param_name' => 'items',
                    'params' => $params,
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Footer_Contacts extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    vc_map(
        array(
            "name" => __("Block footer services", "sputnik"),
            "base" => "block_footer_services",
            'category' => __('Sputnik', 'sputnik'),
            "params" => array(
                array(
                    "type" => "textfield",
                    "heading" => __("Add menu in \"Appearance\" -> \"Menus\" to areas served location", "sputnik"),
                    "param_name" => "hidden",
                    "holder" => "hidden input",
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Footer_Services extends WPBakeryShortCode
        {
        }
    }

    vc_map(
        array(
            "name" => __("Block footer menu", "sputnik"),
            "base" => "block_footer_menu",
            'category' => __('Sputnik', 'sputnik'),
            "params" => array(
                array(
                    "type" => "textfield",
                    "heading" => __("Add menu in \"Appearance\" -> \"Menus\" to areas served location", "sputnik"),
                    "param_name" => "hidden",
                    "holder" => "hidden input",
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Footer_Menu extends WPBakeryShortCode
        {
        }
    }

    vc_map(
        array(
            'name' => __('Block footer gallery', 'sputnik'),
            'base' => 'block_footer_gallery',
            'category' => __('Sputnik', 'sputnik'),
            'params' => array(
                array(
                    'type' => 'attach_images',
                    'heading' => __('Images', 'sputnik'),
                    'param_name' => 'images',
                    'value' => '',
                    'description' => __('Select images from media library.', 'sputnik')
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Footer_Gallery extends WPBakeryShortCode
        {

        }
    }

    $categories = get_terms(
        array(
            'taxonomy' => 'services_category',
            'hide_empty' => 1,
        )
    );

    $categories_array = array();

    foreach ($categories as $category){
        $categories_array[ $category->name ] = $category->term_id;
    }

    $categories_params = array(
        array(
            "type" => "dropdown",
            "heading" => __("Category", "sputnik"),
            "param_name" => "category_id",
            'value' => $categories_array,
        ),
        array(
            'type' => 'attach_image',
            'heading' => __('Image', 'sputnik'),
            'param_name' => 'image',
            'value' => '',
            'description' => __('Select image from media library.', 'sputnik')
        ),
    );
    vc_map(
        array(
            'name' => __('Block services categories slider', 'sputnik'),
            'base' => 'block_services_categories_slider',
            'category' => __('Sputnik', 'sputnik'),
            'params' => array(
                array(
                    'type' => 'param_group',
                    'heading' => __('Categories', 'sputnik'),
                    'param_name' => 'categories',
                    'params' => $categories_params,
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode_Repeater_Params')) {
        class WPBakeryShortCode_Block_Services_Categories_Slider extends WPBakeryShortCode_Repeater_Params
        {

        }
    }




    vc_map(
        array(
            'name' => __('Block blockquote', 'sputnik'),
            'base' => 'block_blockquote',
            'category' => __('Sputnik', 'sputnik'),
            'params' => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Style', 'sputnik'),
                    'param_name' => 'style',
                    'value' => array('A', 'B'),
                ),
                array(
                    'type' => 'textarea',
                    'heading' => __('Text', 'sputnik'),
                    'param_name' => 'text',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Name', 'sputnik'),
                    'param_name' => 'name',
                    'dependency' => array(
                        'element' => 'style',
                        'value' => array('A'),
                    ),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Position', 'sputnik'),
                    'param_name' => 'position',
                    'dependency' => array(
                        'element' => 'style',
                        'value' => array('A'),
                    ),
                ),
                array(
                    'type' => 'attach_image',
                    'heading' => __('Image', 'sputnik'),
                    'param_name' => 'image',
                    'value' => '',
                    'description' => __('Select image from media library.', 'sputnik'),
                    'dependency' => array(
                        'element' => 'style',
                        'value' => array('A'),
                    ),
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Block_Blockquote extends WPBakeryShortCode
        {

        }
    }


    $categories_params = array(
        array(
            "type" => "dropdown",
            "heading" => __("Category", "sputnik"),
            "param_name" => "category_id",
            'value' => $categories_array,
        ),
        array(
            'type' => 'attach_image',
            'heading' => __('Image', 'sputnik'),
            'param_name' => 'image',
            'value' => '',
            'description' => __('Select image from media library.', 'sputnik')
        ),
    );
    $params = array_merge($categories_params, sputnik_get_vc_icons($vc_icons_data));
    vc_map(
        array(
            'name' => __('Section categories', 'sputnik'),
            'base' => 'section_categories',
            'category' => __('Sputnik', 'sputnik'),
            'params' => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Style', 'sputnik'),
                    'param_name' => 'style',
                    'value' => array('A', 'B'),
                ),
                array(
                    'type' => 'param_group',
                    'heading' => __('Categories', 'sputnik'),
                    'param_name' => 'categories',
                    'params' => $params,
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode_Repeater_Params')) {
        class WPBakeryShortCode_Section_Categories extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    vc_map(
        array(
            'name' => __('Section discount', 'sputnik'),
            'base' => 'section_discount',
            'category' => __('Sputnik', 'sputnik'),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'heading' => __('Title', 'sputnik'),
                    'param_name' => 'title',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Description', 'sputnik'),
                    'param_name' => 'description',
                ),
                array(
                    "type" => "vc_link",
                    "heading" => __("Button link", "sputnik"),
                    "param_name" => "button_link",
                ),
                array(
                    'type' => 'param_group',
                    'heading' => __('List_items', 'sputnik'),
                    'param_name' => 'list_items',
                    'params' => array(
                        array(
                            'type' => 'textfield',
                            'heading' => __('Text', 'sputnik'),
                            'param_name' => 'text',
                        ),
                    ),
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode_Repeater_Params')) {
        class WPBakeryShortCode_Section_Discount extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    $branches_params = array(
        array(
            "type" => "textfield",
            "heading" => __("Text", "sputnik"),
            "param_name" => "text",
        ),
    );
    $items_params = array_merge($contacts_params, sputnik_get_vc_icons($vc_icons_data));
    $params = array_merge(
        array(
            array(
                'type' => 'param_group',
                'heading' => __('Items', 'sputnik'),
                'param_name' => 'items',
                'params' => $items_params,
            ),
            array(
                "type" => "textfield",
                "heading" => __("Title", "sputnik"),
                "param_name" => "title",
            ),
        ), sputnik_get_vc_icons($vc_icons_data)
    );

    vc_map(
        array(
            'name' => __('Section branches', 'sputnik'),
            'base' => 'section_branches',
            'category' => __('Sputnik', 'sputnik'),
            'params' => $params,
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Section_Branches extends WPBakeryShortCode_Repeater_Params
        {

        }
    }

    // Block unordered list
    vc_map(
        array(
            'name' => __('Block unordered list', 'sputnik'),
            'base' => 'block_unordered_list',
            'category' => __('Sputnik', 'sputnik'),
            'params' => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Style', 'sputnik'),
                    'param_name' => 'style',
                    'value' => array(
                        '1' => 'list-mark-1',
                        '2' => 'list-mark-2',
                        '3' => 'list-mark-3',
                        '4' => 'list-mark-4',
                        '5' => 'list-mark-5',
                        '9' => 'list-num list-num-1',
                    ),
                ),
                array(
                    'type' => 'checkbox',
                    'heading' => __('Bold', 'sputnik'),
                    'param_name' => 'bold',
                    'value' => 'list_mod-a',
                ),
                array(
                    'type' => 'param_group',
                    'heading' => __('Items', 'sputnik'),
                    'param_name' => 'items',
                    'params' => array(
                        array(
                            'type' => 'textarea',
                            'heading' => __('Text', 'sputnik'),
                            'param_name' => 'text',
                        ),
                    ),
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode_Repeater_Params')) {
        class WPBakeryShortCode_Block_Unordered_List extends WPBakeryShortCode_Repeater_Params
        {

        }
    }
//end Block unordered list

    vc_map(
        array(
            'name' => __('Section strategy and mission', 'sputnik'),
            'base' => 'section_strategy_and_mission',
            'category' => __('Sputnik', 'sputnik'),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'heading' => __('Title', 'sputnik'),
                    'param_name' => 'title_1',
                ),
                array(
                    'type' => 'textarea',
                    'heading' => __('Text', 'sputnik'),
                    'param_name' => 'text_1',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Title', 'sputnik'),
                    'param_name' => 'title_2',
                ),
                array(
                    'type' => 'textarea',
                    'heading' => __('Text', 'sputnik'),
                    'param_name' => 'text_2',
                ),
            ),
        )
    );

    if (class_exists('WPBakeryShortCode')) {
        class WPBakeryShortCode_Section_Strategy_And_Mission extends WPBakeryShortCode
        {

        }
    }

}