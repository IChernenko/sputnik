<?php

if (!function_exists('sputnik_setup')) :

    function sputnik_setup()
    {

        // This theme styles the visual editor with editor-style.css to match the theme style.
        add_editor_style('css/editor-style.css');

        // removes detailed login error information for security
        add_filter('login_errors', create_function('$a', "return null;"));

        //Loading theme textdomain
        load_theme_textdomain('sputnik', get_template_directory() . '/languages');

        // This theme uses post thumbnails
        if (function_exists('add_theme_support')) {
            add_theme_support('post-thumbnails');
            add_image_size('sputnik-post-thumb-large', 1170, 555, true); // for blog full widht
            add_image_size('sputnik-post-thumb-middle', 800, 380, true); // for blog with sidebar
            add_image_size('sputnik-post-thumb-small', 125, 95, true); // for blog widget
            add_image_size('sputnik-post-thumb-home', 270, 230, true); // for home blog block
            add_image_size('sputnik-services-thumb', 370, 200, true);
            add_image_size('sputnik-review-thumb', 40, 40, true); //for home testimonials img
        }

        // support title-tag for Wordpress 4.1+
        add_theme_support('title-tag');

        // Add default posts and comments RSS feed links to head
        add_theme_support('automatic-feed-links');

        // custom menu support
        add_theme_support('menus');
        if (function_exists('register_nav_menus')) {
            register_nav_menus(
                array(
                    'primary_menu' => __('Primary Menu', 'sputnik'),
                    'services_footer_menu' => __('Services footer menu', 'sputnik'),
                    'footer_menu' => __('Footer Menu', 'sputnik'),
                )
            );
        }

        // This theme supports a variety of post formats.
        add_theme_support('post-formats', array('gallery', 'quote', 'video'));

        if (!isset($content_width)) {
            $content_width = 1200;
        }

        // ADD SUPPORT FOR WORDPRESS MENU ************/
        add_theme_support('menus');

        $args = array(
            'flex-width' => true,
            'width' => 350,
            'flex-height' => true,
            'height' => 'auto',
            'default-image' => get_template_directory_uri() . '/images/logo.jpg'
        );

        add_theme_support('custom-header', $args);


        $args = array(
            'default-color' => 'FFFFFF'
        );

        add_theme_support('custom-background', $args);

        // WooCommerce support
        add_theme_support('woocommerce');

    }
endif;// sputnik_setup
add_action('after_setup_theme', 'sputnik_setup');


add_filter('nav_menu_css_class', 'sputnik_special_nav_class', 10, 2);
function sputnik_special_nav_class($classes, $item)
{
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'active ';
    }
    return $classes;
}

// edit password form
add_filter('the_password_form', 'sputnik_custom_password_form');
function sputnik_custom_password_form()
{
    global $post;
    $label = 'pwbox-' . (empty($post->ID) ? rand() : $post->ID);
    $o = '<p>' . __("This post is password protected. To view it please enter your password below:", 'sputnik') . '</p><div class="row"><div class="col-md-6 col-md-offset-3"><form class="lock" action=" ' . esc_url(site_url('wp-login.php?action=postpass', 'login_post')) . ' " method="post">
	<input type="text" id="' . $label . '" name="post_password" placeholder="' . __("Password...", 'sputnik') . '"><button><i class="icon-lock"></i></button>
	</form></div></div>
	';
    return $o;
}

