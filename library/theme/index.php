<?php
	/**  Theme_index  **/

	require_once( get_template_directory() . '/library/theme/styles_scripts.php');
	require_once( get_template_directory() . '/library/theme/functions.php');
	require_once( get_template_directory() . '/library/theme/meta-boxes.php');
	require_once( get_template_directory() . '/library/theme/setup.php');
	require_once( get_template_directory() . '/library/theme/vc_templates.php');
	require_once( get_template_directory() . '/library/theme/customizer.php');