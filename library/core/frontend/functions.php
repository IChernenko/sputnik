<?php

	function sputnik_js_vars(){

		$vars = '';

		$_js = apply_filters('sputnik_js_vars',$vars);

		echo esc_js($_js);

	}


	function sputnik_css_vars(){

		$css = '';
		$header_color  = sputnik_get_option('header_all_color');
		$footer_color  = sputnik_get_option('footer_all_color');
		if ($footer_color){

			$css .= '.footer-top { background-color: '.$header_color.' !important}';
			$css .= '.footer-bottom { background-color: '.$header_color.' !important}';
		}


		if ($header_color)
			$css .= '.header-top { background-color: '.$header_color.' !important}';
		$css .= '';

		echo esc_html($css);
	}


	function sputnik_get_theme_header(){
		$headerType = 1;
		global $wp_query;


		$pix_header_type_page = get_post_meta(get_the_ID(), 'pix_page_header_type', true);
		if ($pix_header_type_page && $pix_header_type_page != 'global'){
			$headerType = (int)$pix_header_type_page;
		}else{
			if (sputnik_get_option('header_settings_type')){
				$headerType = sputnik_get_option('header_settings_type');
			}
		}


		if (isset($wp_query->queried_object->ID)){
			$GLOBALS['sputnik_footer_type_page'] = get_post_meta($wp_query->queried_object->ID, 'pix_page_footer_staticblock', true);
		}else{
			$GLOBALS['sputnik_footer_type_page'] = get_post_meta(get_the_ID(), 'pix_page_footer_staticblock', true);
		}

		$headerFile = sputnikMainPath . '/templates/header/types/header' . $headerType . '.php';
		if (file_exists($headerFile))
			include_once( $headerFile );
	}


	function tempslname_woo_get_page_id(){

		global $post;

		if( is_shop() || is_product_category() || is_product_tag() )
			$id = get_option( 'woocommerce_shop_page_id' );
		elseif( is_product() || !empty($post->ID) )
			$id = $post->ID;
		else
			$id = 0;
		return $id;
	}


	function sputnik_checkAvailableJsToPage($types){
		foreach($types as $type){
			if (function_exists('is_product') && is_product() && $type == 'product'){
				return true;
			}
		}
		return false;
	}

	function sputnik_get_staticblock_content($blockId){

		if (is_array($blockId)){
			// SORT ORDER

			// Prepare sortable array
			$_blocks = array();

			foreach($blockId as $bId){
				if ($bId == 'global'){
					$bId = sputnik_get_option('footer_block');
				}
				$_block = get_post($bId);
				$_blocks[$_block->menu_order][] = $_block;
			}



			foreach ($_blocks as $blockMenuOrder){
				foreach($blockMenuOrder as $block) {
					$shortcodes_custom_css = get_post_meta($block->ID, '_wpb_shortcodes_custom_css', true);
					if (!empty($shortcodes_custom_css)) {
						echo '<style type="text/css" data-type="vc_shortcodes-custom-css">';
						echo esc_html($shortcodes_custom_css);
						echo '</style>';
					}

					echo apply_filters('the_content', $block->post_content);
				}
			}
		}else{

			if ($blockId == 'global'){
				return '';
			}


			$block = get_post($blockId);
			$shortcodes_custom_css = get_post_meta( $blockId, '_wpb_shortcodes_custom_css', true );
			if ( ! empty( $shortcodes_custom_css ) ) {
				echo '<style type="text/css" data-type="vc_shortcodes-custom-css">';
				echo esc_html($shortcodes_custom_css);
				echo '</style>';
			}
			echo apply_filters('the_content', $block->post_content);
		}



	}


	function sputnik_get_staticblock_option_array(){

		$args = array(
			'post_type'        => 'staticblocks',
			'post_status'      => 'publish',
		);
		$staticBlocks = array();
		$staticBlocks[] = 'Select block';
		$staticBlocksData = get_posts( $args );
		foreach($staticBlocksData as $_block){
			$staticBlocks[$_block->ID] = $_block->post_title;
		}
		return $staticBlocks;
	}






?>