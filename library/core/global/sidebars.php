<?php


function sputnik_init_sidebars() {
	if ( function_exists( 'register_sidebar' ) ) {

		register_sidebar( array(
			'name' => 'WP Default Sidebar',
			'id'	=> 'sidebar-1',
			'before_widget' => '<div id="%1$s" class="widget sidebar-item %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		));

		register_sidebar( array(
			'name' => 'Blog Sidebar',
			'id' => 'global-sidebar-1',
			'before_widget' => '<div id="%1$s" class="widget sidebar-item %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		));

		register_sidebar( array(
			'name' => 'Service sidebar',
			'id'	=> 'service-sidebar-1',
			'before_widget' => '<div id="%1$s" class="widget sidebar-item %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		));

		register_sidebar( array(
			'name' => 'Custom Area',
			'id'	=> 'custom-area-1',
			'before_widget' => '<div id="%1$s" class="widget sidebar-item %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		));

	}
}


add_action( 'widgets_init', 'sputnik_init_sidebars' );