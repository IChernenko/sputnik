<?php


	require_once( get_template_directory() . '/library/core/admin/admin-panel/general.php' );
	require_once( get_template_directory() . '/library/core/admin/admin-panel/style.php' );
	require_once( get_template_directory() . '/library/core/admin/admin-panel/header.php' );
	require_once( get_template_directory() . '/library/core/admin/admin-panel/footer.php' );
	require_once( get_template_directory() . '/library/core/admin/admin-panel/services.php' );
	require_once( get_template_directory() . '/library/core/admin/admin-panel/blog.php' );
	require_once( get_template_directory() . '/library/core/admin/admin-panel/css_animation.php' );
	require_once( get_template_directory() . '/library/core/admin/admin-panel/sanitizer.php' );




	function sputnik_customize_register( $wp_customize ) {


		/** GENERAL SETTINGS **/
		sputnik_customize_general_tab($wp_customize,'sputnik');


		/** STYLE SECTION **/

		sputnik_customize_style_tab($wp_customize, 'sputnik');


		/** HEADER SECTION **/

		sputnik_customize_header_tab($wp_customize,'sputnik');


		/** FOOTER SECTION **/

		sputnik_customize_footer_tab($wp_customize,'sputnik');


		/** SERVICES SECTION **/

		sputnik_customize_services_tab($wp_customize,'sputnik');


		/** BLOG SECTION **/

		sputnik_customize_blog_tab($wp_customize,'sputnik');


		/** CSS ANIMATION SECTION **/

		sputnik_customize_css_animation_tab($wp_customize, 'sputnik');


		/** Remove unused sections */

		$removedSections = apply_filters('sputnik_admin_customize_removed_sections', array('header_image','background_image'));
		foreach ($removedSections as $_sectionName){
			$wp_customize->remove_section($_sectionName);
		}

	}


	add_action( 'customize_register', 'sputnik_customize_register' );