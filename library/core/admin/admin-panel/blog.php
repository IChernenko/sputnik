<?php

function sputnik_customize_blog_tab($wp_customize, $theme_name) {

	$wp_customize->add_section( 'sputnik_blog_settings' , array(
		'title'      => esc_html__( 'Blog', 'sputnik' ),
		'priority'   => 12,
	) );


	$wp_customize->add_setting( 'sputnik_blog_settings_sidebar_type' , array(
		'default'     => '2',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_sidebar_blog_type'
	) );

	$wp_customize->add_control(
		'sputnik_blog_settings_sidebar_type',
		array(
			'label'    => esc_html__( 'Blog sidebar type', 'sputnik' ),
			'section'  => 'sputnik_blog_settings',
			'settings' => 'sputnik_blog_settings_sidebar_type',
			'description' => esc_html__( 'Select sidebar type for blog pages (not for static page) - all posts, arhive, category, etc.', 'sputnik' ),
			'type'     => 'select',
			'choices'  => array(
				'1' => esc_html__( 'Full width', 'sputnik' ),
				'2' => esc_html__( 'Right Sidebar', 'sputnik' ),
				'3' => esc_html__( 'Left Sidebar', 'sputnik' ),
			),
			'priority' => 5
		)
	);

	$wp_customize->add_setting( 'sputnik_blog_settings_sidebar_content' , array(
		'default'     => 'sidebar-1',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_sidebar_blog_content'
	) );

	$wp_customize->add_control(
		'sputnik_blog_settings_sidebar_content',
		array(
			'label'    => esc_html__( 'Blog sidebar content', 'sputnik' ),
			'section'  => 'sputnik_blog_settings',
			'settings' => 'sputnik_blog_settings_sidebar_content',
			'description' => esc_html__( 'Select sidebar content for blog pages (not for static page) - all posts, arhive, category, etc.', 'sputnik' ),
			'type'     => 'select',
			'choices'  => array(
				'sidebar-1' => esc_html__( 'WP Default Sidebar', 'sputnik' ),
				'global-sidebar-1' => esc_html__( 'Blog Sidebar', 'sputnik' ),
				'portfolio-sidebar-1' => esc_html__( 'Portfolio Sidebar', 'sputnik' ),
				'custom-area-1' => esc_html__( 'Custom Area', 'sputnik' ),
			),
			'priority' => 10
		)
	);

	$wp_customize->add_setting( 'sputnik_blog_settings_readmore' , array(
		'default'     => esc_html__('Read more', 'sputnik' ),
		'transport'   => 'refresh',
		'sanitize_callback'=>'sputnik_sanitize_text',
	) );

	$wp_customize->add_control(
		'sputnik_blog_settings_readmore',
		array(
			'label'    => esc_html__( 'Read More button text', 'sputnik' ),
			'section'  => 'sputnik_blog_settings',
			'settings' => 'sputnik_blog_settings_readmore',
			'type'     => 'textfield',
			'priority' => 20
		)
	);

	$wp_customize->add_setting( 'sputnik_blog_show_author_block' , array(
		'default'     => 'on',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_onoff'
	) );

	$wp_customize->add_control(
		'sputnik_blog_show_author_block',
		array(
			'label'    => esc_html__( 'Show author block on Page single post', 'sputnik' ),
			'section'  => 'sputnik_blog_settings',
			'settings' => 'sputnik_blog_show_author_block',
			'description' => esc_html__( 'Show author block on/off.', 'sputnik' ),
			'type'     => 'select',
			'choices'  => array(
				'on'  => esc_html__( 'On', 'sputnik' ),
				'off'  => esc_html__( 'Off', 'sputnik' ),
			),
			'priority'   => 30
		)
	);

	$wp_customize->add_setting( 'sputnik_blog_show_date' , array(
		'default'     => 'on',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_onoff'
	) );

	$wp_customize->add_control(
		'sputnik_blog_show_date',
		array(
			'label'    => esc_html__( 'Show date', 'sputnik' ),
			'section'  => 'sputnik_blog_settings',
			'settings' => 'sputnik_blog_show_date',
			'description' => esc_html__( 'Show date posts on/off.', 'sputnik' ),
			'type'     => 'select',
			'choices'  => array(
				'on'  => esc_html__( 'On', 'sputnik' ),
				'off'  => esc_html__( 'Off', 'sputnik' ),
			),
			'priority'   => 40
		)
	);

	$wp_customize->add_setting( 'sputnik_blog_show_author' , array(
		'default'     => 'on',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_onoff'
	) );

	$wp_customize->add_control(
		'sputnik_blog_show_author',
		array(
			'label'    => esc_html__( 'Show author', 'sputnik' ),
			'section'  => 'sputnik_blog_settings',
			'settings' => 'sputnik_blog_show_author',
			'description' => esc_html__( 'Show author posts on/off.', 'sputnik' ),
			'type'     => 'select',
			'choices'  => array(
				'on'  => esc_html__( 'On', 'sputnik' ),
				'off'  => esc_html__( 'Off', 'sputnik' ),
			),
			'priority'   => 50
		)
	);


}