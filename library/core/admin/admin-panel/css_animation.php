<?php

function sputnik_customize_css_animation_tab($wp_customize, $theme_name) {

	$wp_customize->add_section( 'sputnik_css_animation_settings' , array(
		'title'      => esc_html__( 'Css Animation', 'sputnik' ),
		'priority'   => 16,
	) );

	$sputnik_customize_css_animation =  array(
		'' => esc_html__( 'No', 'sputnik' ),
		'bounce' => esc_html__( 'bounce', 'sputnik' ),
		'flash' => esc_html__( 'flash', 'sputnik' ),
		'pulse' => esc_html__( 'pulse', 'sputnik' ),
		'rubberBand' => esc_html__( 'rubberBand', 'sputnik' ),
		'shake' => esc_html__( 'shake', 'sputnik' ),
		'swing' => esc_html__( 'swing', 'sputnik' ),
		'tada' => esc_html__( 'tada', 'sputnik' ),
		'wobble' => esc_html__( 'wobble', 'sputnik' ),
		'jello' => esc_html__( 'jello', 'sputnik' ),

		'bounceIn' => esc_html__( 'bounceIn', 'sputnik' ),
		'bounceInDown' => esc_html__( 'bounceInDown', 'sputnik' ),
		'bounceInLeft' => esc_html__( 'bounceInLeft', 'sputnik' ),
		'bounceInRight' => esc_html__( 'bounceInRight', 'sputnik' ),
		'bounceInUp' => esc_html__( 'bounceInUp', 'sputnik' ),
		'bounceOut' => esc_html__( 'bounceOut', 'sputnik' ),
		'bounceOutDown' => esc_html__( 'bounceOutDown', 'sputnik' ),
		'bounceOutLeft' => esc_html__( 'bounceOutLeft', 'sputnik' ),
		'bounceOutRight' => esc_html__( 'bounceOutRight', 'sputnik' ),
		'bounceOutUp' => esc_html__( 'bounceOutUp', 'sputnik' ),

		'fadeIn' => esc_html__( 'fadeIn', 'sputnik' ),
		'fadeInDown' => esc_html__( 'fadeInDown', 'sputnik' ),
		'fadeInDownBig' => esc_html__( 'fadeInDownBig', 'sputnik' ),
		'fadeInLeft' => esc_html__( 'fadeInLeft', 'sputnik' ),
		'fadeInLeftBig' => esc_html__( 'fadeInLeftBig', 'sputnik' ),
		'fadeInRight' => esc_html__( 'fadeInRight', 'sputnik' ),
		'fadeInRightBig' => esc_html__( 'fadeInRightBig', 'sputnik' ),
		'fadeInUp' => esc_html__( 'fadeInUp', 'sputnik' ),
		'fadeInUpBig' => esc_html__( 'fadeInUpBig', 'sputnik' ),
		'fadeOut' => esc_html__( 'fadeOut', 'sputnik' ),
		'fadeOutDown' => esc_html__( 'fadeOutDown', 'sputnik' ),
		'fadeOutDownBig' => esc_html__( 'fadeOutDownBig', 'sputnik' ),
		'fadeOutLeft' => esc_html__( 'fadeOutLeft', 'sputnik' ),
		'fadeOutLeftBig' => esc_html__( 'fadeOutLeftBig', 'sputnik' ),
		'fadeOutRight' => esc_html__( 'fadeOutRight', 'sputnik' ),
		'fadeOutRightBig' => esc_html__( 'fadeOutRightBig', 'sputnik' ),
		'fadeOutUp' => esc_html__( 'fadeOutUp', 'sputnik' ),
		'fadeOutUpBig' => esc_html__( 'fadeOutUpBig', 'sputnik' ),

		'flip' => esc_html__( 'flip', 'sputnik' ),
		'flipInX' => esc_html__( 'flipInX', 'sputnik' ),
		'flipInY' => esc_html__( 'flipInY', 'sputnik' ),
		'flipOutX' => esc_html__( 'flipOutX', 'sputnik' ),
		'flipOutY' => esc_html__( 'flipOutY', 'sputnik' ),

		'lightSpeedIn' => esc_html__( 'lightSpeedIn', 'sputnik' ),
		'lightSpeedOut' => esc_html__( 'lightSpeedOut', 'sputnik' ),

		'rotateIn' => esc_html__( 'rotateIn', 'sputnik' ),
		'rotateInDownLeft' => esc_html__( 'rotateInDownLeft', 'sputnik' ),
		'rotateInDownRight'=> esc_html__( 'rotateInDownRight', 'sputnik' ),
		'rotateInUpLeft' => esc_html__( 'rotateInUpLeft', 'sputnik' ),
		'rotateInUpRight'=> esc_html__( 'rotateInUpRight', 'sputnik' ),
		'rotateOut' => esc_html__( 'rotateOut', 'sputnik' ),
		'rotateOutDownLeft' => esc_html__( 'rotateOutDownLeft', 'sputnik' ),
		'rotateOutDownRight' => esc_html__( 'rotateOutDownRight', 'sputnik' ),
		'rotateOutUpLeft' => esc_html__( 'rotateOutUpLeft', 'sputnik' ),
		'rotateOutUpRight' => esc_html__( 'rotateOutUpRight', 'sputnik' ),

		'slideInUp' => esc_html__( 'slideInUp', 'sputnik' ),
		'slideInDown' => esc_html__( 'slideInDown', 'sputnik' ),
		'slideInLeft' => esc_html__( 'slideInLeft', 'sputnik' ),
		'slideInRight' => esc_html__( 'slideInRight', 'sputnik' ),
		'slideOutUp' => esc_html__( 'slideOutUp', 'sputnik' ),
		'slideOutDown' => esc_html__( 'slideOutDown', 'sputnik' ),
		'slideOutLeft' => esc_html__( 'slideOutLeft', 'sputnik' ),
		'slideOutRight' => esc_html__( 'slideOutRight', 'sputnik' ),

		'zoomIn' => esc_html__( 'zoomIn', 'sputnik' ),
		'zoomInDown' => esc_html__( 'zoomInDown', 'sputnik' ),
		'zoomInLeft' => esc_html__( 'zoomInLeft', 'sputnik' ),
		'zoomInRight' => esc_html__( 'zoomInRight', 'sputnik' ),
		'zoomInUp' => esc_html__( 'zoomInUp', 'sputnik' ),
		'zoomOut' => esc_html__( 'zoomOut', 'sputnik' ),
		'zoomOutDown' => esc_html__( 'zoomOutDown', 'sputnik' ),
		'zoomOutLeft' => esc_html__( 'zoomOutLeft', 'sputnik' ),
		'zoomOutRight' => esc_html__( 'zoomOutRight', 'sputnik' ),
		'zoomOutUp' => esc_html__( 'zoomOutUp', 'sputnik' ),

		'hinge' => esc_html__( 'hinge', 'sputnik' ),
		'rollIn' => esc_html__( 'rollIn', 'sputnik' ),
		'rollOut' => esc_html__( 'rollOut', 'sputnik' ),
	);

	$wp_customize->add_setting( 'sputnik_css_animation_settings_header_title' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_animation'
	) );

	$wp_customize->add_control(
		'sputnik_css_animation_settings_header_title',
		array(
			'label'    => esc_html__( 'Header Title Animation', 'sputnik' ),
			'section'  => 'sputnik_css_animation_settings',
			'settings' => 'sputnik_css_animation_settings_header_title',
			'description' => esc_html__( 'Select css animation for Header Title Block.', 'sputnik' ),
			'type'     => 'select',
			'choices'  => $sputnik_customize_css_animation,
			'priority' => 10
		)
	);

	$wp_customize->add_setting( 'sputnik_css_animation_settings_blog' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_animation'
	) );

	$wp_customize->add_control(
		'sputnik_css_animation_settings_blog',
		array(
			'label'    => esc_html__( 'Blog Animation', 'sputnik' ),
			'section'  => 'sputnik_css_animation_settings',
			'settings' => 'sputnik_css_animation_settings_blog',
			'description' => esc_html__( 'Select css animation for blog pages.', 'sputnik' ),
			'type'     => 'select',
			'choices'  => $sputnik_customize_css_animation,
			'priority' => 20
		)
	);

	$wp_customize->add_setting( 'sputnik_css_animation_settings_sidebar' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_animation'
	) );

	$wp_customize->add_control(
		'sputnik_css_animation_settings_sidebar',
		array(
			'label'    => esc_html__( 'Sidebar Animation', 'sputnik' ),
			'section'  => 'sputnik_css_animation_settings',
			'settings' => 'sputnik_css_animation_settings_sidebar',
			'description' => esc_html__( 'Select css animation for Sidebar.', 'sputnik' ),
			'type'     => 'select',
			'choices'  => $sputnik_customize_css_animation,
			'priority' => 30
		)
	);

	$wp_customize->add_setting( 'sputnik_css_animation_settings_services' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_animation'
	) );

	$wp_customize->add_control(
		'sputnik_css_animation_settings_services',
		array(
			'label'    => esc_html__( 'Services Category Page Animation', 'sputnik' ),
			'section'  => 'sputnik_css_animation_settings',
			'settings' => 'sputnik_css_animation_settings_services',
			'description' => esc_html__( 'Select css animation for Services category pages.', 'sputnik' ),
			'type'     => 'select',
			'choices'  => $sputnik_customize_css_animation,
			'priority' => 70
		)
	);

}