<?php

function sputnik_customize_services_tab($wp_customize, $theme_name) {

	$wp_customize->add_panel('sputnik_services_settings',
	array(
		'title' => esc_html__( 'Services', 'sputnik' ),
		'priority' => 14,
		)
	);

	// services general settings
	$wp_customize->add_section( 'sputnik_services_general_settings' , array(
		'title'      => __( 'Services General', 'sputnik' ),
		'priority'   => 10,
		'panel' => 'sputnik_services_settings'
	) );


	// services categories page settings
	$wp_customize->add_section( 'sputnik_services_categories_settings' , array(
		'title'      => esc_html__( 'Services Categories Page', 'sputnik' ),
		'priority'   => 20,
		'panel' => 'sputnik_services_settings'
	) );


	// $wp_customize->add_setting( 'sputnik_services_settings_sidebar_type' , array(
	// 	'default'     => '2',
	// 	'transport'   => 'refresh',
	// 	'sanitize_callback' => 'sputnik_sanitize_sidebar_services_type'
	// ) );

	// $wp_customize->add_control(
	// 	'sputnik_services_settings_sidebar_type',
	// 	array(
	// 		'label'    => esc_html__( 'Services sidebar type', 'sputnik' ),
	// 		'section'  => 'sputnik_services_categories_settings',
	// 		'settings' => 'sputnik_services_settings_sidebar_type',
	// 		'description' => esc_html__( 'Select sidebar type for services categories pages.', 'sputnik' ),
	// 		'type'     => 'select',
	// 		'choices'  => array(
	// 			'1' => esc_html__( 'Full width', 'sputnik' ),
	// 			'2' => esc_html__( 'Right Sidebar', 'sputnik' ),
	// 			'3' => esc_html__( 'Left Sidebar', 'sputnik' ),
	// 		),
	// 		'priority' => 40
	// 	)
	// );

	// $wp_customize->add_setting( 'sputnik_services_settings_sidebar_content' , array(
	// 	'default'     => 'sidebar-1',
	// 	'transport'   => 'refresh',
	// 	'sanitize_callback' => 'sputnik_sanitize_sidebar_services_content'
	// ) );

	// $wp_customize->add_control(
	// 	'sputnik_services_settings_sidebar_content',
	// 	array(
	// 		'label'    => esc_html__( 'Services sidebar content', 'sputnik' ),
	// 		'section'  => 'sputnik_services_categories_settings',
	// 		'settings' => 'sputnik_services_settings_sidebar_content',
	// 		'description' => esc_html__( 'Select sidebar content for services categories pages.', 'sputnik' ),
	// 		'type'     => 'select',
	// 		'choices'  => array(
	// 			'sidebar-1' => esc_html__( 'WP Default Sidebar', 'sputnik' ),
	// 			'global-sidebar-1' => esc_html__( 'Blog Sidebar', 'sputnik' ),
	// 			'services-sidebar-1' => esc_html__( 'Services Sidebar', 'sputnik' ),
	// 			'custom-area-1' => esc_html__( 'Custom Area', 'sputnik' ),
	// 		),
	// 		'priority' => 50
	// 	)
	// );


	// services single page settings
	$wp_customize->add_section( 'sputnik_services_single_settings' , array(
		'title'      => esc_html__( 'Services Single Page', 'sputnik' ),
		'priority'   => 30,
		'panel' => 'sputnik_services_settings'
	) );




}