<?php

function sputnik_customize_header_tab($wp_customize, $theme_name) {

	$wp_customize->add_panel('sputnik_header_settings',
	array(
		'title' => esc_html__( 'Header', 'sputnik' ),
		'priority' => 5,
		)
	);

	// header general settings
	$wp_customize->add_section( 'sputnik_header_general_settings' , array(
		'title'      => __( 'Header General Settings', 'sputnik' ),
		'priority'   => 10,
		'panel' => 'sputnik_header_settings'
	) );

	// header general settings
	$wp_customize->add_section( 'sputnik_header_topbar_settings' , array(
		'title'      => __( 'Top Bar', 'sputnik' ),
		'priority'   => 20,
		'panel' => 'sputnik_header_settings'
	) );


	$wp_customize->add_setting( 'sputnik_header_general_settings_type' , array(
		'default'     => '1',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_header_type'
	) );

	$wp_customize->add_control(
		'sputnik_header_general_settings_type',
		array(
			'label'    => esc_html__( 'Header Type', 'sputnik' ),
			'section'  => 'sputnik_header_general_settings',
			'settings' => 'sputnik_header_general_settings_type',
			'type'     => 'select',
			'choices'  => array(
				'1' => esc_html__( 'Header type 1', 'sputnik' ),
				'2' => esc_html__( 'Header type 2', 'sputnik' ),
			),
			'priority'   => 5
		)
	);


	$wp_customize->add_setting( 'sputnik_header_general_settings_headerimage' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback'=>'esc_url_raw'
	) );

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'sputnik_header_general_settings_headerimage',
			array(
				'label'      => esc_html__( 'Header Image', 'sputnik' ),
				'section'    => 'sputnik_header_general_settings',
				'context'    => 'sputnik_header_general_settings_headerimage',
				'settings'   => 'sputnik_header_general_settings_headerimage',
				'priority'   => 10
			)
		)
	);

	$wp_customize->add_setting( 'sputnik_header_general_settings_headerimage_overlay' , array(
		'default'     => 'off',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_onoff'
	) );

	$wp_customize->add_control(
		'sputnik_header_general_settings_headerimage_overlay',
		array(
			'label'    => esc_html__( 'On/off header image overlay', 'sputnik' ),
			'section'  => 'sputnik_header_general_settings',
			'settings' => 'sputnik_header_general_settings_headerimage_overlay',
			'type'     => 'select',
			'choices'  => array(
				'off' => esc_html__( 'Off', 'sputnik' ),
				'on' => esc_html__( 'On', 'sputnik' ),
			),
			'priority'   => 20
		)
	);

	$wp_customize->add_setting( 'sputnik_header_general_settings_headerimage_opacity' , array(
		'default'     => '0.1',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_overlay_opacity'
	) );

	$wp_customize->add_control(
		'sputnik_header_general_settings_headerimage_opacity',
		array(
			'label'    => esc_html__( 'Select header image overlay opacity', 'sputnik' ),
			'section'  => 'sputnik_header_general_settings',
			'settings' => 'sputnik_header_general_settings_headerimage_opacity',
			'type'     => 'select',
			'choices'  => array(
				'0.1' => '0.1',
				'0.2' => '0.2',
				'0.3' => '0.3',
				'0.4' => '0.4',
				'0.5' => '0.5',
				'0.6' => '0.6',
				'0.7' => '0.7',
				'0.8' => '0.8',
				'0.9' => '0.9',
			),
			'priority'   => 30
		)
	);

	$wp_customize->add_setting( 'sputnik_header_general_settings_title_single_post' , array(
		'default'     => esc_html__('Blog details', 'sputnik' ),
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_text'
	) );

	$wp_customize->add_control(
		'sputnik_header_general_settings_title_single_post',
		array(
			'label'    => esc_html__( 'Title Single Post Page', 'sputnik' ),
			'section'  => 'sputnik_header_general_settings',
			'settings' => 'sputnik_header_general_settings_title_single_post',
			'type'     => 'text',
			'priority'   => 50
		)
	);

	$wp_customize->add_setting( 'sputnik_header_general_settings_title_all_posts' , array(
		'default'     => esc_html__('All posts', 'sputnik' ),
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_text'
	) );

	$wp_customize->add_control(
		'sputnik_header_general_settings_title_all_posts',
		array(
			'label'    => esc_html__( 'Title All Posts Page', 'sputnik' ),
			'section'  => 'sputnik_header_general_settings',
			'settings' => 'sputnik_header_general_settings_title_all_posts',
			'type'     => 'text',
			'priority'   => 60
		)
	);

	$wp_customize->add_setting( 'sputnik_header_general_settings_image_all_posts' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback' => 'esc_url_raw'
	) );

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'sputnik_header_general_settings_image_all_posts',
			array(
				'label'      => esc_html__( 'All Posts Page Image', 'sputnik' ),
				'section'    => 'sputnik_header_general_settings',
				'settings'   => 'sputnik_header_general_settings_image_all_posts',
			)
		)
	);

	$wp_customize->add_setting( 'sputnik_header_general_settings_title_single_service' , array(
		'default'     => esc_html__('Service details', 'sputnik' ),
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_text'
	) );

	$wp_customize->add_control(
		'sputnik_header_general_settings_title_single_service',
		array(
			'label'    => esc_html__( 'Title Single Service Page', 'sputnik' ),
			'section'  => 'sputnik_header_general_settings',
			'settings' => 'sputnik_header_general_settings_title_single_service',
			'type'     => 'text',
			'priority'   => 70
		)
	);

	$wp_customize->add_setting( 'sputnik_header_general_settings_title_search_results' , array(
		'default'     => esc_html__('Search results', 'sputnik' ),
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_text'
	) );

	$wp_customize->add_control(
		'sputnik_header_general_settings_title_search_results',
		array(
			'label'    => esc_html__( 'Title Search Results Page', 'sputnik' ),
			'section'  => 'sputnik_header_general_settings',
			'settings' => 'sputnik_header_general_settings_title_search_results',
			'type'     => 'text',
			'priority'   => 80
		)
	);

	$wp_customize->add_setting( 'sputnik_header_general_settings_show_button' , array(
		'default'     => 'on',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_onoff'
	) );

	$wp_customize->add_control(
		'sputnik_header_general_settings_show_button',
		array(
			'label'    => esc_html__( 'Show button', 'sputnik' ),
			'section'  => 'sputnik_header_general_settings',
			'settings' => 'sputnik_header_general_settings_show_button',
			'description' => esc_html__( 'Show button on/off.', 'sputnik' ),
			'type'     => 'select',
			'choices'  => array(
				'on'  => esc_html__( 'On', 'sputnik' ),
				'off'  => esc_html__( 'Off', 'sputnik' ),
			),
			'priority'   => 90
		)
	);

	$wp_customize->add_setting( 'sputnik_header_general_settings_text_button' , array(
		'default'     => esc_html__('Get a free quote', 'sputnik' ),
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_text'
	) );

	$wp_customize->add_control(
		'sputnik_header_general_settings_text_button',
		array(
			'label'    => esc_html__( 'Button text', 'sputnik' ),
			'section'  => 'sputnik_header_general_settings',
			'settings' => 'sputnik_header_general_settings_text_button',
			'type'     => 'text',
			'priority'   => 100
		)
	);

	$wp_customize->add_setting( 'sputnik_header_general_settings_link_button' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback' => 'esc_url'
	) );

	$wp_customize->add_control(
		'sputnik_header_general_settings_link_button',
		array(
			'label'    => esc_html__( 'Button link', 'sputnik' ),
			'section'  => 'sputnik_header_general_settings',
			'settings' => 'sputnik_header_general_settings_link_button',
			'type'     => 'text',
			'priority'   => 110
		)
	);


	$wp_customize->add_setting( 'sputnik_header_topbar_settings_type' , array(
		'default'     => 'off',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_onoff'
	) );

	$wp_customize->add_control(
		'sputnik_header_topbar_settings_type',
		array(
			'label'    => esc_html__( 'Top Bar', 'sputnik' ),
			'section'  => 'sputnik_header_topbar_settings',
			'settings' => 'sputnik_header_topbar_settings_type',
			'type'     => 'select',
			'choices'  => array(
				'off' => esc_html__( 'Off', 'sputnik' ),
				'on' => esc_html__( 'On', 'sputnik' ),
			),
			'priority'   => 10
		)
	);

	$wp_customize->add_setting( 'sputnik_header_topbar_settings_email' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_text'
	) );

	$wp_customize->add_control(
		'sputnik_header_settings_email',
		array(
			'label'    => esc_html__( 'Email', 'sputnik' ),
			'section'  => 'sputnik_header_topbar_settings',
			'settings' => 'sputnik_header_topbar_settings_email',
			'type'     => 'text',
			'priority'   => 20
		)
	);

	$wp_customize->add_setting( 'sputnik_header_topbar_settings_phone' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_text'
	) );

	$wp_customize->add_control(
		'sputnik_header_topbar_settings_phone',
		array(
			'label'    => esc_html__( 'Phone', 'sputnik' ),
			'section'  => 'sputnik_header_topbar_settings',
			'settings' => 'sputnik_header_topbar_settings_phone',
			'type'     => 'text',
			'priority'   => 30
		)
	);

	$wp_customize->add_setting( 'sputnik_header_topbar_settings_socicon1' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_text'
	) );

	$wp_customize->add_control(
		'sputnik_header_topbar_settings_socicon1',
		array(
			'label'    => esc_html__( 'Social Network Icon 1', 'sputnik' ),
			'section'  => 'sputnik_header_topbar_settings',
			'settings' => 'sputnik_header_topbar_settings_socicon1',
			'description' => wp_kses_post( __( 'Add icon fa-twitter <a href="//fortawesome.github.io/Font-Awesome/icons/" target="_blank">See all icons</a>', 'sputnik' ) ),
			'type'     => 'text',
			'priority'   => 40
		)
	);

	$wp_customize->add_setting( 'sputnik_header_topbar_settings_soclink1' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback' => 'esc_url'
	) );

	$wp_customize->add_control(
		'sputnik_header_topbar_settings_soclink1',
		array(
			'label'    => esc_html__( 'Social Link 1', 'sputnik' ),
			'section'  => 'sputnik_header_topbar_settings',
			'settings' => 'sputnik_header_topbar_settings_soclink1',
			'description' => esc_html__( 'https://twitter.com/', 'sputnik' ),
			'type'     => 'text',
			'priority'   => 50
		)
	);

	$wp_customize->add_setting( 'sputnik_header_topbar_settings_socicon2' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_text'
	) );

	$wp_customize->add_control(
		'sputnik_header_topbar_settings_socicon2',
		array(
			'label'    => esc_html__( 'Social Network Icon 2', 'sputnik' ),
			'section'  => 'sputnik_header_topbar_settings',
			'settings' => 'sputnik_header_topbar_settings_socicon2',
			'description' => wp_kses_post( __( 'Add icon fa-facebook <a href="//fortawesome.github.io/Font-Awesome/icons/" target="_blank">See all icons</a>', 'sputnik' ) ),
			'type'     => 'text',
			'priority'   => 60
		)
	);

	$wp_customize->add_setting( 'sputnik_header_topbar_settings_soclink2' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback' => 'esc_url'
	) );

	$wp_customize->add_control(
		'sputnik_header_topbar_settings_soclink2',
		array(
			'label'    => esc_html__( 'Social Link 2', 'sputnik' ),
			'section'  => 'sputnik_header_topbar_settings',
			'settings' => 'sputnik_header_topbar_settings_soclink2',
			'description' => esc_html__( 'https://www.facebook.com/', 'sputnik' ),
			'type'     => 'text',
			'priority'   => 70
		)
	);

	$wp_customize->add_setting( 'sputnik_header_topbar_settings_socicon3' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_text'
	) );

	$wp_customize->add_control(
		'sputnik_header_topbar_settings_socicon3',
		array(
			'label'    => esc_html__( 'Social Network Icon 3', 'sputnik' ),
			'section'  => 'sputnik_header_topbar_settings',
			'settings' => 'sputnik_header_topbar_settings_socicon3',
			'description' => wp_kses_post( __( 'Add icon fa-google-plus <a href="//fortawesome.github.io/Font-Awesome/icons/" target="_blank">See all icons</a>', 'sputnik' ) ),
			'type'     => 'text',
			'priority'   => 80
		)
	);

	$wp_customize->add_setting( 'sputnik_header_topbar_settings_soclink3' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback' => 'esc_url'
	) );

	$wp_customize->add_control(
		'sputnik_header_topbar_settings_soclink3',
		array(
			'label'    => esc_html__( 'Social Link 3', 'sputnik' ),
			'section'  => 'sputnik_header_topbar_settings',
			'settings' => 'sputnik_header_topbar_settings_soclink3',
			'description' => esc_html__( 'https://www.googleplus.com/', 'sputnik' ),
			'type'     => 'text',
			'priority'   => 90
		)
	);

	$wp_customize->add_setting( 'sputnik_header_topbar_settings_socicon4' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_text'
	) );

	$wp_customize->add_control(
		'sputnik_header_topbar_settings_socicon4',
		array(
			'label'    => esc_html__( 'Social Network Icon 4', 'sputnik' ),
			'section'  => 'sputnik_header_topbar_settings',
			'settings' => 'sputnik_header_topbar_settings_socicon4',
			'description' => wp_kses_post( __( 'Add icon fa-pinterest-p <a href="//fortawesome.github.io/Font-Awesome/icons/" target="_blank">See all icons</a>', 'sputnik' ) ),
			'type'     => 'text',
			'priority'   => 100
		)
	);

	$wp_customize->add_setting( 'sputnik_header_topbar_settings_soclink4' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback' => 'esc_url'
	) );

	$wp_customize->add_control(
		'sputnik_header_topbar_settings_soclink4',
		array(
			'label'    => esc_html__( 'Social Link 4', 'sputnik' ),
			'section'  => 'sputnik_header_topbar_settings',
			'settings' => 'sputnik_header_topbar_settings_soclink4',
			'description' => esc_html__( 'https://pinterest.com/', 'sputnik' ),
			'type'     => 'text',
			'priority'   => 110
		)
	);

	$wp_customize->add_setting( 'sputnik_header_topbar_settings_socicon5' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_text'
	) );

	$wp_customize->add_control(
		'sputnik_header_topbar_settings_socicon5',
		array(
			'label'    => esc_html__( 'Social Network Icon 5', 'sputnik' ),
			'section'  => 'sputnik_header_topbar_settings',
			'settings' => 'sputnik_header_topbar_settings_socicon5',
			'description' => wp_kses_post( __( 'Add icon fa-instagram <a href="//fortawesome.github.io/Font-Awesome/icons/" target="_blank">See all icons</a>', 'sputnik' ) ),
			'type'     => 'text',
			'priority'   => 120
		)
	);

	$wp_customize->add_setting( 'sputnik_header_topbar_settings_soclink5' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback' => 'esc_url'
	) );

	$wp_customize->add_control(
		'sputnik_header_topbar_settings_soclink5',
		array(
			'label'    => esc_html__( 'Social Link 5', 'sputnik' ),
			'section'  => 'sputnik_header_topbar_settings',
			'settings' => 'sputnik_header_topbar_settings_soclink5',
			'description' => esc_html__( 'https://www.instagram.com/', 'sputnik' ),
			'type'     => 'text',
			'priority'   => 130
		)
	);

}