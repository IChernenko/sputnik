<?php

function sputnik_customize_footer_tab($wp_customize, $theme_name) {

	$staticBlocks = sputnik_get_staticblock_option_array();

	$wp_customize->add_section( 'sputnik_footer_settings' , array(
		'title'      => esc_html__( 'Footer', 'sputnik' ),
		'priority'   => 6,
	) );

	$wp_customize->add_setting( 'sputnik_footer_block' , array(
		'default'     => 'default',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_footer_block'
	) );

	$wp_customize->add_control(
		'sputnik_footer_block',
		array(
			'label'    => esc_html__( 'Footer Block', 'sputnik' ),
			'section'  => 'sputnik_footer_settings',
			'settings' => 'sputnik_footer_block',
			'type'     => 'select',
			'choices'  => $staticBlocks,
			'priority' => 10
		)
	);

}