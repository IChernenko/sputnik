<?php

function sputnik_customize_style_tab($wp_customize, $theme_name) {


	$wp_customize->add_section( 'sputnik_style_settings' , array(
		'title'      => esc_html__( 'Style Settings', 'sputnik' ),
		'priority'   => 4,
	) );

	$wp_customize->add_setting(
		'sputnik_style_settings_main_color',
		array(
			'default' => '#00c0e2',
			'transport'   => 'refresh',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'sputnik_style_settings_main_color',
			array(
				'label' => esc_html__( 'Main Color', 'sputnik' ),
				'section' => 'sputnik_style_settings',
				'settings' => 'sputnik_style_settings_main_color',
				'priority'   => 10
			)
		)
	);

	$wp_customize->add_setting(
		'sputnik_style_settings_additional_color_1',
		array(
			'default' => '#5fd3e8',
			'transport'   => 'refresh',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'sputnik_style_settings_additional_color_1',
			array(
				'label' => esc_html__( 'Additional Color 1', 'sputnik' ),
				'section' => 'sputnik_style_settings',
				'settings' => 'sputnik_style_settings_additional_color_1',
				'priority'   => 20
			)
		)
	);

	$wp_customize->add_setting(
		'sputnik_style_settings_additional_color_2',
		array(
			'default' => '#e3ebf0',
			'transport'   => 'refresh',
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'sputnik_style_settings_additional_color_2',
			array(
				'label' => esc_html__( 'Additional Color 2', 'sputnik' ),
				'section' => 'sputnik_style_settings',
				'settings' => 'sputnik_style_settings_additional_color_2',
				'priority'   => 30
			)
		)
	);

	$wp_customize->add_setting( 'sputnik_style_settings_custom_css' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_text'
	) );


   $wp_customize->add_control(
		'sputnik_style_settings_custom_css',
		array(
		  'label' => esc_html__( 'Custom CSS', 'sputnik' ),
		  'type' => 'textarea',
		  'section' => 'sputnik_style_settings',
		  'settings' => 'sputnik_style_settings_custom_css',
		  'description' => esc_html__( 'Add custom CSS here', 'sputnik' ),
		  'priority'   => 40
		)
	);

}

