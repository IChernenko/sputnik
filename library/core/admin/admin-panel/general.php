<?php

function sputnik_customize_general_tab($wp_customize, $theme_name) {

	$wp_customize->add_section( 'sputnik_general_settings' , array(
		'title'      => esc_html__( 'General Settings', 'sputnik' ),
		'priority'   => 0,
	) );

	/* logo image */
	$wp_customize->add_setting( 'sputnik_general_settings_logo1' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback'=>'esc_url_raw'
	) );

	$wp_customize->add_control(
	   new WP_Customize_Image_Control(
		   $wp_customize,
		   'sputnik_general_settings_logo1',
			   array(
				   'label'      => esc_html__( 'Logo image for Header type 1', 'sputnik' ),
				   'section'    => 'sputnik_general_settings',
				   'context'    => 'sputnik_general_settings_logo1',
				   'settings'   => 'sputnik_general_settings_logo1',
				   'priority'   => 50,
				   'description' => esc_html__( 'Recommended size: 200x50', 'sputnik')
			   )
	   )
   );

	$wp_customize->add_setting( 'sputnik_general_settings_logo2' , array(
		'default'     => '',
		'transport'   => 'refresh',
		'sanitize_callback'=>'esc_url_raw'
	) );

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			$wp_customize,
			'sputnik_general_settings_logo2',
			array(
				'label'      => esc_html__( 'Logo image for Header type 2', 'sputnik' ),
				'section'    => 'sputnik_general_settings',
				'context'    => 'sputnik_general_settings_logo2',
				'settings'   => 'sputnik_general_settings_logo2',
				'priority'   => 50,
				'description' => esc_html__( 'Recommended size: 200x50', 'sputnik')
			)
		)
	);

	// $wp_customize->add_setting( 'sputnik_general_settings_logo_mobile' , array(
	// 	'default'     => '',
	// 	'transport'   => 'refresh',
	// 	'sanitize_callback'=>'esc_url_raw'
	// ) );

	// $wp_customize->add_control(
	// 	new WP_Customize_Image_Control(
	// 		$wp_customize,
	// 		'sputnik_general_settings_logo_mobile',
	// 		array(
	// 			'label'      => esc_html__( 'Logo mobile image', 'sputnik' ),
	// 			'section'    => 'sputnik_general_settings',
	// 			'context'    => 'sputnik_general_settings_logo_mobile',
	// 			'settings'   => 'sputnik_general_settings_logo_mobile',
	// 			'priority'   => 50,
	// 			'description' => esc_html__( 'Recommended size: 34x34', 'sputnik')
	// 		)
	// 	)
	// );

	$wp_customize->add_setting( 'sputnik_general_settings_loader' , array(
		'default'     => 'off',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_loader'
	) );

   $wp_customize->add_control(
		'sputnik_general_settings_loader',
		array(
			'label'    => esc_html__( 'Loader', 'sputnik' ),
			'section'  => 'sputnik_general_settings',
			'settings' => 'sputnik_general_settings_loader',
			'type'     => 'select',
			'choices'  => array(
				'off'  => esc_html__( 'Off', 'sputnik' ),
				'usemain' => esc_html__( 'Use on main', 'sputnik' ),
				'useall' => esc_html__( 'Use on all pages', 'sputnik' )
			),
			'priority'   => 110
		)
	);

   $wp_customize->add_setting( 'sputnik_general_settings_breadcrumbs' , array(
		'default'     => 'on',
		'transport'   => 'refresh',
		'sanitize_callback' => 'sputnik_sanitize_onoff'
	) );

   $wp_customize->add_control(
		'sputnik_general_settings_breadcrumbs',
		array(
			'label'    => esc_html__( 'Breadcrumbs', 'sputnik' ),
			'section'  => 'sputnik_general_settings',
			'settings' => 'sputnik_general_settings_breadcrumbs',
			'type'     => 'select',
			'choices'  => array(
				'on'  => esc_html__( 'On', 'sputnik' ),
				'off' => esc_html__( 'Off', 'sputnik' )
			),
			'priority'   => 120
		)
	);

	$wp_customize->add_setting('sputnik_general_404_style', array(
		'sanitize_callback' => 'esc_html',
	));
	$wp_customize->add_control('sputnik_general_404_style', array(
		'label' => esc_html__('Select 404 style', 'sputnik'),
		'type' => 'select',
		'section' => 'sputnik_general_settings',
		'choices' => array(
			'A' => 'A',
			'B' => 'B',
		),
	));

}