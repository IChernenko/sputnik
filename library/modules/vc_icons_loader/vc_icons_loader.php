<?php

/**
 * Iconc loader for VC
 * version 1.15.11
 */

function sputnik_init_vc_icons(){
	$pix_libs = $pix_fonts = $pix_fonts_str = $params = $params1 = $params2 = array();

	if(function_exists('fil_init')) {

		if( array_key_exists( 'vc_iconpicker-type-pixflaticon' , $GLOBALS['wp_filter']) ) {
			$pix_libs[esc_html__( 'Flaticon', 'sputnik' )] = 'pixflaticon';
		}
		if( array_key_exists( 'vc_iconpicker-type-pixstrokegap' , $GLOBALS['wp_filter']) ) {
			$pix_libs[esc_html__( 'Stroke-Gap-Icons', 'sputnik' )] = 'pixstrokegap';
		}
		if( array_key_exists( 'vc_iconpicker-type-pixfontawesome' , $GLOBALS['wp_filter']) ) {
			$pix_libs[esc_html__( 'Font Awesome', 'sputnik' )] = 'pixfontawesome';
		}
		if( array_key_exists( 'vc_iconpicker-type-pixelegant' , $GLOBALS['wp_filter']) ) {
			$pix_libs[esc_html__( 'Elegant', 'sputnik' )] = 'pixelegant';
		}
		if( array_key_exists( 'vc_iconpicker-type-pixicomoon' , $GLOBALS['wp_filter']) ) {
			$pix_libs[esc_html__( 'Icomoon', 'sputnik' )] = 'pixicomoon';
		}
		if( array_key_exists( 'vc_iconpicker-type-pixsimple' , $GLOBALS['wp_filter']) ) {
			$pix_libs[esc_html__( 'Simple', 'sputnik' )] = 'pixsimple';
		}
		if( array_key_exists( 'vc_iconpicker-type-pixcustom1' , $GLOBALS['wp_filter']) ) {
			$pix_libs[esc_html__( 'Custom 1', 'sputnik' )] = 'pixcustom1';
		}
		if( array_key_exists( 'vc_iconpicker-type-pixcustom2' , $GLOBALS['wp_filter']) ) {
			$pix_libs[esc_html__( 'Custom 2', 'sputnik' )] = 'pixcustom2';
		}
		if( array_key_exists( 'vc_iconpicker-type-pixcustom3' , $GLOBALS['wp_filter']) ) {
			$pix_libs[esc_html__( 'Custom 3', 'sputnik' )] = 'pixcustom3';
		}
		if( array_key_exists( 'vc_iconpicker-type-pixcustom4' , $GLOBALS['wp_filter']) ) {
			$pix_libs[esc_html__( 'Custom 4', 'sputnik' )] = 'pixcustom4';
		}
		if( array_key_exists( 'vc_iconpicker-type-pixcustom5' , $GLOBALS['wp_filter']) ) {
			$pix_libs[esc_html__( 'Custom 5', 'sputnik' )] = 'pixcustom5';
		}
		if( array_key_exists( 'vc_iconpicker-type-fontawesome' , $GLOBALS['wp_filter']) ) {
			$pix_libs[esc_html__( 'VC Font Awesome', 'sputnik' )] = 'fontawesome';
		}
		if( array_key_exists( 'vc_iconpicker-type-openiconic' , $GLOBALS['wp_filter']) ) {
			$pix_libs[esc_html__( 'VC Open Iconic', 'sputnik' )] = 'openiconic';
		}
		if( array_key_exists( 'vc_iconpicker-type-typicons' , $GLOBALS['wp_filter']) ) {
			$pix_libs[esc_html__( 'VC Typicons', 'sputnik' )] = 'typicons';
		}
		if( array_key_exists( 'vc_iconpicker-type-entypo' , $GLOBALS['wp_filter']) ) {
			$pix_libs[esc_html__( 'VC Entypo', 'sputnik' )] = 'entypo';
		}
		if( array_key_exists( 'vc_iconpicker-type-linecons' , $GLOBALS['wp_filter']) ) {
			$pix_libs[esc_html__( 'VC Linecons', 'sputnik' )] = 'linecons';
		}

		$add_icon_libs = array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Icon library', 'sputnik' ),
			'param_name' => 'type',
			'value' => $pix_libs,
			'admin_label' => true,
			'description' => esc_html__( 'Select icon library.', 'sputnik' ),
		);

		if (is_array($pix_libs)) {
			$pix_fonts_str[] = $add_icon_libs;

			foreach ($pix_libs as $val) {
				if ($val != ''){
					$pix_fonts[$val] = array(
						'type' => 'iconpicker',
						'heading' => esc_html__('Icon', 'sputnik'),
						'param_name' => 'icon_' . $val,
						'value' => '',
						'settings' => array(
							'emptyIcon' => true,
							'type' => $val,
							'iconsPerPage' => 4000,
						),
						'dependency' => array(
							'element' => 'type',
							'value' => $val,
						),
						'description' => esc_html__('Select icon from library.', 'sputnik'),
					);
				}

				$pix_fonts_str[] = $pix_fonts[$val];
			}
		}
	}
	return $pix_fonts_str;
}



function sputnik_get_vc_icons($pix_fonts_str){
	$result = array();
	if (!empty($pix_fonts_str) && function_exists('fil_init'))
		$result = apply_filters('sputnik_vc_icons_loader_show',$pix_fonts_str);
	return array_values($result);

}
