<?php
add_action( 'init', 'sputnik_buttons' );

function sputnik_buttons() {
    add_filter( "mce_external_plugins", "sputnik_add_buttons" );
    add_filter( 'mce_buttons', 'sputnik_register_buttons' );
}

function sputnik_add_buttons( $plugin_array ) {
    $plugin_array['sputnik'] = get_template_directory_uri() . '/js/sputnik-tinymce-buttons.js';
    return $plugin_array;
}

function sputnik_register_buttons( $buttons ) {
    array_push( $buttons, 'dropcap', 'highlight' ); // dropcap
    return $buttons;
}