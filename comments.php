<?php
/*** The template for displaying comments ***/

if ( post_password_required() ) {
	return;
}

$sputnik_blog_css_animation = ( sputnik_get_option('css_animation_settings_blog', '') != '' ) ? ' wow '.sputnik_get_option('css_animation_settings_blog', '') : '';
?>


<?php if ( have_comments() ) : ?>
<div id="comments" class="blog-single-post-comments <?php echo esc_attr($sputnik_blog_css_animation); ?>">

	<h3 class="comments-title">
	<?php
		$comments_number = get_comments_number();
		if ( $comments_number == 1 ) :
			esc_html_e( 'Post comment', 'sputnik' );
		else :
			esc_html_e( 'Post comments', 'sputnik' );
		endif;
		?>
		<span><?php echo ': ' . esc_html($comments_number); ?></span>
	</h3>

	<ul class="comment-list">
		<?php
			wp_list_comments( array(
				'short_ping'   => true,
				'style'        => 'ul',
				'callback'     => 'sputnik_comments_callback',
				'end-callback' => 'sputnik_comments_end_callback'
			) );
		?>
	</ul><!-- .comment-list -->

	<ul class="pager">
		<li><?php previous_comments_link( esc_html__( 'Older comments', 'sputnik' ) ); ?></li>
		<li><?php next_comments_link( esc_html__( 'Newer comments', 'sputnik' ) ); ?></li>
	</ul>
</div>
<?php endif; // Check for have_comments(). ?>

<?php
	// If comments are closed and there are comments, let's leave a little note, shall we?
	if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
?>
	<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'sputnik' ); ?></p>
<?php endif; ?>

<div class="blog-single-post-leave-comment <?php echo esc_attr($sputnik_blog_css_animation); ?>">

	<?php
		$commenter = wp_get_current_commenter();
		$req = get_option( 'require_name_email' );
		$aria_req = ( $req ? " aria-required='true'" : '' );
		$html_req = ( $req ? " required='required'" : '' );
		$fields =  array(
			'author' => '<div class="blog-single-post-leave-comment-form row"><div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 blog-single-post-leave-comment-form-item">' .
			'<input id="author" name="author" type="text" placeholder="' . esc_html__( 'Full Name', 'sputnik' ) . ( $req ? ' *' : '' ) . '" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" autocomplete="on" tabindex="2" ' . $aria_req . $html_req . ' /></div>',
			'email'  => '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 blog-single-post-leave-comment-form-item">'.
			'<input id="email" name="email" type="text" placeholder="' . esc_html__( 'Email', 'sputnik' ) . ( $req ? ' *' : '' ) . '" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" autocomplete="on" tabindex="3" ' . $aria_req . $html_req . ' /></div>',
			'url'    => '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 blog-single-post-leave-comment-form-item">'.
			'<input id="url" name="url" type="text" placeholder="' . esc_html__( 'Website', 'sputnik' ) . '" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" autocomplete="on" tabindex="4"' . $aria_req . ' /></div></div>',
			);

		$comments_args = array(
			'fields' =>  $fields,
			'comment_field' => '<div class="blog-single-post-leave-comment-form row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blog-single-post-leave-comment-form-item"><textarea id="comment" name="comment" cols="45" rows="5" aria-required="true" required="required">' . esc_html__( 'Comment message', 'sputnik' ) . '</textarea></div></div>',
			'title_reply_before'   => '<h3 id="reply-title" class="comment-reply-title">',
			'title_reply_after'    => '</h3>',
			'submit_button'        => '
									<div class="blog-single-post-leave-comment-form row">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blog-single-post-leave-comment-form-item">
											<button><span class="my-btn my-btn-grey">
												<span class="my-btn-bg-top"></span>
												<span class="my-btn-bg-bottom"></span>
												<span class="my-btn-text">
													' . esc_html__( 'Send comment', 'sputnik' ) . '
												</span>
											</span></button>
										</div>
									</div>
			',
			'submit_field'  => '%1$s %2$s'
		);

		comment_form( $comments_args );
	 ?>

</div>