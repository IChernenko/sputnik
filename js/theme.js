/*=== "use strict" mode on ===*/
"use strict";

/*=== WOW plugin init ====*/
new WOW().init();

(function ($) {

	$(window).on('load', function () {

		/*=== hide pre loader ===*/

		$('#preloader').fadeOut( "500", function() {
			$(this).remove();
		});

		/*=== Stellar.js parallax plugin init ====*/
		$.stellar({
			horizontalScrolling: false,
			verticalOffset: 0,
		});

		/****** ISOTOP STARTS */

		var blog_container = $('.blog-masonry-holder');

		blog_container.imagesLoaded(function() {
			blog_container.isotope({
				// options
				itemSelector: '.item',
				layoutMode: 'masonry',
			});
		});

		var services_container = $('.services-masonry-holder');

		services_container.imagesLoaded(function() {
			services_container.isotope({
				// options
				itemSelector: '.item',
				layoutMode: 'masonry',
			});
		});

		$('.services-isotop-filter a').on( 'click', function() {

			var container = $('.services-masonry-holder');
			var filterValue = $(this).attr('data-filter');

			//don't proceed if already selected
			if ($(this).hasClass('selected')) {
				return false;
			}
			var $services_optionSet = $(this).parents('.services-option-set');
			$services_optionSet.find('.selected').removeClass('selected');
			$(this).addClass('selected');

			filterValue = filterValue === 'false' ? false : filterValue;
			services_container.isotope({ filter: filterValue });
			return false;
		});

		/****** ISOTOP ENDS */

	});

	var windowWidth = $(window).width();
	$('#main-slider-1').height(windowWidth/2 + 'px');

	/*=== show main search ===*/

	$('#show-search').on('click', function() {
		$('#main-logo, #main-menu li, #show-search, #show-menu').fadeOut( "300", function() {
			setTimeout(function() {
				$('#main-search-input, #close-search').fadeIn(300);
				$('#main-search').focus();
				$('#main-navbar').css('border-bottom', '1px solid');
			}, 400)
		});
		return false;
	});

	/*=== close main search ===*/

	$('#close-search').on('click', function() {
		$('#main-navbar').css('border-bottom', 'none');
		$('#main-search-input, #close-search').fadeOut( "300", function() {
			setTimeout(function() {
				$('#main-logo, #main-menu li, #show-search, #show-menu').fadeIn(300);
			}, 400)
		});
		return false;
	});

   /*=== service mark animate at scroll ===*/

	if($('*').is('.service-mark-wrapper')) {

		$('.service-mark-wrapper').on('scrollSpy:enter', function() {

			$('.service-mark-wrapper').children(".service-mark-border-top").css('left', '0');
			setTimeout(function() { $('.service-mark-wrapper').children(".service-mark-border-right").css('top', '0');	}, 300)
			setTimeout(function() { $('.service-mark-wrapper').children(".service-mark-border-bottom").css('right', '0'); }, 600)
			setTimeout(function() { $('.service-mark-wrapper').children(".service-mark-border-left").css('bottom', '0'); }, 900)
			setTimeout(function() { $('.service-mark-wrapper').addClass('service-mark'); }, 1200)

		});

		$('.service-mark-wrapper').scrollSpy();

	}


	if($('*').is('.chart')) {
		$('.chart').on('scrollSpy:enter', function() {

			$(this).easyPieChart({
				barColor: false,
				trackColor: false,
				scaleColor: false,
				scaleLength: false,
				lineCap: false,
				lineWidth: false,
				size: false,
				animate: 7000,


				onStep: function(from, to, percent) {

					$(this.el).find('.percent').text(Math.round(percent));

				}

			});

		});
		$('.chart').scrollSpy();
	}

	/*=== Play and pause video section ====*/
	var video =  document.getElementById('aboutvideo');

	var play_video = $('#play-video');
	var stop_video = $('#stop-video');

	play_video.on('click', function(){
		  video.play();
		  play_video.css('display', 'none');
		  stop_video.css('display', 'block');

		  return false;
	});

	stop_video.on('click', function(){
		  video.pause();
		  stop_video.css('display', 'none');
		  play_video.css('display', 'block');

		  return false;
	});

	/************************************************************************************ CAROUSEL STARTS */

	$('.enable-owl-carousel').each(function(){
		var owlAutoplay = $(this).attr('data-autoplay'),
			owlItemsMobile = $(this).attr('data-itemmobile'),
			owlItemsTablet = $(this).attr('data-itemtablet'),
			owlItemsDesktop = $(this).attr('data-itemdesktop');

		$(this).owlCarousel({

			autoplay: owlAutoplay,
			slideSpeed: 2000,
			itemsScaleUp: true,
			stopOnHover: true,
			navText: [
				"<i class='fa fa-angle-right'></i>", "<i class='fa fa-angle-left'></i>"
			],
			items: owlItemsDesktop, // items above 1199px browser width
			itemsDesktop: [1199, owlItemsDesktop], // items between 1199px and 769px
			itemsTablet: [768, owlItemsTablet], // betweem 768px and 480px
			itemsMobile: [479, owlItemsMobile], // items between 479 and 0
		});
	});

	/************************************************************************************ CAROUSEL ENDS */


	/*=== Accordion slide menu ====*/

	$('#left-menu').metisMenu({
		//toggle: false, // disable the auto collapse. Default: true.
		doubleTapToGo: true
	});

	/*=== Show left slide menu ====*/

	$('#show-menu').on('click', function() {

		$("#slide-menu").css('left', '0');
		$("#black-overlay").css('display', 'block');
		$("#black-overlay").css('opacity', '1');

		return false;

	});

	/*=== Close left slide menu ====*/

	$('#close-menu, #black-overlay').on('click', function() {

		$("#slide-menu").css('left', '-290px');
		$("#black-overlay").css('opacity', '0');
		setTimeout(function() {
			$("#black-overlay").css('display', 'none');
		}, 300)

		return false;

	});

	/****** FLICKR STARTS */
	$('.basicuse').each(function(){
		var sputnikFlickrLimit = $(this).attr('data-limit'),
			sputnikFlickrId = $(this).attr('data-id');

		$(this).jflickrfeed({
			limit: sputnikFlickrLimit,
			qstrings: {
				id: sputnikFlickrId
			},
			itemTemplate: '<li><a href="{{image_b}}" rel="prettyPhoto[flickr-'+sputnikFlickrId+']"><img src="{{image_s}}" alt="{{title}}" /></a></li>',
			itemCallback: function() {
				prettyPhoto({
					changepicturecallback: function() {
						$('.pp_pic_holder').show();
					}
				});
			}

		});
	});

	/****** FLICKR ENDS */

	/****** PRETTYPHOTO */

	function prettyPhoto(){
		"use strict";

		$('a[data-rel]').each(function() {
			$(this).attr('rel', $(this).data('rel'));
		});

		$("a[rel^='prettyPhoto']").prettyPhoto({
			animation_speed: 'normal', /* fast/slow/normal */
			slideshow: false, /* false OR interval time in ms */
			autoplay_slideshow: false, /* true/false */
			opacity: 0.80, /* Value between 0 and 1 */
			show_title: false, /* true/false */
			allow_resize: true, /* Resize the photos bigger than viewport. true/false */
			horizontal_padding: 0,
			default_width: 680,
			default_height: 495,
			counter_separator_label: '/', /* The separator for the gallery counter 1 "of" 2 */
			theme: 'pp_default', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
			hideflash: false, /* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
			wmode: 'opaque', /* Set the flash wmode attribute */
			autoplay: true, /* Automatically start videos: True/False */
			modal: false, /* If set to true, only the close button will close the window */
			overlay_gallery: false, /* If set to true, a gallery will overlay the fullscreen image on mouse over */
			keyboard_shortcuts: true, /* Set to false if you open forms inside prettyPhoto */
			deeplinking: false,
			social_tools: false,
			iframe_markup: '<iframe src ="{path}" allowfullscreen="true"  width="{width}" height="{height}" frameborder="no"></iframe>'
		});
	}
	prettyPhoto();

})(jQuery);
