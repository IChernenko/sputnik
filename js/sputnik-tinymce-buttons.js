(function() {
    tinymce.create('tinymce.plugins.Sputnik', {
        init : function(ed, url) {
            ed.addButton('highlight', {
                title : 'Highlight',
                text: 'H',
                icon: false,
                type: 'menubutton',
                menu: [
                    {
                        text: 'Background primary',
                        onclick: function() {
                            var selected_text = ed.selection.getContent();
                            var return_text = '';
                            return_text = '<span class="text-bg bg-primary">' + selected_text + '</span>';
                            ed.execCommand('mceInsertContent', 0, return_text);
                        },
                    },
                    {
                        text: 'Background secondary',
                        onclick: function() {
                            var selected_text = ed.selection.getContent();
                            var return_text = '';
                            return_text = '<span class="text-bg bg-secondary">' + selected_text + '</span>';
                            ed.execCommand('mceInsertContent', 0, return_text);
                        },
                    },
                    {
                        text: 'Background border',
                        onclick: function() {
                            var selected_text = ed.selection.getContent();
                            var return_text = '';
                            return_text = '<span class="text-bg bg-border">' + selected_text + '</span>';
                            ed.execCommand('mceInsertContent', 0, return_text);
                        },
                    },
                    {
                        text: 'Color primary',
                        onclick: function() {
                            var selected_text = ed.selection.getContent();
                            var return_text = '';
                            return_text = '<span class="color-primary">' + selected_text + '</span>';
                            ed.execCommand('mceInsertContent', 0, return_text);
                        },
                    },
                    {
                        text: 'Color secondary',
                        onclick: function() {
                            var selected_text = ed.selection.getContent();
                            var return_text = '';
                            return_text = '<span class="color-secondary">' + selected_text + '</span>';
                            ed.execCommand('mceInsertContent', 0, return_text);
                        },
                    },
                    {
                        text: 'Color dark',
                        onclick: function() {
                            var selected_text = ed.selection.getContent();
                            var return_text = '';
                            return_text = '<span class="color-dark">' + selected_text + '</span>';
                            ed.execCommand('mceInsertContent', 0, return_text);
                        },
                    },
                ],
                // image : url + '/dropcap.jpg'
            });

            ed.addButton('dropcap', {
                title : 'Dropcap',
                type: 'menubutton',
                text: 'D',
                icon: false,
                menu: [
                    {
                        text: 'Dropcap primary',
                        onclick: function() {
                            var selected_text = ed.selection.getContent();
                            var return_text = '';
                            return_text = '<p class="dropcap dropcap_primary">' + selected_text + '</p>';
                            ed.execCommand('mceInsertContent', 0, return_text);
                        },
                    },
                    {
                        text: 'Dropcap secondary',
                        onclick: function() {
                            var selected_text = ed.selection.getContent();
                            var return_text = '';
                            return_text = '<p class="dropcap dropcap_primary dropcap_sm">' + selected_text + '</p>';
                            ed.execCommand('mceInsertContent', 0, return_text);
                        },
                    },
                    {
                        text: 'Dropcap black',
                        onclick: function() {
                            var selected_text = ed.selection.getContent();
                            var return_text = '';
                            return_text = '<p class="dropcap dropcap_secondary typography-dropcap">' + selected_text + '</p>';
                            ed.execCommand('mceInsertContent', 0, return_text);
                        },
                    },
                    {
                        text: 'Dropcap black secondary',
                        onclick: function() {
                            var selected_text = ed.selection.getContent();
                            var return_text = '';
                            return_text = '<p class="dropcap dropcap_secondary dropcap_sm typography-dropcap">' + selected_text + '</p>';
                            ed.execCommand('mceInsertContent', 0, return_text);
                        },
                    },
                ],
                // image : url + '/dropcap.jpg'
            });

        },
        // ... Hidden code
    });
    // Register plugin
    tinymce.PluginManager.add( 'sputnik', tinymce.plugins.Sputnik );
})();