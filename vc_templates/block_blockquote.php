<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $style
 * @var $text
 * @var $name
 * @var $position
 * @var $image
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Blockquote
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);
$img_id = preg_replace('/[^\d]/', '', $image);
$img_meta_array = sputnik_pix_wp_get_attachment($img_id);

if ($style == 'A') { ?>
    <blockquote class="b-blockquote b-blockquote-2">
        <p><?php echo $text; ?></p>
        <footer class="b-blockquote-2__footer">
            <div class="b-blockquote-2__img center-block"><img src="<?php echo $img_meta_array['url']; ?>" alt="<?php echo $img_meta_array['alt']; ?>" class="img-responsive"/></div>
            <cite title="Blockquote Title" class="b-blockquote-2__cite">
                <span class="b-blockquote-2__author"><?php echo $name; ?></span>
                <span class="b-blockquote-2__category"><?php echo $position; ?></span>
            </cite>
        </footer>
    </blockquote>
    <!-- end b-blockquote-->
<?php } elseif ($style == 'B') { ?>
    <blockquote class="b-blockquote b-blockquote-1 typography-blockquote">
        <p><?php echo $text; ?></p>
    </blockquote>
    <!-- end b-blockquote-->
<?php } ?>

