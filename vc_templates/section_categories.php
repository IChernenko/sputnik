<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $style
 * @var $categories [N]['category_id']
 * @var $categories [N]['type']
 * Shortcode class
 * @var $this WPBakeryShortCode_Section_Categories
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$categories = (array)vc_param_group_parse_atts($categories);
if ($style == 'A') {
    foreach ($categories as $category) {
        $font_icon = $category['icon_' . $category['type']];
        $category = get_category($category['category_id']);
        ?>
        <div class="col-md-4">
            <section class="b-advantages-3"><i class="b-advantages-3__icon <?php echo $font_icon; ?>"></i>
                <h3 class="b-advantages-3__title ui-title-inner"><a href="<?php echo get_category_link($category->term_id); ?>" class="b-advantages-5__link"><?php echo $category->name; ?></a></h3>
                <div class="b-advantages-3__decor ui-decor-1"></div>
                <div class="b-advantages-3__info"><?php echo $category->category_description; ?></div>
            </section>
            <!-- end b-advantages-->
        </div>
    <?php }
} elseif ($style == 'B') {
    foreach ($categories as $category) {
        $img_id = preg_replace('/[^\d]/', '', $category['image']);
        $img_meta_array = sputnik_pix_wp_get_attachment($img_id);
        $category = get_category($category['category_id']);
        ?>
        <div class="col-sm-4">
            <article class="b-post b-post-1 b-post-1_mod-a clearfix">
                <div class="entry-media"><a href="<?php echo $img_meta_array['src']; ?>" class="js-zoom-images"><img src="<?php echo $img_meta_array['src']; ?>" alt="Foto" class="img-responsive"/></a></div>
                <div class="entry-main">
                    <div class="entry-header">
                        <h2 class="entry-title ui-title-inner"><?php echo $category->name; ?></h2>
                        <div class="ui-decor-1"></div>
                    </div>
                    <div class="entry-content">
                        <p><?php echo $category->category_description; ?></p>
                    </div>
                    <div class="entry-footer"><a href="<?php echo get_category_link($category->term_id); ?>" class="btn btn-default btn-w-ic btn-sm btn-effect">view details<i class="icon fa fa-long-arrow-right"></i></a></div>
                </div>
            </article>
            <!-- end post-->
        </div>
    <?php }
} ?>