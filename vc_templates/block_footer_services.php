<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $hidden (unused)
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Footer_Services
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

$theme_locations = get_nav_menu_locations();

$menu_items = wp_get_nav_menu_items($theme_locations['services_footer_menu']);
?>

<div class="col-md-2">9
    <section class="footer-section">
        <h3 class="footer-section__title ui-title-type-1">what we do</h3>
        <div class="ui-decor-1"></div>
        <ul class="footer-list list list-mark-4">
            <?php foreach ($menu_items as $menu_item){ ?>
                <li class="footer-list__item"><a href="<?php echo $menu_item->url ?>" class="footer-list__link"><?php echo $menu_item->title ?></a></li>
            <?php } ?>
        </ul>
    </section>
</div>
