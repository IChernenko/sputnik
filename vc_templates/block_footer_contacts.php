<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $text
 * @var $items [N]['text']
 * @var $items [N]['type']
 * Shortcode class
 * @var $this WPBakeryShortCode_Section_Contacts
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$items = (array)vc_param_group_parse_atts($items);
?>

<section class="footer-section">
    <h3 class="footer-section__title ui-title-type-1">contact details</h3>
    <div class="ui-decor-1"></div>
    <div class="footer__info">
        <p><?php echo $text; ?></p>
    </div>
    <?php foreach ($items as $item) {
        $font_icon = $item['icon_' . $item['type']]; ?>
        <div class="footer__contact"><i class="icon <?php echo $font_icon; ?>"></i> <?php echo $item['text']; ?></div>
    <?php } ?>
</section>