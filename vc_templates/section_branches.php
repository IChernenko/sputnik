<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $title
 * @var $type
 * @var $items[N]['text']
 * @var $items[N]['type']
 * Shortcode class
 * @var $this WPBakeryShortCode_Section_Branches
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$items = (array)vc_param_group_parse_atts($items);
$font_icon = ${'icon_' . $type};
?>

<section class="b-branches"><i class="b-branches__icon <?php echo $font_icon; ?>"></i>
    <h3 class="b-branches__title ui-title-inner ui-title-inner_lg"><?php echo $title; ?></h3>
    <div class="ui-decor-1"></div>
    <? foreach ($items as $item) {
        $font_icon = $item['icon_' . $item['type']];
        ?>
        <div class="b-branches__item"><i class="icon <?php echo $font_icon; ?>"></i> <?php echo $item['text']; ?></div>
    <? } ?>
</section>