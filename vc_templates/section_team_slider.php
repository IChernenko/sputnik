<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $slides [N]['title']
 * @var $slides [N]['name']
 * @var $slides [N]['position']
 * @var $slides [N]['text']
 * @var $slides [N]['image']
 * @var $slides [N]['twitter_link']
 * @var $slides [N]['facebook_link']
 * @var $slides [N]['google_plus_link']
 * @var $slides [N]['instagram_link']
 * Shortcode class
 * @var $this WPBakeryShortCode_Section_Team_Slider
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$slides = (array)vc_param_group_parse_atts($slides);
?>

<div data-min480="1" data-min768="3" data-min992="4" data-min1200="4" data-pagination="false" data-navigation="true" data-auto-play="4000" data-stop-on-hover="true" class="owl-carousel owl-theme enable-owl-carousel">
    <?php foreach ($slides as $slide) {
        $img_id = preg_replace('/[^\d]/', '', $slide['image']);
        $img_meta_array = sputnik_pix_wp_get_attachment($img_id);
        $twitter_link = vc_build_link($slide['twitter_link']);
        $facebook_link = vc_build_link($slide['facebook_link']);
        $google_plus_link = vc_build_link($slide['google_plus_link']);
        $instagram_link = vc_build_link($slide['instagram_link']);
        ?>
        <section class="b-team">
            <div class="b-team__media"><img src="<?php echo $img_meta_array['src']; ?>" alt="<?php echo $img_meta_array['alt']; ?>" class="img-responsive"/>
                <ul class="social-net list-inline">
                    <?php if (!empty($twitter_link['url'])) { ?>
                        <li class="social-net__item">
                            <a href="<?php echo $twitter_link['url']; ?>" class="social-net__link"><i class="icon fa fa-twitter"></i></a>
                        </li>
                    <?php } if (!empty($facebook_link['url'])) { ?>
                        <li class="social-net__item">
                            <a href="<?php echo $facebook_link['url']; ?>" class="social-net__link"><i class="icon fa fa-facebook"></i></a>
                        </li>
                    <?php } if (!empty($google_plus_link['url'])) { ?>
                        <li class="social-net__item">
                            <a href="<?php echo $google_plus_link['url']; ?>" class="social-net__link"><i class="icon fa fa-google-plus"></i></a>
                        </li>
                    <?php } if (!empty($instagram_link['url'])) { ?>
                        <li class="social-net__item">
                            <a href="<?php echo $instagram_link['url']; ?>" class="social-net__link"><i class="icon fa fa-instagram"></i></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="b-team__inner">
                <h3 class="b-team__name ui-title-inner"><?php echo $slide['name']; ?></h3>
                <div class="b-team__category"><?php echo $slide['position']; ?></div>
                <div class="ui-decor-1"></div>
                <div class="b-team__description"><?php echo $slide['text']; ?></div>
            </div>
        </section>
    <?php } ?>
</div>