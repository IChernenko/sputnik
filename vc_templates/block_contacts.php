<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $main_items [N]['text']
 * @var $main_items [N]['type']
 * @var $items [N]['title']
 * @var $items [N]['subtitle']
 * @var $items [N]['text']
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Contacts
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$main_items = (array)vc_param_group_parse_atts($main_items);
$items = (array)vc_param_group_parse_atts($items);
?>
    <div class="b-contacts-3">
        <div class="b-contacts-3__main-info">
        <?php foreach ($main_items as $item) {
            $font_icon = $item['icon_' . $item['type']]; ?>
            <div class="b-contacts-3__main-info__item"><i class="icon fa <?php echo $font_icon; ?>"></i><?php echo $item['text']; ?></div>
        <?php } ?>
        </div>
        <?php foreach ($items as $item) { ?>
            <div class="b-contacts-3__section">
                <h3 class="b-contacts-3__title"><?php echo $item['title']; ?></h3>
                <div class="b-contacts-3__subtitle"><?php echo $item['subtitle']; ?></div>
                <div class="b-contacts-3__info"><?php echo $item['text']; ?></div>
            </div>
        <?php } ?>
    </div>