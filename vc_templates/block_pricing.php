<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $style
 * @var $price_title
 * @var $price_number
 * @var $price_label
 * @var $box_icons_number
 * @var $button_link
 * @var $list_items [N]['text']
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Pricing
 */

$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$list_items = (array)vc_param_group_parse_atts($list_items);

$button_link = vc_build_link($button_link);
if ($style == 'A') { ?>
    <section class="b-pricing">
        <h3 class="b-pricing__title"><?php echo $price_title; ?></h3>
        <div class="b-pricing-price">$<span class="b-pricing-price__number"><?php echo $price_number; ?></span></div>
        <div class="b-pricing__info"><?php echo $price_label; ?></div>
        <span class="b-pricing__icon">
            <?php str_repeat('<i class="icon flaticon-box-1"></i>', (int)$box_icons_number); ?>
        </span>
        <ul class="b-pricing-list list-unstyled">
            <?php foreach ($list_items as $data) { ?>
                <li class="b-pricing-list__item"><?php echo $data['text']; ?></li>
            <?php } ?>
        </ul>
        <a href="<?php echo $button_link['url']; ?>" class="btn btn-default btn-w-ic btn-effect"><?php echo $button_link['title']; ?><i class="icon fa fa-long-arrow-right"></i></a>
    </section>
<?php } elseif ($style == 'B') { ?>
    <section class="b-pricing b-pricing_mod-a">
        <h3 class="b-pricing__title"><?php echo $price_title; ?></h3>
        <div class="b-pricing-price">$<span class="b-pricing-price__number"><?php echo $price_number; ?></span></div>
        <div class="b-pricing__info"><?php echo $price_label; ?></div>
        <span class="b-pricing__icon"><?php str_repeat('<i class="icon flaticon-box-1"></i>', (int)$box_icons_number); ?></span>
        <ul class="b-pricing-list list-unstyled">
            <?php foreach ($list_items as $data) { ?>
                <li class="b-pricing-list__item"><?php echo $data['text']; ?></li>
            <?php } ?>
        </ul>
        <a href="<?php echo $button_link['url']; ?>" class="btn btn-default btn-w-ic btn-effect"><?php echo $button_link['title']; ?><i class="icon fa fa-long-arrow-right"></i></a>
    </section>
<?php } elseif ($style == 'C') { ?>
    <section class="b-pricing b-pricing_mod-b">
        <h3 class="b-pricing__title"><?php echo $price_title; ?></h3>
        <div class="b-pricing-price">$<span class="b-pricing-price__number"><?php echo $price_number; ?></span></div>
        <div class="b-pricing__info"><?php echo $price_label; ?></div>
        <span class="b-pricing__icon">
            <?php str_repeat('<i class="icon flaticon-box-1"></i>', (int)$box_icons_number); ?>
        </span>
        <ul class="b-pricing-list list-unstyled">
            <?php foreach ($list_items as $data) { ?>
                <li class="b-pricing-list__item"><?php echo $data['text']; ?></li>
            <?php } ?>
        </ul><a href="<?php echo $button_link['url']; ?>" class="btn btn-default btn-w-ic btn-effect"><?php echo $button_link['title']; ?><i class="icon fa fa-long-arrow-right"></i></a>
    </section>
<?php } ?>

