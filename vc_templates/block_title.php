<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $style
 * @var $color
 * @var $title
 * @var $subtitle
 * @var $align
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Title
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);
if ($style == 'A'){
    $title = explode(' ', $title, 2);
?>
<div class="ui-subtitle-block ui-subtitle-block_first <?php echo $align;?>"><?php echo $subtitle;?></div>
<h2 class="ui-title-block <?php echo $align;?> <?php echo $color; ?>"><span class="ui-title-block__emphasis"><?php echo $title[0];?></span> <?php echo $title[1];?></h2>
<?php } elseif ($style == 'B') {?>
<h2 class="ui-title-type-1 <?php echo $color; ?>"><?php echo $title;?></h2>
<div class="ui-decor-1"></div>
<?php } ?>
