<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $button_text
 * @var $history_items [N]['date']
 * @var $history_items [N]['title']
 * @var $history_items [N]['text']
 * Shortcode class
 * @var $this WPBakeryShortCode_Section_History
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$history_items = (array)vc_param_group_parse_atts($history_items);

$widget_id = mt_rand(1000,9999);
?>

<script>

    jQuery(document).ready(function ($) {

        $("#show_more_history_<?php echo $widget_id;?>").on("click", function () {

            var hiddenContent = $("#history_<?php echo $widget_id;?> .js-scroll-content:hidden").first();

            console.log(hiddenContent);

            hiddenContent.show();

            if ($("#history_<?php echo $widget_id;?> .js-scroll-content:hidden").first().length == 0) {
                $("#show_more_history_<?php echo $widget_id;?>").hide();
            }

            hiddenContent.addClass("animated");
            hiddenContent.addClass("animation-done");
            hiddenContent.addClass("bounceInUp");

        });
    });

</script>

<div class="b-history" id="history_<?php echo $widget_id;?>">
    <?php for ($i = 0; $i < count($history_items); $i = $i + 3) { ?>
        <div class="js-scroll-content" style="<?php echo $i < 3 ? '' : 'display: none;' ?>">
                <?php if (isset($history_items[$i])) {
                    ?>
                    <section class="b-history__item">
                        <div class="b-history__date"><?php echo $history_items[$i]['date']; ?></div>
                        <div class="b-history__inner">
                            <h3 class="b-history__title ui-title-inner"><?php echo $history_items[$i]['title']; ?></h3>
                            <div class="ui-decor-1"></div>
                            <div class="b-history__info"><?php echo $history_items[$i]['text']; ?></div>
                        </div>
                    </section>
                <?php } ?>
                <?php if (isset($history_items[$i + 1])) {
                    ?>
                    <section class="b-history__item">
                        <div class="b-history__date"><?php echo $history_items[$i + 1]['date']; ?></div>
                        <div class="b-history__inner">
                            <h3 class="b-history__title ui-title-inner"><?php echo $history_items[$i + 1]['title']; ?></h3>
                            <div class="ui-decor-1"></div>
                            <div class="b-history__info"><?php echo $history_items[$i + 1]['text']; ?></div>
                        </div>
                    </section>
                <?php } ?>
                <?php if (isset($history_items[$i + 2])) {
                    ?>
                    <section class="b-history__item">
                        <div class="b-history__date"><?php echo $history_items[$i + 2]['date']; ?></div>
                        <div class="b-history__inner">
                            <h3 class="b-history__title ui-title-inner"><?php echo $history_items[$i + 2]['title']; ?></h3>
                            <div class="ui-decor-1"></div>
                            <div class="b-history__info"><?php echo $history_items[$i + 2]['text']; ?></div>
                        </div>
                    </section>
                <?php } ?>
        </div>
    <?php } ?>
    <span class="b-history__btn btn-link btn-link-sm js-scroll-next" id="show_more_history_<?php echo $widget_id;?>"><?php echo $button_text; ?><i class="icon fa fa-long-arrow-right"></i></span>
</div>


