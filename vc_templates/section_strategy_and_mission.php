<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $title_1
 * @var $text_1
 * @var $title_2
 * @var $text_2
 * Shortcode class
 * @var $this WPBakeryShortCode_Section_Strategy_And_Mission
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);

?>
<div class="section-type-2">
    <div class="container">
        <div class="section-full-width">
            <div class="block-table__cell block-table__cell_2-col">
                <div class="section-type-3">
                    <div class="section-type-3__inner pruning">
                        <h3 class="ui-title-inner"><?php echo $title_1; ?></h3>
                        <div class="ui-decor-1"></div>
                        <p><?php echo $text_1; ?></p>
                    </div>
                </div>
            </div>
            <div class="block-table__cell block-table__cell_2-col">
                <div class="section-type-3 section-type-3_color_white bg-primary">
                    <div class="section-type-3__inner pruning">
                        <h3 class="ui-title-inner"><?php echo $title_2; ?></h3>
                        <div class="ui-decor-1"></div>
                        <p><?php echo $text_2; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end section-type-2-->