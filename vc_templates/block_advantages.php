<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $style
 * @var $align
 * @var $lifted
 * @var $advantages_items [N]['type']
 * @var $advantages_items [N]['title']
 * @var $advantages_items [N]['text']
 * @var $advantages_items [N]['button_link']
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Advantages
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$advantages_items = (array)vc_param_group_parse_atts($advantages_items);
if ($style == 'A') {
    foreach ($advantages_items as $item) {
        $font_icon = $item['icon_' . $item['type']];
        ?>
        <section class="b-advantages-2 b-advantages-2_<?php echo $align; ?>-col"><i class="b-advantages-2__icon <?php echo $font_icon; ?>"></i>
            <h3 class="b-advantages-2__title ui-title-inner"><?php echo $item['title']; ?></h3>
            <div class="b-advantages-2__decor ui-decor-1"></div>
            <div class="b-advantages-2__info"><?php echo $item['text']; ?></div>
        </section>
    <?php }
} elseif ($style == 'B') {
    foreach ($advantages_items as $item) {
        ?>
        <section class="b-advantages-4"><i class="b-advantages-4__icon fa fa-check-square-o"></i>
            <h3 class="b-advantages-4__title ui-title-inner"><?php echo $item['title']; ?></h3>
            <div class="b-advantages-4__decor ui-decor-1"></div>
            <div class="b-advantages-4__info"><?php echo $item['text']; ?></div>
        </section>
    <?php }
} elseif ($style == 'C') {
    foreach ($advantages_items as $item) {
        $font_icon = $item['icon_' . $item['type']];
        ?>
        <section class="b-advantages-5"><i class="b-advantages-5__icon <?php echo $font_icon; ?>"></i>
            <h3 class="b-advantages-5__title ui-title-inner"><?php echo $item['title']; ?></h3>
            <div class="b-advantages-5__info"><?php echo $item['text']; ?></div>
        </section>
    <?php }
} elseif ($style == 'D') { ?>
    <div class="section-advantages-1 <?php echo $lifted; ?>">
        <?php foreach ($advantages_items as $item) {
            $font_icon = $item['icon_' . $item['type']];
            $button_link = vc_build_link($$item['button_link']);
            ?>
            <section class="b-advantages-1"><i class="b-advantages-1__icon <?php echo $font_icon; ?>" style="font-size: 44px;"></i>
                <h3 class="b-advantages-1__title ui-title-inner"><?php echo $item['title']; ?></h3>
                <div class="b-advantages-1__info"><?php echo $item['text']; ?></div>
                <a href="<?php echo $button_link['url']; ?>" class="b-advantages-1__btn btn-link btn-link-sm">read more<i class="icon fa fa-long-arrow-right"></i></a>
            </section>
            <!-- end b-advantages-->
        <?php } ?>
    </div>
<?php } elseif ($style == 'E') {
    foreach ($advantages_items as $item) {
        $font_icon = $item['icon_' . $item['type']];
        ?>
        <section class="b-advantages-3"><i class="b-advantages-3__icon <?php echo $font_icon; ?>"></i>
            <h3 class="b-advantages-3__title ui-title-inner"><a href="service-details.html" class="b-advantages-5__link"><?php echo $item['title']; ?></a></h3>
            <div class="b-advantages-3__decor ui-decor-1"></div>
            <div class="b-advantages-3__info"><?php echo $item['text']; ?></div>
        </section>
        <!-- end b-advantages-->
    <?php } ?>
<?php }