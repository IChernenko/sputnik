<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $title
 * @var $description
 * @var $button_link
 * @var $list_items [N]['text']
 * Shortcode class
 * @var $this WPBakeryShortCode_Section_Discount
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$list_items = (array)vc_param_group_parse_atts($list_items);
$button_link = vc_build_link($button_link);
?>
<div class="section-bg__inner">
    <h3 class="b-banners-1__title"><?php echo $title;?></h3>
    <div class="b-banners-1__subtitle"><?php echo $description;?></div>
    <div class="b-banners-1__decor"></div>
    <ul class="b-banners-1__list list list-mark-3 list_mod-a">
        <?php foreach ($list_items as $item) { ?>
            <li><?php echo $item['text'];?></li>
        <?php } ?>
    </ul>
    <a href="<?php echo $button_link['url'];?>" class="btn btn-primary btn-w-ic btn-sm btn-effect"><?php echo $button_link['title']; ?><i class="icon fa fa-long-arrow-right"></i></a>
</div>

