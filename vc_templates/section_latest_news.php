<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $button_link
 * Shortcode class
 * @var $this WPBakeryShortCode_Section_Latest_News
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
extract($atts);
$button_link = vc_build_link($button_link);

$args = array(
        'post_type' => 'post',
        'orderby' => 'post_date',
        'order' => 'DESC',
        'paged' => 1,
        'posts_per_page' => 1,
);
$the_query = new WP_Query($args);
if ($the_query->have_posts()) {
    $i = 0; ?>

    <?php while ($the_query->have_posts()) {
        $the_query->the_post();
        if ($i == 0) { ?>
            <div class="col-sm-6">
                <article class="b-post-2 clearfix">
                    <div class="entry-media">
                        <a href="<?php the_post_thumbnail_url(); ?>" class="js-zoom-images">
                            <?php echo get_the_post_thumbnail(get_the_ID(), array('555', '285'), array('class' => 'img-responsive')); ?>
                        </a>
                    </div>
                    <div class="entry-main">
                        <div class="entry-meta">
                        <span class="entry-meta__item">
                            <a href="<?php echo get_the_permalink(); ?>" class="entry-meta__link"><?php echo get_the_date('j M Y'); ?></a>
                        </span>
                        <span class="entry-meta__item">
                            <a href="<?php echo get_the_permalink() . '#comments'; ?>" class="entry-meta__link"><?php echo get_comments_number(get_the_ID()); ?> comments</a>
                        </span>
                        </div>
                        <div class="entry-header">
                            <h2 class="entry-title ui-title-inner"><?php echo get_the_title(); ?></h2>
                            <div class="ui-decor-1"></div>
                        </div>
                    </div>
                </article>
            </div>
        <?php }
        $i++;
    } ?>

<?php }
wp_reset_postdata();

$args = array(
        'post_type' => 'post',
        'orderby' => 'post_date',
        'order' => 'DESC',
        'paged' => 1,
        'posts_per_page' => 2,
        'offset' => 1,
);
$the_query = new WP_Query($args);
if ($the_query->have_posts()) { ?>
    <div class="col-sm-6">
        <div class="post-group-2">
            <?php while ($the_query->have_posts()) {
                $the_query->the_post(); ?>
                <article class="b-post-3 clearfix">
                    <div class="entry-media">
                        <a href="<?php the_post_thumbnail_url(); ?>" class="js-zoom-images">
                            <?php echo get_the_post_thumbnail(get_the_ID(), array('100', '90'), array('class' => 'img-responsive')); ?>
                        </a>
                    </div>
                    <div class="entry-main">
                        <div class="entry-meta">
                            <span class="entry-meta__item">
                                <a href="<?php echo get_the_permalink(); ?>" class="entry-meta__link"><?php echo get_the_date('j M Y'); ?></a>
                            </span>
                            <span class="entry-meta__item">
                                <a href="<?php echo get_the_permalink() . '#comments'; ?>" class="entry-meta__link"><?php echo get_comments_number(get_the_ID()); ?> comments</a>
                            </span>
                        </div>
                        <div class="entry-header">
                            <h2 class="entry-title ui-title-inner"><?php echo get_the_title(); ?></h2>
                            <div class="ui-decor-1"></div>
                        </div>
                    </div>
                </article>
            <?php } ?>
        </div>
        <a href="<?php echo $button_link['url'];?>" class="b-post-3__link-all btn-link btn-link-sm"><?php echo __('read more news', 'sputnik'); ?><i class="icon fa fa-long-arrow-right"></i></a>
    </div>

<?php }
wp_reset_postdata();
?>


