<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $style
 * @var $items [N]['title']
 * @var $items [N]['text']
 * @var $items [N]['type']
 * Shortcode class
 * @var $this WPBakeryShortCode_Section_Contacts
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$items = (array)vc_param_group_parse_atts($items);
?>

<?php if ($style == 'A') { ?>
    <div class="b-contacts-1 block-table">
        <?php foreach ($items as $item) { ?>
            <div class="b-contacts-1__item block-table__cell">
                <div class="b-contacts-1__title"><?php echo $item['title']; ?></div>
                <div class="b-contacts-1__info"><?php echo do_shortcode($item['text']); ?></div>
            </div>
        <?php } ?>
    </div>
<?php } elseif ($style == 'B') { ?>
    <div class="section-contacts-2">
        <div class="b-contacts-2 block-table">
            <?php foreach ($items as $item) {
                $font_icon = $item['icon_' . $item['type']]; ?>
                <div class="b-contacts-2__item block-table__cell">
                    <div class="b-contacts-2__wrap"><i class="b-contacts-2__icon fa <?php echo $font_icon; ?>"></i>
                        <div class="b-contacts-2__inner">
                            <div class="b-contacts-2__title"><?php echo $item['title']; ?></div>
                            <div class="b-contacts-2__info b-contacts-2__info_lg"><?php echo do_shortcode($item['text']); ?></div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php }