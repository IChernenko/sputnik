<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $images
 * Shortcode class
 * @var $this WPBakeryShortCode_Section_Brand_Photos
 */
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$images = explode(',', $images);

$images_html = array();

foreach ($images as $image){
    $img_id = preg_replace('/[^\d]/', '', $image);
    $img_meta_array = sputnik_pix_wp_get_attachment($img_id);
    $images_html []= '<div class="b-brands__item">
            <img src="' . esc_url($img_meta_array['src']) . '" alt="' . $img_meta_array['alt'] . '" class="img-responsive"/>
        </div>';
}

echo '<div data-min480="2" data-min768="3" data-min992="5" data-min1200="5" data-pagination="true" data-navigation="false" data-auto-play="4000" data-stop-on-hover="true" class="b-brands b-brands_mod-b owl-carousel owl-theme enable-owl-carousel">' . implode('', $images_html) . '</div>';


