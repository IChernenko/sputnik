<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $categories [N]['category_id']
 * @var $categories [N]['image']
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Services_Categories_Slider
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$categories = (array)vc_param_group_parse_atts($categories); ?>

<div class="post-group-1">
    <div data-min480="1" data-min768="2" data-min992="3" data-min1200="3" data-pagination="true" data-navigation="false" data-auto-play="40000" data-stop-on-hover="true" class="owl-carousel owl-theme enable-owl-carousel">
        <?php foreach ($categories as $category) {
            $img_id = preg_replace('/[^\d]/', '', $category['image']);
            $img_meta_array = sputnik_pix_wp_get_attachment($img_id);
            $category = get_category($category['category_id']);
            ?>
            <article class="b-post b-post-1 clearfix">
                <div class="entry-media"><a href="<?php echo $img_meta_array['src']; ?>" class="js-zoom-images"><img src="<?php echo $img_meta_array['src']; ?>" alt="<?php echo $img_meta_array['alt']; ?>" class="img-responsive"/></a></div>
                <div class="entry-main">
                    <div class="entry-header">
                        <h2 class="entry-title ui-title-inner"><?php echo $category->name; ?></h2>
                        <div class="ui-decor-1"></div>
                    </div>
                    <div class="entry-content">
                        <p><?php echo $category->category_description; ?></p>
                    </div>
                </div>
            </article>
            <!-- end post-->
            <!--    --><?php //echo get_category_link($category->term_id); ?>
        <?php } ?>
    </div>
</div>