<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $style
 * @var $bold
 * @var $items [N]['text']
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Unordered_List
 */

$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);

$items = (array)vc_param_group_parse_atts($items);

?>
<<?php echo $style == 'list-num list-num-1'? 'ol' : 'ul'; ?> <?php if ($bold) {echo 'style="color:#333;"';} else { echo 'style="color:#888;"';} ?>  class="list <?php echo $style; ?> <?php if ($bold) {echo 'list_mod-a';} else { echo 'typography-last-elem';} ?>">
<?php foreach ($items as $data) { ?>
    <li><?php echo $data['text']; ?></li>
<?php } ?>
</<?php echo $style == 'list-num list-num-1'? 'ol' : 'ul'; ?>>