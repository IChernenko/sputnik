<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $color
 * @var $slides [N]['name']
 * @var $slides [N]['position']
 * @var $slides [N]['title']
 * @var $slides [N]['text']
 * @var $slides [N]['image']
 * Shortcode class
 * @var $this WPBakeryShortCode_Block_Testimonials_Slider
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$slides = (array)vc_param_group_parse_atts($slides);
?>

<div data-pagination="true" data-navigation="false" data-single-item="true" data-auto-play="7000" data-transition-style="fade" data-main-text-animation="true" data-after-init-delay="3000" data-after-move-delay="1000" data-stop-on-hover="true" class="owl-carousel owl-theme owl-theme_left-controls enable-owl-carousel">

    <?php foreach ($slides as $slide) {
        $img_id = preg_replace('/[^\d]/', '', $slide['image']);
        $img_meta_array = sputnik_pix_wp_get_attachment($img_id);
        ?>
        <article class="b-reviews-1 <?php echo $color; ?>">
            <div class="b-reviews-1__header">
                <div class="b-reviews-1__foto">
                    <img src="<?php echo $img_meta_array['src']; ?>" alt="<?php echo $img_meta_array['alt']; ?>" class="img-responsive"/>
                </div>
                <div class="b-reviews-1__wrap">
                    <div class="b-reviews-1__name"><?php echo $slide['name']; ?></div>
                    <div class="b-reviews-1__categories"><?php echo $slide['position']; ?></div>
                </div>
            </div>
            <h3 class="b-reviews-1__title"><?php echo $slide['title']; ?></h3>
            <div class="b-reviews-1__content">
                <p><?php echo $slide['text']; ?></p>
            </div>
        </article>
    <?php } ?>
</div>