<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $style
 * @var $progress_list_items[N]['type']
 * @var $progress_list_items[N]['quantity']
 * @var $progress_list_items[N]['title']
 * Shortcode class
 * @var $this WPBakeryShortCode_Section_Progress_List
 */
$atts = vc_map_get_attributes($this->getShortcode(), $atts);
$atts = $this->convertAttributesToArray($atts);
extract($atts);
$progress_list_items = (array)vc_param_group_parse_atts($progress_list_items);

?>
<ul class="b-progress-list b-progress-list_mod-<?php echo $style;?> list-unstyled">
    <?php foreach ($progress_list_items as $item){
        $font_icon = $item['icon_' . $item['type']];
        ?>
        <li class="b-progress-list__item clearfix">
            <div class="b-progress-list__wrap">
                <i class="b-progress-list__icon <?php echo $font_icon;?>"></i>
                <div class="b-progress-list__inner"><span data-percent="<?php echo $item['quantity'];?>" class="b-progress-list__percent js-chart"><span class="js-percent"></span></span><span class="b-progress-list__name"><?php echo $item['title'];?></span></div>
            </div>
        </li>
    <?php } ?>
</ul>