<footer class="footer">
    <div class="footer__main section-bg section-bg_dark parallax-bg">
        <ul class="bg-slideshow">
            <li>
                <div style="background-image:url(assets/media/components/footer/bg.jpg)" class="bg-slide"></div>
            </li>
        </ul>
        <div class="section-bg__inner">
            <div class="container">
                <div class="row">
                    <?php
                    $footer_post = get_theme_mod('footer_choice_section');
                    // The Query
                    $the_query = new WP_Query(array('p' => (int)$footer_post, 'post_type' => 'staticblocks'));

                    // The Loop
                    if ($the_query->have_posts()) :
                        while ($the_query->have_posts()) : $the_query->the_post();
                            the_content();
                        endwhile;
                        /* Restore original Post Data */
                        wp_reset_postdata();
                    else :
                        // no posts found
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- end footer__main-->

    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-5">© 2016<span class="copyright__emphasis"> Sputnik Logistics</span> All rights reserved.</div>
                <div class="col-lg-4 col-md-2">
                    <ul class="social-net list-inline">
                        <li class="social-net__item"><a href="twitter.com" class="social-net__link"><i class="icon fa fa-twitter"></i></a></li>
                        <li class="social-net__item"><a href="facebook.com" class="social-net__link"><i class="icon fa fa-facebook"></i></a></li>
                        <li class="social-net__item"><a href="plus.google.com" class="social-net__link"><i class="icon fa fa-google-plus"></i></a></li>
                        <li class="social-net__item"><a href="vimeo.com" class="social-net__link"><i class="icon fa fa-vimeo"></i></a></li>
                        <li class="social-net__item"><a href="instagram.com" class="social-net__link"><i class="icon fa fa-instagram"></i></a></li>
                    </ul>
                    <!-- end social-list-->
                </div>
                <div class="col-lg-4 col-md-5">
                    <div class="copyright-links"><a href="terms-of-use.html" class="copyright-links__item"> Terms of Use</a><a href="privacy-policy.html" class="copyright-links__item"> Privacy Policy</a><a href="terms-of-use.html" class="copyright-links__item"> Tracking</a></div>
                </div>
            </div>
        </div>
    </div>
    <!-- end copyright-->
</footer>
<!-- end footer-->
</div>
<!-- end layout-theme-->
<?php wp_footer(); ?>
</body>
</html>