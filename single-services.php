<?php
/*** The template for displaying all pages. ***/

get_header();

$sputnik_custom = isset( $wp_query ) ? get_post_custom( $wp_query->get_queried_object_id() ) : '';
$sputnik_layout = isset( $sputnik_custom['pix_page_layout'] ) ? $sputnik_custom['pix_page_layout'][0] : '2';
$sputnik_sidebar = isset( $sputnik_custom['pix_selected_sidebar'][0] ) ? $sputnik_custom['pix_selected_sidebar'][0] : 'sidebar-1';

if ( ! is_active_sidebar($sputnik_sidebar) ) $sputnik_layout = '1';

?>

	<!-- =========================
		PAGE
	============================== -->
	<div class="page-section">
		<div class="container">
			<div class="row">

				<?php sputnik_show_sidebar( 'left', $sputnik_layout, $sputnik_sidebar ); ?>

				<!-- === BLOG ITEMS === -->

				<div class="<?php if ( $sputnik_layout == 1 ) : ?>col-lg-12 col-md-12 col-sm-12<?php else : ?>col-lg-9 col-md-9 col-sm-9<?php endif; ?> col-xs-12 blog-items-<?php echo esc_attr($sputnik_layout); ?>">

					<?php
					// Start the Loop.
					while ( have_posts() ) : the_post();
						the_content();

					endwhile;
					?>

				</div>

				<?php sputnik_show_sidebar( 'right', $sputnik_layout, $sputnik_sidebar ); ?>

			</div>
		</div>
	</div>
	<!-- =========================
		END PAGE
	============================== -->

<?php get_footer(); ?>