<?php
$sputnik_header_image = sputnik_get_option( 'header_general_settings_headerimage' );
$sputnik_header_title_css_animation = ( sputnik_get_option('css_animation_settings_header_title') != '' ) ? ' wow '.sputnik_get_option('css_animation_settings_header_title') : '';
$sputnik_header_add_class = ( sputnik_get_option( 'header_general_settings_show_button', 'on' ) == 'on' ) ? 'col-md-offset-2 col-md-8' : 'col-md-12';
?>
<!-- ===================================
	PAGE HEADER
======================================== -->
<div class="page-header" data-stellar-background-ratio="0.4" style="background-image: url(
	<?php
	if ( is_page() ) :

		if ( has_post_thumbnail() ) :
			$post_thumbnail_id = get_post_thumbnail_id($post->ID);
			$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id );
			echo esc_url($post_thumbnail_url); ?>);" >
		<?php
		elseif ( !empty( $sputnik_header_image ) ) :
			echo esc_url( $sputnik_header_image ); ?>);">
		<?php
		else : ?>
			);">
		<?php
		endif;

	else :

		if ( !empty( $sputnik_header_image ) ) :
			echo esc_url( $sputnik_header_image ); ?>);">
		<?php
		else : ?>
			);">
		<?php
		endif;

	endif; ?>
	<?php if ( sputnik_get_option( 'header_general_settings_headerimage_overlay', 'on' ) == 'on' ) : ?>
		<span class="page-header-overlay" style="background-color: rgba(0, 0, 0, <?php echo esc_attr( sputnik_get_option( 'header_general_settings_headerimage_opacity', '0.1' ) ); ?>) !important;"></span>
	<?php endif; ?>
	<div class="container">
		<div class="row">

			<div class="<?php echo $sputnik_header_add_class . ' ' . esc_attr($sputnik_header_title_css_animation); ?>">
				<!-- === PAGE HEADER TITLE === -->
				<?php require_once( get_template_directory() . '/templates/header/header_title.php' ); ?>

				<!-- === PAGE HEADER BREADCRUMB === -->


				<div class="page-header-breadcrumb">
					<?php if ( function_exists( 'sputnik_pix_breadcrumbs' ) && ! is_page_template( 'home-template.php' ) && sputnik_get_option( 'general_settings_breadcrumbs', 'on' ) == 'on' ) {
						sputnik_pix_breadcrumbs();
					}
					?>
				</div>
			</div>

		<?php
		if ( sputnik_get_option( 'header_general_settings_show_button', 'on' ) == 'on' ) :
		?>
			<!-- === PAGE HEADER BUTTON === -->
			<div class="col-md-2 page-header-button">
				<?php
				if ( sputnik_get_option( 'header_general_settings_link_button' ) != '' ) :
				?>
				<a href="<?php echo esc_url(sputnik_get_option( 'header_general_settings_link_button' ) ); ?>">
				<?php
				endif;
				?>
					<div class="my-btn my-btn-primary">
						<div class="my-btn-bg-top"></div>
						<div class="my-btn-bg-bottom"></div>
						<div class="my-btn-text">
							<?php echo sputnik_get_option( 'header_general_settings_text_button', esc_html__('Get a free quote', 'sputnik' ) );?>
						</div>
					</div>
				<?php
				if ( sputnik_get_option( 'header_general_settings_link_button' ) != '' ) :
				?>
				</a>
				<?php
				endif;
				?>
			</div>
		<?php
		endif;
		?>
		</div>
	</div>
</div>
<!-- ===================================
	END PAGE HEADER
======================================== -->