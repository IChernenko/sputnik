<?php /* Header Top Bar template */ ?>

<!-- =========================
TOP BAR
============================== -->
<div class="top-bar">
	<div class="container">

		<!-- === TOP BAR SOCIAL ICONS === -->

		<div class="top-bar-social">
			<?php
			if ( sputnik_get_option( 'header_topbar_settings_soclink1' ) != '' && sputnik_get_option( 'header_topbar_settings_socicon1' ) != '' ) : ?>
			<a href="<?php echo esc_url(sputnik_get_option( 'header_topbar_settings_soclink1' )); ?>"><div class="my-btn my-btn-primary">
				<div class="my-btn-bg-top"></div>
				<div class="my-btn-bg-bottom"></div>
				<div class="my-btn-text">
					<i class="fa <?php echo esc_attr(sputnik_get_option( 'header_topbar_settings_socicon1' )); ?>"></i>
				</div>
			</div></a>
			<?php
			endif; ?>
			<?php
			if ( sputnik_get_option( 'header_topbar_settings_soclink2' ) != '' && sputnik_get_option( 'header_topbar_settings_socicon2' ) != '' ) : ?>
			<a href="<?php echo esc_url(sputnik_get_option( 'header_topbar_settings_soclink2' )); ?>"><div class="my-btn my-btn-primary">
				<div class="my-btn-bg-top"></div>
				<div class="my-btn-bg-bottom"></div>
				<div class="my-btn-text">
					<i class="fa <?php echo esc_attr(sputnik_get_option( 'header_topbar_settings_socicon2' )); ?>"></i>
				</div>
			</div></a>
			<?php
			endif; ?>
			<?php
			if ( sputnik_get_option( 'header_topbar_settings_soclink3' ) != '' && sputnik_get_option( 'header_topbar_settings_socicon3' ) != '' ) : ?>
			<a href="<?php echo esc_url(sputnik_get_option( 'header_topbar_settings_soclink3' )); ?>"><div class="my-btn my-btn-primary">
				<div class="my-btn-bg-top"></div>
				<div class="my-btn-bg-bottom"></div>
				<div class="my-btn-text">
					<i class="fa <?php echo esc_attr(sputnik_get_option( 'header_topbar_settings_socicon3' )); ?>"></i>
				</div>
			</div></a>
			<?php
			endif; ?>
			<?php
			if ( sputnik_get_option( 'header_topbar_settings_soclink4' ) != '' && sputnik_get_option( 'header_topbar_settings_socicon4' ) != '' ) : ?>
			<a href="<?php echo esc_url(sputnik_get_option( 'header_topbar_settings_soclink4' )); ?>"><div class="my-btn my-btn-primary">
				<div class="my-btn-bg-top"></div>
				<div class="my-btn-bg-bottom"></div>
				<div class="my-btn-text">
					<i class="fa <?php echo esc_attr(sputnik_get_option( 'header_topbar_settings_socicon4' )); ?>"></i>
				</div>
			</div></a>
			<?php
			endif; ?>
			<?php
			if ( sputnik_get_option( 'header_topbar_settings_soclink5' ) != '' && sputnik_get_option( 'header_topbar_settings_socicon5' ) != '' ) : ?>
			<a href="<?php echo esc_url(sputnik_get_option( 'header_topbar_settings_soclink5' )); ?>"><div class="my-btn my-btn-primary">
				<div class="my-btn-bg-top"></div>
				<div class="my-btn-bg-bottom"></div>
				<div class="my-btn-text">
					<i class="fa <?php echo esc_attr(sputnik_get_option( 'header_topbar_settings_socicon5' )); ?>"></i>
				</div>
			</div></a>
			<?php
			endif; ?>

		</div>

		<!-- === TOP BAR PHONE === -->
		<?php
		if ( sputnik_get_option( 'header_topbar_settings_phone' ) != '' ) : ?>
		<div class="top-bar-phone">
			<div class="top-bar-phone-icon">
				<i class="fa fa-phone"></i>
			</div>
			<?php echo wp_kses_post( sputnik_get_option( 'header_topbar_settings_phone' ) ); ?>
		</div>
		<?php
		endif; ?>

		<!-- === TOP BAR E-MAIL === -->
		<?php
		if ( sputnik_get_option( 'header_topbar_settings_email' ) != '' ) : ?>
		<div class="top-bar-mail">
			<div class="top-bar-mail-icon">
				<i class="fa fa-envelope"></i>
			</div>
			<a href="mailto:<?php echo esc_attr( sputnik_get_option( 'header_topbar_settings_email' ) ); ?>">
				<?php echo wp_kses_post( sputnik_get_option( 'header_topbar_settings_email' ) ); ?>
			</a>
		</div>
		<?php
		endif; ?>

	</div>
</div>
<!-- =========================
	END TOP BAR
============================== -->