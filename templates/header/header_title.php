<?php /* Header Title template */ ?>

<?php
	$sputnik_pix_postpage_id = get_option( 'page_for_posts' );
	$sputnik_pix_frontpage_id = get_option( 'page_on_front' );
	$sputnik_pix_page_id = isset( $wp_query ) ? $wp_query->get_queried_object_id() : '';
?>

<div class="page-header-title">
	<h2>
		<?php
			if (is_single() && ! is_attachment() && get_post_type() == 'post' ) :
				echo wp_kses_post( sputnik_get_option('header_general_settings_title_single_post', esc_html__('Blog details', 'sputnik' ) ) );
			elseif (is_single() && ! is_attachment() && get_post_type() == 'portfolio' ) :
				echo wp_kses_post( sputnik_get_option('header_general_settings_title_single_service', esc_html__('Service details', 'sputnik' ) ) );
			elseif ( is_post_type_archive() ) :
				 echo post_type_archive_title( '', false );
			elseif ( is_tax() ) :
				echo single_term_title( '', false );
			elseif ( is_archive() ) :
				echo wp_kses_post( get_the_archive_title( ) );
			elseif ( is_search() ) :
				echo wp_kses_post( sputnik_get_option('header_general_settings_title_search_results', esc_html__('Search results', 'sputnik' ) ) );
			elseif (  is_404() ) :
				echo wp_kses_post( esc_html__( '404', 'sputnik' ) );
			elseif ( $sputnik_pix_frontpage_id == $sputnik_pix_page_id && $sputnik_pix_page_id == $sputnik_pix_postpage_id ) :
				echo wp_kses_post( sputnik_get_option('header_general_settings_title_all_posts', esc_html__('All posts', 'sputnik' ) ) );
			elseif ( isset($post->ID) && $post->ID > 0 ) :
				echo wp_kses_post( get_the_title($sputnik_pix_page_id) );
			else :
				echo wp_kses_post( get_the_title() );
			endif;
		?>
	</h2>
</div>