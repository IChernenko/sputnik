<?php

// get meta options/values
$sputnik_quote_content = rwmb_meta('post_quote_content');
$sputnik_quote_source = rwmb_meta('post_quote_source');

?>
<div class="post-quotes">
	<blockquote>
		<p><?php echo wp_kses_post($sputnik_quote_content); ?></p>
		<footer><?php echo wp_kses_post($sputnik_quote_source); ?></footer>
	</blockquote>
</div>