<?php
/**
 * The template includes blog post format gallery.
 *
 * @package sputnik
 * @since 1.0
 */

$sputnik_postpage_id = get_option( 'page_for_posts' );
$sputnik_frontpage_id = get_option( 'page_on_front' );
$sputnik_page_id = isset($wp_query) ? $wp_query->get_queried_object_id() : '';

if ( ( $sputnik_page_id == $sputnik_postpage_id && $sputnik_postpage_id != $sputnik_frontpage_id ) || is_single() ) :
	$sputnik_custom = isset ($wp_query) ? get_post_custom($wp_query->get_queried_object_id()) : '';
	$sputnik_layout = isset ($sputnik_custom['sputnik_page_layout']) ? $sputnik_custom['sputnik_page_layout'][0] : '2';
else :
	$sputnik_layout = sputnik_get_option('blog_settings_sidebar_type', '2');
endif;

$sputnik_size_thumb = ( $sputnik_layout == '1' ) ? 'sputnik-post-thumb-large' : 'sputnik-post-thumb-middle';

// get the gallery images
$gallery = rwmb_meta('post_gallery', 'type=image&size='.$sputnik_size_thumb.'');

$argsThumb = array(
	'order'          => 'ASC',
	'post_type'      => 'attachment',
	'post_parent'    => $post->ID,
	'post_mime_type' => 'image',
	'post_status'    => null,
	//'exclude' => get_post_thumbnail_id()
);
$attachments = get_posts($argsThumb);


if ( $gallery && count( $gallery ) > 1 || $attachment && count( $attachment ) > 1 ) : ?>

<div class="blog-item-img blog-item-gallery">
	<div class="owl-carousel enable-owl-carousel owl-theme" data-autoplay="4000" data-itemmobile = "1" data-itemtablet = "1" data-itemdesktop="1">
	<?php
		if ( $gallery ) :
			foreach ( $gallery as $slide ) {
				echo '<img src="' . esc_url( $slide['url'] ) . '" width="' . esc_attr( $slide['width'] ) . '" height="' . esc_attr( $slide['height'] ) . '" alt="' .esc_attr( $slide['alt'] ).'" title="' .esc_attr( $slide['title'] ). '" >';
			}
		elseif ( $attachments ) :
			foreach ( $attachments as $attachment ) {
				echo '
					<img src="'.esc_url( wp_get_attachment_url( $attachment->ID, 'full', false, false ) ).'" alt="'.esc_attr( get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ) ).'" title="'.esc_attr( get_post_meta( $attachment->ID, '_wp_attachment_image_title', true ) ).'" >
				';
			}
		endif;
	?>

	</div>
</div>
<?php

elseif ( $gallery && count( $gallery ) == 1 || $attachment && count( $attachment ) == 1 ) : ?>

	<div class="blog-item-img">

		<?php
			if ( $gallery ) :
				foreach ( $gallery as $slide ) {
					if ( is_single() ) :
						echo '<img src="' . esc_url( $slide['url'] ) . '" width="' . esc_attr( $slide['width'] ) . '" height="' . esc_attr( $slide['height'] ) . '" alt="' .esc_attr( $slide['alt'] ).'" title="' .esc_attr( $slide['title'] ). '" >';
					else :
						echo '<a href="'.esc_url( get_the_permalink() ).'">';
						echo '<img src="' . esc_url( $slide['url'] ) . '" width="' . esc_attr( $slide['width'] ) . '" height="' . esc_attr( $slide['height'] ) . '" alt="' .esc_attr( $slide['alt'] ).'" title="' .esc_attr( $slide['title'] ). '" >';
						echo '</a>';
					endif;
				}
			elseif ( $attachments ) :
				foreach ( $attachments as $attachment ) {
					echo '
						<div>
							<img src="'.esc_url( wp_get_attachment_url( $attachment->ID, 'full', false, false ) ).'" alt="'.esc_attr( get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ) ).'" title="'.esc_attr( get_post_meta( $attachment->ID, '_wp_attachment_image_title', true ) ).'" >
						</div>
					';
				}
			endif;

		?>

	</div>

	<?php
else : ?>

<div class="blog-item-image">
	<?php if ( is_single() ) : ?>

		<?php if ( has_post_thumbnail() ) : ?>
			<?php the_post_thumbnail( $size = $sputnik_size_thumb ); ?>
		<?php endif; ?>

	<?php else : ?>

		<?php if ( has_post_thumbnail() ) : ?>
			<a href="<?php esc_url( the_permalink() ); ?>">
			<?php the_post_thumbnail( $size = $sputnik_size_thumb ); ?>
			</a>
		<?php endif; ?>

	<?php endif; ?>
</div>

<?php
endif;
