<?php
/**
 * This template is for displaying part of blog format video.
 *
 * @package Pix-Theme
 * @since 1.0
 */

$sputnik_postpage_id = get_option( 'page_for_posts' );
$sputnik_frontpage_id = get_option( 'page_on_front' );
$sputnik_page_id = isset($wp_query) ? $wp_query->get_queried_object_id() : '';

if ( ( $sputnik_page_id == $sputnik_postpage_id && $sputnik_postpage_id != $sputnik_frontpage_id ) || is_single() ) :
	$sputnik_custom = isset ($wp_query) ? get_post_custom($wp_query->get_queried_object_id()) : '';
	$sputnik_layout = isset ($sputnik_custom['sputnik_page_layout']) ? $sputnik_custom['sputnik_page_layout'][0] : '2';
else :
	$sputnik_layout = sputnik_get_option('blog_settings_sidebar_type', '2');
endif;

$sputnik_size_thumb = ( $sputnik_layout == '1' ) ? 'sputnik-post-thumb-large' : 'sputnik-post-thumb-middle';

?>

<?php if ( get_post_meta( get_the_ID(), 'post_video', true ) != '' ) : ?>

<div class="blog-item_img wrap-video embed-responsive embed-responsive-16by9">
	<?php echo rwmb_meta('post_video', 'type=oembed'); ?>
</div>

<?php else : ?>

<div class="blog-item-image">
	<?php if ( is_single() ) : ?>

		<?php if ( has_post_thumbnail() ) : ?>
			<?php the_post_thumbnail( $size = $sputnik_size_thumb ); ?>
		<?php endif; ?>

	<?php else : ?>

		<?php if ( has_post_thumbnail() ) : ?>
			<a href="<?php esc_url( the_permalink() ); ?>">
			<?php the_post_thumbnail( $size = $sputnik_size_thumb ); ?>
			</a>
		<?php endif; ?>

	<?php endif; ?>
</div>

<?php endif; ?>



