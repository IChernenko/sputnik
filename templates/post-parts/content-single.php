<?php
/**
 * This template is for displaying part of blog.
 *
 * @package Pix-Theme
 * @since 1.0
 */
$sputnik_post_format  = get_post_format();
$sputnik_post_format = !in_array($sputnik_post_format, array("quote", "gallery", "video", "audio")) ? "standared" : $sputnik_post_format;

$sputnik_text_readmore = sputnik_get_option('blog_settings_readmore');
$sputnik_post_date = strtotime($post->post_date);
?>

<div class="blog-single-post-item clearfix">

	<?php if ( class_exists( 'RW_Meta_Box' ) ) : ?>
	<?php get_template_part( 'templates/post-parts/post-format/blog', $sputnik_post_format ); ?>
	<?php else : ?>
	<?php get_template_part( 'templates/post-parts/post-format/blog', 'default' ); ?>
	<?php endif; ?>

	<div class="blog-single-post-date">
		<?php echo sprintf( '<div class="blog-single-post-date-number">%s</div>%s', esc_attr( date( 'd', $sputnik_post_date ) ), esc_attr( date( 'M', $sputnik_post_date ) ) ); ?>
	</div>
	<div class="blog-single-post-info">
		<?php if ( sputnik_get_option( 'blog_show_author', 'on' ) == 'on' ) : ?>
		<span class="author-icon"><i class="fa fa-user"></i></span>
		<?php the_author_posts_link(); ?>
		<?php endif; ?>
		<?php if ( sputnik_get_option( 'blog_show_date', 'on' ) == 'on' ) : ?>
		<span class="date-icon"><i class="fa fa-calendar"></i></span>
		<?php echo get_the_date(); ?>
		<?php endif; ?>
		<?php if ( comments_open() ) : ?>
		<span class="comments-icon"><i class="fa fa-comment"></i></span>
		<?php comments_popup_link( esc_html__( 'Post a Comment', 'sputnik' ), esc_html__( '1 comment', 'sputnik' ), esc_html__( '% comments', 'sputnik' ), "comments-link"); ?>
		<?php endif; ?>
	</div>
	<div class="blog-single-post-title">
		<h3><?php the_title(); ?></h3>
	</div>
	<div class="blog-single-post-text rtd">
		<?php the_content(); ?>
	</div>
	<div class="more-page">
		<?php
			$args = array(
			 'link_before'      => '<span>'
			,'link_after'       => '</span>' );

			wp_link_pages( $args );
		?>
	</div>
</div>

