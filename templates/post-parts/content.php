<?php
/**
 * This template is for displaying part of blog.
 *
 * @package Pix-Theme
 * @since 1.0
 */
$sputnik_post_format  = get_post_format();
$sputnik_post_format = !in_array($sputnik_post_format, array("quote", "gallery", "video", "audio")) ? "standared" : $sputnik_post_format;

$sputnik_text_readmore = sputnik_get_option('blog_settings_readmore', esc_html__( 'Read more', 'sputnik' ));
$sputnik_post_date = strtotime($post->post_date);
?>

<!-- === BLOG ITEM === -->
<div id="post-<?php esc_attr( the_ID() ); ?>" <?php post_class( 'blog-item' ); ?>>

	<?php if ( class_exists( 'RW_Meta_Box' ) ) : ?>
	<?php get_template_part( 'templates/post-parts/post-format/blog', $sputnik_post_format ); ?>
	<?php else : ?>
	<?php get_template_part( 'templates/post-parts/post-format/blog', 'default' ); ?>
	<?php endif; ?>

	<div class="blog-item-date">
		<?php echo sprintf( '<div class="blog-item-date-number">%s</div>%s', esc_attr( date( 'd', $sputnik_post_date ) ), esc_attr( date( 'M', $sputnik_post_date ) ) ); ?>
	</div>
	<div class="blog-item-info">
		<?php if ( sputnik_get_option( 'blog_show_author', 'on' ) == 'on' ) : ?>
		<span class="author-icon"><i class="fa fa-user"></i></span>
		<?php the_author_posts_link(); ?>
		<?php endif; ?>
		<?php if ( sputnik_get_option( 'blog_show_date', 'on' ) == 'on' ) : ?>
		<span class="date-icon"><i class="fa fa-calendar"></i></span>
		<a href="<?php esc_url( the_permalink() ); ?>"><?php echo get_the_date(); ?></a>
		<?php endif; ?>
		<?php if ( comments_open() ) : ?>
		<span class="comments-icon"><i class="fa fa-comment"></i></span>
		<?php comments_popup_link( esc_html__( 'Post a Comment', 'sputnik' ), esc_html__( '1 comment', 'sputnik' ), esc_html__( '% comments', 'sputnik' ), "comments-link"); ?>
		<?php endif; ?>
	</div>
	<div class="blog-item-title">
		<h3><a href="<?php esc_url( the_permalink() ); ?>"><?php the_title(); ?></a></h3>
	</div>
	<div class="blog-item-text"><?php echo do_shortcode( get_the_excerpt() ); ?></div>
	<div class="more-page">
		<?php
			$args = array(
			 'link_before'      => '<span>'
			,'link_after'       => '</span>' );

			wp_link_pages( $args );
		?>
	</div>
	<div class="blog-item-button">
		<a href="<?php esc_url( the_permalink() ); ?>" title="<?php esc_attr( the_title() ); ?>"><div class="my-btn">
			<div class="my-btn-bg-top"></div>
			<div class="my-btn-bg-bottom"></div>
			<div class="my-btn-text">
				<?php echo esc_attr( $sputnik_text_readmore ); ?>
			</div>
		</div></a>
	</div>
</div>
