<?php
/**
 * The template for displaying a "No posts found" message
 *
 */
?>

<div class="blog-item">
	<div class="blog-item-title">
		<h3><?php _e( 'Nothing Found', 'sputnik' ) ?></h3>
	</div>

	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

	<p><?php echo __( 'Ready to publish your first post?', 'sputnik' );?></p>
	<p><?php printf( '<a href="%1$s">' . esc_html__( 'Get started here', 'sputnik' ) . '</a>', esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

	<?php elseif ( is_search() ) : ?>

	<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'sputnik' ); ?></p>
	<?php get_search_form(); ?>

	<?php else : ?>

	<p><?php esc_html_e( 'It seems we can\'t find what you\'re looking for. Perhaps searching can help.', 'sputnik' ); ?></p>
	<?php get_search_form(); ?>

	<?php endif; ?>
</div>
