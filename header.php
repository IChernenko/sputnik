<?php
/**
 * Header Template
 *
 * Here we setup all logic and XHTML that is required for the header section of all screens.
 *
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<!-- Loader-->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end-->

<div data-header="sticky" data-header-top="200" class="l-theme animated-css">
    <!-- Start Switcher-->
    <div class="switcher-wrapper">
        <div class="demo_changer">
            <div class="demo-icon color-primary"><i class="fa fa-cog fa-spin fa-2x"></i></div>
            <div class="form_holder">
                <div class="predefined_styles">
                    <div class="skin-theme-switcher">
                        <h4>Color</h4><a href="javascript:void(0);" data-switchcolor="color1" style="background-color:#e95615;" class="styleswitch"></a><a href="javascript:void(0);" data-switchcolor="color2" style="background-color:#5aaff7;" class="styleswitch"></a><a href="javascript:void(0);" data-switchcolor="color3" style="background-color:#28af0f;" class="styleswitch"></a><a href="javascript:void(0);" data-switchcolor="color4" style="background-color:#e425e9;" class="styleswitch"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end switcher-->

    <!-- ==========================-->
    <!-- MOBILE MENU-->
    <!-- ==========================-->
    <div off-canvas="mobile-slidebar left overlay"><a href="home.html" class="navbar-brand scroll">
            <img src="assets/media/general/logo_type-w.png" alt="logo" class="normal-logo img-responsive"/>
        </a>
        <ul class="nav navbar-nav">
            <?php
            $theme_locations = get_nav_menu_locations();

            $menu_items = wp_get_nav_menu_items($theme_locations['primary']);

            for ($i = 0; $i < count($menu_items); $i++) {
                if ((boolean)$menu_items[$i]->menu_item_parent != (boolean)$menu_items[$i + 1]->menu_item_parent) {
                    if ((boolean)$menu_items[$i]->menu_item_parent == false) {
                        ?>
                        <li class="dropdown <?php echo (int)$menu_items[$i]->object_id === get_the_ID() ? 'active' : ''; ?>">
                        <a data-toggle="dropdown" href="<?php echo $menu_items[$i]->url ?>"><?php echo $menu_items[$i]->title ?></a>
                        <ul role="menu" aria-labelledby="dropdownMenu" class="dropdown-menu">
                    <?php } else { ?>
                        <li class="<?php echo (int)$menu_items[$i]->object_id === get_the_ID() ? 'active' : ''; ?>"><a href="<?php echo $menu_items[$i]->url ?>"><?php echo $menu_items[$i]->title ?></a></li>
                        </ul>
                        </li>
                    <?php } ?>
                <?php } else { ?>
                    <li class="<?php echo (int)$menu_items[$i]->object_id === get_the_ID() ? 'active' : ''; ?>"><a href="<?php echo $menu_items[$i]->url ?>"><?php echo $menu_items[$i]->title ?></a></li>
                <?php } ?>
            <?php } ?>
        </ul>
    </div>
    <!-- end mobile menu-->


    <header class="header header-topbar-view header-logo-white header-topbarbox-1-left header-topbarbox-2-right header-navibox-1-left header-navibox-2-right header-navibox-3-right navbar-fixed-top">
        <div class="top-bar clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="header-topbarbox-1">
                            <ul class="social-net list-inline">
                                <li class="social-net__item"><a href="twitter.com" class="social-net__link"><i class="icon fa fa-twitter"></i></a></li>
                                <li class="social-net__item"><a href="facebook.com" class="social-net__link"><i class="icon fa fa-facebook"></i></a></li>
                                <li class="social-net__item"><a href="plus.google.com" class="social-net__link"><i class="icon fa fa-google-plus"></i></a></li>
                                <li class="social-net__item"><a href="youtube.com" class="social-net__link"><i class="icon fa fa-youtube-play"></i></a></li>
                                <li class="social-net__item"><a href="pinterest.com" class="social-net__link"><i class="icon fa fa-pinterest-p"></i></a></li>
                            </ul>
                            <!-- end social-list-->
                        </div>
                        <div class="header-topbarbox-2">
                            <div class="header-contact">
                                <div class="header-contact__item"><i class="icon fa fa-phone"></i> 123 456 7890</div>
                                <div class="header-contact__item"><i class="icon fa fa-envelope-o"></i> inquiry@domain.com</div>
                                <div class="header-contact__item"><i class="icon fa fa-clock-o"></i> Mon to Fri : 0700 - 1800</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end top-bar-->

        <nav id="nav" class="navbar header-main">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="header-navibox-1">
                            <!-- Mobile Trigger Start-->
                            <button class="menu-mobile-button visible-xs-block js-toggle-mobile-slidebar toggle-menu-button"><i class="toggle-menu-button-icon"><span></span><span></span><span></span><span></span><span></span><span></span></i></button>
                            <a href="index.html" class="navbar-brand scroll"><img src="assets/media/general/logo_type-a.png" alt="Logo" class="normal-logo logo__img img-responsive"/><img src="assets/media/general/logo_type-a.png" alt="logo" class="scroll-logo logo__img img-responsive hidden-xs"/></a>
                        </div>
                        <div class="header-nav header-navibox-2"><a href="home.html" class="header-main__btn btn btn-primary btn-effect">get a free quote</a></div>
                        <div class="header-navibox-3">
                            <ul class="main-menu nav navbar-nav yamm">
                                <?php
                                $theme_locations = get_nav_menu_locations();

                                $menu_items = wp_get_nav_menu_items($theme_locations['primary_menu']);

                                for ($i = 0; $i < count($menu_items); $i++) {
                                    if ((boolean)$menu_items[$i]->menu_item_parent != (boolean)$menu_items[$i + 1]->menu_item_parent) {
                                        if ((boolean)$menu_items[$i]->menu_item_parent == false) {
                                            ?>
                                            <li class="dropdown <?php echo (int)$menu_items[$i]->object_id === get_the_ID() ? 'active' : ''; ?>">
                                            <a data-toggle="dropdown" href="<?php echo $menu_items[$i]->url ?>"><?php echo $menu_items[$i]->title ?></a>
                                            <ul role="menu" aria-labelledby="dropdownMenu" class="dropdown-menu">
                                        <?php } else { ?>
                                            <li class="<?php echo (int)$menu_items[$i]->object_id === get_the_ID() ? 'active' : ''; ?>"><a href="<?php echo $menu_items[$i]->url ?>"><?php echo $menu_items[$i]->title ?></a></li>
                                            </ul>
                                            </li>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <li class="<?php echo (int)$menu_items[$i]->object_id === get_the_ID() ? 'active' : ''; ?>"><a href="<?php echo $menu_items[$i]->url ?>"><?php echo $menu_items[$i]->title ?></a></li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                            <!-- end main-menu-->
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <!-- end header-main-->
    </header>
    <!-- end header-->

    <?php if (!is_front_page() && !is_404()) {
        $meta = get_post_meta(get_the_ID());
        if (!empty($meta)) {
            foreach ($meta as $key => $val) {
                $meta[$key] = maybe_unserialize($val[0]);
            }
        }
        ?>
        <div data-stellar-background-ratio="0.4" data-stellar-vertical-offset="0" class="section-title-page section-bg_dark-3 section-bg parallax" style="background:url(<?php echo !is_front_page() && is_home() ? get_theme_mod('sputnik_header_general_settings_image_all_posts') : $meta['header_image'] ?>)">
            <div class="section-bg__inner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="b-title-page"><?php echo !is_front_page() && is_home() ? get_theme_mod('sputnik_header_general_settings_title_all_posts') : $meta['header_title'] ?></h1>
                            <div class="b-title-page__info"><?php echo $meta['header_description'] ?></div>
                            <ol class="breadcrumb">
                                <?php if (function_exists('bcn_display')) {
                                    bcn_display();
                                } ?>
                                <!--                            <li><a href="home.html">home</a></li>-->
                                <!--                            <li class="active">about</li>-->
                            </ol>
                            <!-- end breadcrumb-->
                            <div class="ui-decor-1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end b-title-page-->
    <?php } ?>
