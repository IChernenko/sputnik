<?php
/*** The template for displaying 404 page. ***/

if (get_theme_mod('sputnik_general_404_style') == 'A') {

    get_header(); ?>

    <div data-stellar-background-ratio="0.4" data-stellar-vertical-offset="0" class="section-title-page parallax-bg section-bg_dark-3 section-bg parallax" style="background: url('<?php echo get_template_directory_uri() . '/media/images/bg-1.jpg'; ?>');">
        <div class="section-bg__inner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="b-title-page">404 error</h1>
                        <div class="b-title-page__info">Page Not Found</div>
                        <ol class="breadcrumb">
                            <li><a href="<?php echo get_home_url(); ?>">home</a></li>
                            <li><a href="<?php echo get_home_url(); ?>">pages</a></li>
                            <li class="active">404</li>
                        </ol>
                        <!-- end breadcrumb-->
                        <div class="ui-decor-1"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end b-title-page-->

    <section class="section-type-6">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="ui-title-block text-center"><span class="ui-title-block__emphasis">Sorry,</span> The Page Not Found</h2>
                </div>
            </div>
        </div>
        <div class="b-404-1" style="background: whitesmoke url('<?php echo get_template_directory_uri() . '/media/images/bg-1.png'; ?>') 50% 50% no-repeat;"><img src="<?php echo get_template_directory_uri() . '/media/images/404-1.png'; ?>" alt="404" class="b-404-1__img center-block img-responsive">
            <div class="b-404-1__info">perhaps go to home page and try again !!</div>
            <a class="b-404-1__btn btn btn-default btn-w-ic btn-sm btn-effect" href="<?php echo get_home_url(); ?>">take me to home page<i class="icon fa fa-long-arrow-right"></i></a>
        </div>
    </section>
    <!-- end section-type-6-->


    <?php get_footer();
} elseif(get_theme_mod('sputnik_general_404_style') == 'B'){?>
    <!DOCTYPE html>
    <html lang="en" class="no-js">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="x-ua-compatible" content="ie=edge"/>
        <title>Sputnik / 404</title>
        <meta content="" name="description"/>
        <meta content="" name="keywords"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta content="telephone=no" name="format-detection"/>
        <meta name="HandheldFriendly" content="true"/>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/css/master.css'; ?>"/>
        <link rel="icon" type="image/x-icon" href="favicon.ico"/>
    </head>
    <body>
    <div class="b-404-2 section-bg section-bg_dark-4">
        <div class="section-bg__inner">
            <header class="b-404-2__header"><a href="<?php echo get_home_url(); ?>" class="b-404-2__logo center-block"><img src="<?php echo get_template_directory_uri() . '/media/images/logo_type-c.png'; ?>" alt="logo" class="img-responsive"></a></header>
            <div class="b-404-2__main"><img src="<?php echo get_template_directory_uri() . '/media/images/404-2.png'; ?>" alt="404" class="b-404-2__img center-block img-responsive">
                <div class="b-404-2__info">error , page not found</div><a href="<?php echo get_home_url(); ?>" class="b-404-2__btn btn btn-default btn-w-ic btn-sm btn-effect">take me to home page<i class="icon fa fa-long-arrow-right"></i></a>
            </div>
            <footer class="b-404-2__footer">
                <div class="copyright">
                    <ul class="social-net list-inline">
                        <li class="social-net__item"><a href="twitter.com" class="social-net__link"><i class="icon fa fa-twitter"></i></a></li>
                        <li class="social-net__item"><a href="facebook.com" class="social-net__link"><i class="icon fa fa-facebook"></i></a></li>
                        <li class="social-net__item"><a href="plus.google.com" class="social-net__link"><i class="icon fa fa-google-plus"></i></a></li>
                        <li class="social-net__item"><a href="vimeo.com" class="social-net__link"><i class="icon fa fa-vimeo"></i></a></li>
                        <li class="social-net__item"><a href="instagram.com" class="social-net__link"><i class="icon fa fa-instagram"></i></a></li>
                    </ul>
                    <!-- end social-list-->
                    <p>© 2016<span class="copyright__emphasis"> Sputnik Logistics</span> All rights reserved.</p>
                </div>
            </footer>
        </div>
    </div>
    </body>
    </html>
<?php } ?>