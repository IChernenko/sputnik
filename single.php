<?php
/**
 * The Template for displaying all single posts
 */

get_header();

$sputnik_custom = isset( $wp_query ) ? get_post_custom( $wp_query->get_queried_object_id() ) : '';
$sputnik_layout = isset( $sputnik_custom['pix_page_layout'] ) ? $sputnik_custom['pix_page_layout'][0] : '2';
$sputnik_sidebar = isset( $sputnik_custom['pix_selected_sidebar'][0] ) ? $sputnik_custom['pix_selected_sidebar'][0] : 'sidebar-1';

if ( ! is_active_sidebar($sputnik_sidebar) ) $sputnik_layout = '1';

?>

<!-- =========================
	BLOG ITEMS
============================== -->
<div class="def-section single-blog-section">
	<div class="container">
		<div class="row">

			<?php sputnik_show_sidebar( 'left', $sputnik_layout, $sputnik_sidebar ); ?>

			<!-- === BLOG ITEMS === -->

			<div class="<?php if ( $sputnik_layout == 1 ) : ?>col-lg-12 col-md-12 col-sm-12<?php else : ?>col-lg-9 col-md-9 col-sm-9<?php endif; ?> col-xs-12 blog-single-item-<?php echo esc_attr($sputnik_layout); ?>">


					<!-- === BLOG ITEM === -->
					<div class="blog-single-post">

						<?php
							// Start the Loop.
							while ( have_posts() ) : the_post();

								get_template_part( 'templates/post-parts/content', 'single' );

							endwhile;

						?>

						<?php if (sputnik_get_option( 'blog_show_author_block', 'on' ) == 'on' ) : ?>
						<div class="blog-single-post-bb">
							<div class="blog-single-post-bb-arrow"><i class="flaticon-direction248"></i></div>
							<div class="blog-single-post-bb-line"></div>
						</div>

						<div class="blog-single-post-author">
							<?php if ( get_the_author_meta( 'user_url' ) != '' ) : ?>
								<div class="blog-single-post-author-avatar">
									<a class="post-avatar" href="<?php esc_url( the_author_meta( 'user_url' ) ); ?>" target="_blank">
										<?php echo get_avatar( get_the_author_meta('user_email'), 150 ); ?>
									</a>
								</div>
							<?php else : ?>
								<div class="blog-single-post-author-avatar">
									<?php echo get_avatar( get_the_author_meta('user_email'), 150 ); ?>
								</div>
							<?php endif; ?>

							<div class="blog-single-post-author-info">
								<div class="blog-single-post-author-info-name">
									<span class="blog-single-post-author-info-label"><?php esc_html_e( 'Author', 'sputnik' ); ?></span><br>
									<?php
									if ( get_the_author_meta('user_firstname') != '' || the_author_meta('user_lastname') != '' ) :
										echo get_the_author_meta('user_firstname') . ' ' . get_the_author_meta('user_lastname');
									else :
										the_author_meta('display_name');
									endif;
									?>
								</div>
								<div class="blog-single-post-author-info-socials">
									<?php if ( get_the_author_meta('twitter') != '' ) : ?>
									<a href="<?php echo esc_url(get_the_author_meta('twitter')); ?>"><div class="my-btn my-btn-default">
										<div class="my-btn-bg-top"></div>
										<div class="my-btn-bg-bottom"></div>
										<div class="my-btn-text">
											<i class="fa fa-twitter"></i>
										</div>
									</div></a>
									<?php endif; ?>
									<?php if ( get_the_author_meta('facebook') != '' ) : ?>
									<a href="<?php echo esc_url(get_the_author_meta('facebook')); ?>"><div class="my-btn my-btn-default">
										<div class="my-btn-bg-top"></div>
										<div class="my-btn-bg-bottom"></div>
										<div class="my-btn-text">
											<i class="fa fa-facebook"></i>
										</div>
									</div></a>
									<?php endif; ?>
									<?php if ( get_the_author_meta('google-plus') != '' ) : ?>
									<a href="<?php echo esc_url(get_the_author_meta('google-plus')); ?>"><div class="my-btn my-btn-default">
										<div class="my-btn-bg-top"></div>
										<div class="my-btn-bg-bottom"></div>
										<div class="my-btn-text">
											<i class="fa fa-google-plus"></i>
										</div>
									</div></a>
									<?php endif; ?>
									<?php if ( get_the_author_meta('pinterest') != '' ) : ?>
									<a href="<?php echo esc_url(get_the_author_meta('pinterest')); ?>"><div class="my-btn my-btn-default">
										<div class="my-btn-bg-top"></div>
										<div class="my-btn-bg-bottom"></div>
										<div class="my-btn-text">
											<i class="fa fa-pinterest-p"></i>
										</div>
									</div></a>
									<?php endif; ?>
									<?php if ( get_the_author_meta('instagram') != '' ) : ?>
									<a href="<?php echo esc_url(get_the_author_meta('instagram')); ?>"><div class="my-btn my-btn-default">
										<div class="my-btn-bg-top"></div>
										<div class="my-btn-bg-bottom"></div>
										<div class="my-btn-text">
											<i class="fa fa-instagram"></i>
										</div>
									</div></a>
									<?php endif; ?>
								</div>
								<div class="clearfix"></div>
								<div class="blog-single-post-author-info-text">
									<?php the_author_meta('user_description'); ?>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<?php endif; ?>

						<?php
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) {
							comments_template();
						}
						?>

					</div>

			</div>

			<?php sputnik_show_sidebar( 'right', $sputnik_layout, $sputnik_sidebar ); ?>

		</div>
	</div>
</div>

<?php get_footer(); ?>