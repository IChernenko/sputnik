<?php
/*** The taxonomy for service category. ***/

$sputnik_pix_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
$sputnik_css_animation_services = ( sputnik_get_option('css_animation_settings_services', '') != '' ) ? ' wow '.sputnik_get_option('css_animation_settings_services', '') : '';
?>

<?php get_header(); ?>

<div class="page-section">

	<div class="container">
		<div class="row">
			<!-- Left Column Starts -->
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
				<ul class="service-menu <?php echo esc_attr($sputnik_css_animation_services); ?>">
					<?php
					$args = array( 'taxonomy' => 'services_category', 'hide_empty' => '1', 'title_li'=> '', 'show_count' => '0', 'current_category' => $sputnik_pix_term->term_id, 'show_option_all' => esc_html__( 'All services', 'sputnik' ) );
					$categories = wp_list_categories ($args);
					?>
				</ul>
			</div>
			<!-- Left Column Ends -->
			<!-- Right Column Starts -->
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
				<div class="row services-masonry-holder">

				<?php
					$sputnik_pix_services = get_objects_in_term( $sputnik_pix_term->term_id, 'services_category');
					$args = array(
								'post_type' => 'services',
								'orderby' => array( 'menu_order' => 'ASC', 'date' => 'DESC' ),
								'post__in' => $sputnik_pix_services,
								'posts_per_page' => -1
							);

					$wp_query = new WP_Query( $args );

					if ( $wp_query->have_posts() ) :
						while ( $wp_query->have_posts() ) :
							$wp_query->the_post();
							$sputnik_pix_thumbnail = get_the_post_thumbnail($post->ID, 'sputnik-services-thumb', array('class' => "img-responsive") );
					?>
					<!-- Block Starts -->
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 item">
						<div class="service-1 <?php echo esc_attr($sputnik_css_animation_services); ?>">
							<div class="service-1-image">
								<?php echo wp_kses_post($sputnik_pix_thumbnail); ?>
							</div>
							<div class="service-1-title">
								<h3><?php echo wp_kses_post(get_the_title()); ?></h3>
							</div>
							<div class="service-1-text">
								<p>
									<?php echo sputnik_limit_words(get_the_excerpt(), 20); ?>
								</p>
							</div>
							<div class="service-1-button">
								<a href="<?php echo esc_url(get_permalink(get_the_ID())); ?>">
									<div class="my-btn my-btn-default">
										<div class="my-btn-bg-top"></div>
										<div class="my-btn-bg-bottom"></div>
										<div class="my-btn-text">
											<?php echo esc_html__( 'More', 'sputnik' ); ?>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
					<!-- Block End -->
					<?php
						endwhile;
					endif;
					?>
				</div>
			</div>
			<!-- Right Column Ends -->
		</div>
	</div>

</div>

<?php get_footer(); ?>